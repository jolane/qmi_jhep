#!/usr/bin/env python3
import glob, subprocess, sys, os
source = os.environ["QMI_JHEP"] + "/AmpGen"
target = os.environ["QMI_JHEP"] + "/AmpGenDev/Gen/AmpGen"

headers = list(glob.glob(f"{source}/AmpGen/*.h"))
headers_simd = list(glob.glob(f"{source}/AmpGen/simd/*.h"))
srcs = list(glob.glob(f"{source}/src/*.cpp"))
srcs_LS = list(glob.glob(f"{source}/src/Lineshapes/*.cpp"))
apps = list(glob.glob(f"{source}/apps/*.cpp")) + list(glob.glob(f"{source}/examples/*.cpp"))

excluded_files = ["IExtendLikelihood.cpp", "Utilities.cpp", "CompilerWrapper.cpp", "IExtendLikelihood.h", "AmpGen.cpp"]


def copy(files, target):
    for f in files:
        excludeF = False
        for excl in excluded_files:
            if excl in f:
                excludeF = True
        if excludeF ==False:
            subprocess.run(f"rsync --progress {f} {target}".split())


copy(headers, f"{target}/AmpGen")
copy(headers_simd, f"{target}/AmpGen/simd")
copy(srcs, f"{target}/src/")
copy(srcs_LS, f"{target}/src/Lineshapes/")
copy(apps, f"{target}/apps")

