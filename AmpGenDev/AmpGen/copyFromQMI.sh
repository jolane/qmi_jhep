#!/bin/bash
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/AmpGen/*.h AmpGen/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/AmpGen/simd/*.h AmpGen/simd/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/src/*.cpp src/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/src//Lineshapes/*.cpp src/Lineshapes/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/src//Lineshapes/*.cpp apps/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/apps/*.cpp apps/
rsync --progress $HOME/sw/qmiAmpGen/AmpGen/examples/*.cpp apps/
mv apps/AmpGen.cpp apps/appGenerator.cpp

