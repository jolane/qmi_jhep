# File automatically generated: DO NOT EDIT.

# Get the exported informations about the targets
get_filename_component(_dir "${CMAKE_CURRENT_LIST_FILE}" PATH)

# Set useful properties
get_filename_component(_dir "${_dir}" PATH)
set(DaVinciDev_INCLUDE_DIRS ${_dir}/include)
set(DaVinciDev_LIBRARY_DIRS ${_dir}/lib)

set(DaVinciDev_BINARY_PATH ${_dir}/bin ${_dir}/scripts)
set(DaVinciDev_PYTHON_PATH ${_dir}/python)

set(DaVinciDev_COMPONENT_LIBRARIES )
set(DaVinciDev_LINKER_LIBRARIES AmpGen)

set(DaVinciDev_ENVIRONMENT PREPEND;PYTHONPATH;\${LCG_releases_base}/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib;PREPEND;PATH;\${LCG_releases_base}/gcc/8.2.0-3fa06/x86_64-centos7/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_96/Python/2.7.16/x86_64-centos7-gcc8-opt/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/bin;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/gcc/8.2.0-3fa06/x86_64-centos7/lib64;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96/Boost/1.70.0/x86_64-centos7-gcc8-opt/lib;SET;ROOTSYS;\${LCG_releases_base}/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt;SET;PYTHONHOME;\${LCG_releases_base}/LCG_96/Python/2.7.16/x86_64-centos7-gcc8-opt;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v32r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v45r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v23r0p1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v24r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v26r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v14r4/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v21r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v45r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;\${LCG_releases_base}/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/include;SET;GAUDIAPPNAME;DaVinciDev;SET;GAUDIAPPVERSION;v45r1;PREPEND;PATH;\${.}/scripts;PREPEND;PATH;\${.}/bin;PREPEND;LD_LIBRARY_PATH;\${.}/lib;PREPEND;ROOT_INCLUDE_PATH;\${.}/include;PREPEND;PYTHONPATH;\${.}/python;PREPEND;PYTHONPATH;\${.}/python/lib-dynload;SET;DAVINCIDEV_PROJECT_ROOT;\${.}/../../;SET;AMPGENROOT;\${DAVINCIDEV_PROJECT_ROOT}/AmpGen;SET;AMPGEN_CXX;/cvmfs/lhcb.cern.ch/lib/bin/x86_64-centos7/lcg-g++-8.2.0)

set(DaVinciDev_EXPORTED_SUBDIRS)
foreach(p AmpGen)
  get_filename_component(pn ${p} NAME)
  if(EXISTS ${_dir}/cmake/${pn}Export.cmake)
    set(DaVinciDev_EXPORTED_SUBDIRS ${DaVinciDev_EXPORTED_SUBDIRS} ${p})
  endif()
endforeach()

set(DaVinciDev_OVERRIDDEN_SUBDIRS )
