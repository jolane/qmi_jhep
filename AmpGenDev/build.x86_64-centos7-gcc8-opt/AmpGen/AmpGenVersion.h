#ifndef AMPGEN_VERSION
/* Automatically generated file: do not modify! */
#ifndef CALC_GAUDI_VERSION
#define CALC_GAUDI_VERSION(maj,min) (((maj) << 16) + (min))
#endif
#define AMPGEN_MAJOR_VERSION 1
#define AMPGEN_MINOR_VERSION 2
#define AMPGEN_PATCH_VERSION 0
#define AMPGEN_VERSION CALC_GAUDI_VERSION(AMPGEN_MAJOR_VERSION,AMPGEN_MINOR_VERSION)
#endif
