# Install script for directory: /afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/AmpGen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/InstallArea/x86_64-centos7-gcc8-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib/libAmpGen.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so"
         OLD_RPATH "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libAmpGen.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/ampGenerator.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ampGenerator.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/BaryonFitter.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/BaryonFitter.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/bkpipigamma_SimFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/bkpipigamma_SimFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/FitHyperonParameters.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitHyperonParameters.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/FitterWithPolarisation.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FitterWithPolarisation.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/gammaGen.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/gammaGen_bkg.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/gammaGen_bkg.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiCKMSimFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiCKMSimFit_bkg.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimFit_bkg.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/QcGen2.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/QcGen2_bkg.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGen2_bkg.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/QcGenerator.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/QcGenerator.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/SignalOnlyFitter.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SignalOnlyFitter.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/SimFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/SimFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiPsi3770Fit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiBDKFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiPsi3770BinnedFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770BinnedFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiCKMSimBinnedFit.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiCKMSimBinnedFit.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiPsi3770Fit_bkg.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiPsi3770Fit_bkg.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE OPTIONAL FILES "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/bin/KspipiBDKFit_bkg.exe")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe"
         OLD_RPATH "/afs/cern.ch/user/j/jolane/work/qmi_jhep/AmpGenDev/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96/GSL/2.5/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/KspipiBDKFit_bkg.exe")
    endif()
  endif()
endif()

