# File automatically generated: DO NOT EDIT.
set(DaVinciDev_heptools_version 96)
set(DaVinciDev_heptools_system x86_64-centos7-gcc8)

set(DaVinciDev_PLATFORM x86_64-centos7-gcc8-opt)

set(DaVinciDev_VERSION v45r1)
set(DaVinciDev_VERSION_MAJOR 45)
set(DaVinciDev_VERSION_MINOR 1)
set(DaVinciDev_VERSION_PATCH )

set(DaVinciDev_USES DaVinci;v45r1)
set(DaVinciDev_DATA )

list(INSERT CMAKE_MODULE_PATH 0 ${DaVinciDev_DIR}/cmake)
include(DaVinciDevPlatformConfig)
