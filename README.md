QMI JHEP
====

This repository contains the tools needed to reproduce the results in arxiv.org/abs/2305.10787 - which has also been submitted to JHEP (hence the name of the repo)

Structure
---
This repo will essentially be divided 2:
    1. The applications/scripts needed to use the QMI method with simulated samples from AmpGen on the CERN grid (through DIRAC) - the "backend"
    2. The results/tables that appear in the paper - the "frontend"

Getting started
===
This repo contains a modified DavinciDev environment - mainly out of ease of use - AmpGen is a CMake project and can be compiled locally, then the build environment is uploaded as a tarball to Dirac, as a DiracFile, which is then downloaded by each worker node running the job. Thus if the worker node can run DaVinci selections, it can also do our QMI method, with the caveat that the machine itself must support the avx2 instruction - introduced in 2013 (i.e. as long as you are not running on 10+ year old hardware). 

Compiling
---

Enter the DaVinciDev folder and hit make - this just makes an Gaudi compatible version of AmpGen - you can then either run directly from the folder (e.g. "./run QcGen2.exe --nEvents 1000 --Output output.root my_opts.opt") or submit jobs to the grid using this directory for the "directory" variable of "GaudiExec()" for your "application" of the job you need to submit.
