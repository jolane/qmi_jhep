import os
import ROOT
import uncertainties
import uncertainties.umath
import pickle as pkl
import math
import sys

def rB(xp, yp, xm, ym):
    return 0.5 * (xp**2 + yp**2)**0.5 + 0.5 * (xm**2 + ym**2)**0.5
def deltaB(xp, yp, xm, ym):
    return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (xp * ym + xm* yp)/(xp*xm - yp * ym)) + math.pi))
def gamma(xp, yp, xm, ym):
    return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (-xp * ym + xm* yp)/(xp*xm + yp * ym)) + math.pi))

def getChi2(fileName):
    f = ROOT.TFile.Open(fileName)
    keys = [k.GetName() for k in f.GetListOfKeys()]
    
    p = {}
    for k in keys:
        if "chi2" in k:
            chi2 = f.Get(k)[0]
            ndf = f.Get(k.replace("chi2", "ndf"))[0]
            p[k] = chi2
            p[k.replace("chi2", "ndf")] = ndf
    return p

def getResultsDict(fileName):
    f = ROOT.TFile.Open(fileName)
    fr = f.Get("FitResult")
    output = fileName.split("/")[-1].replace("plot.root", "results.pkl")
    order = 0
    seed=0
    if "poly_" in fileName:
        order = int(fileName.split("poly_")[-1].split("_")[0])
        if "BDK" in fileName:
            seed = int(fileName.split("_")[-5])
        if "psi3770" in fileName:
            seed = int(fileName.split("_")[-4])



    print("{}".format(fileName))
    status = fr.Status()
    nParams = fr.NPar()
    ndf = fr.Ndf()
    chi2 = fr.Chi2()
    p = {}
    fcn = fr.MinFcnValue()
    #p["Seed"] = [seed]
    #p["Order"] = [order]
    #p["Status"] = [status]
    for i in range(nParams):
        p[fr.ParName(i)] = [uncertainties.ufloat(fr.Parameter(i), fr.Error(i))]

    keys = [k.GetName() for k in f.GetListOfKeys()]
    for k in keys:
        if "chi2" in k:
            chi2 = f.Get(k)[0]
            ndf = f.Get(k.replace("chi2", "ndf"))[0]
            p[k] = chi2
            p[k.replace("chi2", "ndf")] = ndf
    doCKM = False
    for k in p:
        if "CKM" in k:
            doCKM = True

    if doCKM:

        def findCKM(fr, p):
            for i in range(fr.NPar()):
                if p in fr.ParName(i):
                    return i

        def CKMIndex(fr, i):
            if i==0:
                return findCKM(fr, "x+")
            if i==1:
                return findCKM(fr,"y+")
            if i==2:
                return findCKM(fr, "x-")
            if i==3:
                return findCKM(fr, "y-")
            
           
        xp = p["CKM::x+"][0]
        yp = p["CKM::y+"][0]
        xm = p["CKM::x-"][0]
        ym = p["CKM::y-"][0]
        rBFit = rB(xp, yp, xm ,ym)
        deltaBFit = deltaB(xp, yp, xm ,ym)
        gammaFit = gamma(xp, yp, xm ,ym)

        drBFit = 0
        ddeltaBFit = 0
        dgammaFit = 0
        derivrB = list(rBFit.derivatives.values())
        derivdeltaB = list(deltaBFit.derivatives.values())
        derivgamma = list(gammaFit.derivatives.values())
        for i in range(len(derivrB)):
            for j in range(len(derivrB)):
                CKMi = CKMIndex(fr, i)
                CKMj = CKMIndex(fr, j)
                drBFit += derivrB[i] * fr.CovMatrix(CKMi, CKMj) * derivrB[j]
                ddeltaBFit += derivdeltaB[i] * fr.CovMatrix(CKMi, CKMj) * derivdeltaB[j]
                dgammaFit += derivgamma[i] * fr.CovMatrix(CKMi, CKMj) * derivgamma[j]

        rBFit2 = uncertainties.ufloat(rBFit.n, drBFit**0.5)
        deltaBFit2 = uncertainties.ufloat(deltaBFit.n, ddeltaBFit**0.5)
        gammaFit2 = uncertainties.ufloat(gammaFit.n, dgammaFit**0.5)
        
       

        p["CKM::rB"] = rBFit2
        p["CKM::deltaB"] = deltaBFit2
        p["CKM::gamma"] = gammaFit2
    return p


def makeOptFromDict(d, O, output):
    opt = """EventType D0 K0S0 pi- pi+
#TagTypes  {
#    "KK D0{K+,K-} .443"
#    "Kspi0 D0{K0S0,pi0} .690"
#    "Kppim D0{K+,pi-} 4.740"
#    "Kmpip D0{K-,pi+} 4.740"
#    "Kspipi D0{K0S0,pi-,pi+} .899"
#}
#
#nEVents = 1000 is the 'normal' version - change this? 
#CouplingConstant::Coordinates  polar
#CouplingConstant::AngularUnits deg 
Particle::DefaultModifier BL
#Particle::SpinFormalism   Canonical
#Particle::DefaultModifier BELLE2018
Particle::SpinFormalism   Covariant


#Use 1 decay with the yields of all types -> KK = CPEven (pretend all the CP even are just KK decays since we are w
TagTypes  {
    "KK D0{K+,K-} 2.546"
    "Kspi0 D0{K0S0,pi0} 1.725"
    "Kppim D0{K+,pi-} 23.457"
    "Kmpip D0{K-,pi+} 23.457"
    "Kspipi D0{K0S0,pi-,pi+} 1.833"
}



BTagTypes {
    "Bm2DKm -1 6.2765" 
    "Bp2DKp 1 6.2765" 
}

#CKM::x+ Free -0.094 0.01
#CKM::y+ Free -0.014 0.01
#CKM::x- Free 0.059 0.01
#CKM::y- Free 0.069 0.01
#CKM::rB Free 0.095 0.01 
#CKM::deltaB Free 1.929 0.01
#CKM::gamma Free 1.065 0.01
#CKM::x+ Free -0.0094 0.01
#CKM::y+ Free -0.0014 0.01
#CKM::x- Free 0.0059 0.01
#CKM::y- Free 0.0069 0.01
#


PhaseCorrection::debug false
testPhaseCorr false
makeCPConj true
useCache false
NIntMods {
    0.01
    2
}
staggerFit true
staggerFit::finalFit true
# Example usage of the K-matrix with P-vector parameters from https://arxiv.org/pdf/0804.2089.pdf 
# Note that as amplitude conventions are likely to be very different, so shouldn't reproduce the results. 
# The P-vector conventions on the other hand should be the same. 

#EventType D0 K0S0 pi- pi+

# Flag to interpret complex numbers are (amp,phase) rather than real/imaginary, and angles in deg rather than rad.


PhaseCorrection::PolyType antiSym_legendre
PhaseCorrection::useSquareDalitz true
doFit false
MinEvents 5
"""
    opt += "PhaseCorrection::Order {}\n".format(O)
    for k in d:
        if "PhaseCorrection" in k:
            v = d[k][0].n
            dv = d[k][0].s
            opt += "{} Free {} {}\n".format(k, v, dv)
        if "CKM::" in k:
            if k!="CKM::rB" and k!="CKM::deltaB" and k!="CKM::gamma":
                print(k)
                v = d[k][0].n
                dv = d[k][0].s
                opt += "{} Free {} {}\n".format(k, v, dv)
    with open(output, "w") as f:
        f.write(opt)

def getOrder(fitFile):
    if "poly_" in fitFile:
        return int(fitFile.split("/")[-1].replace("poly_", "").split("_")[0])


def chi2(psi_data, BDK_data, fitFile, fitType="combined"):
    os.system("xrdcp {} .".format(psi_data))
    os.system("xrdcp {} .".format(BDK_data))
    psi_data_local = psi_data.split("/")[-1]
    BDK_data_local = BDK_data.split("/")[-1]
    #psi_data_local = "psipp.root"
    #BDK_data_local = "BDK.root"
    print(fitFile)
    d = getResultsDict(fitFile)
    O = getOrder(fitFile)
    output = fitFile.split("/")[-1]
    log = output.replace("_plot.root", ".log")
    opt = fitFile.split("/")[-1].replace("plot.root", ".opt")
    makeOptFromDict(d, O, opt)
    NInt = int(float(2.5e5))
    if fitType=="combined":
        os.system("KspipiCKMSimFit.exe --nCores 1 --NInt {} --BESIIIDataSample {} --LHCbDataSample {} --Plots {} --LogFile {} {} 2&>1 > /dev/null".format(NInt, psi_data_local, BDK_data_local, output, log, opt))

    if fitType=="psi3770":
        os.system("KspipiPsi3770Fit.exe --NInt {} --BESIIIDataSample {} --Plots {} --LogFile {} {} 2&>1 > /dev/null".format(NInt, psi_data_local, output, log, opt))
    
    os.system("rm {}".format(psi_data_local))
    os.system("rm {}".format(BDK_data_local))
    d_chi2 = getChi2(output)
    return d_chi2


print(sys.argv) 
try:
    psi3770_datafile = sys.argv[2]
except:
    psi3770_datafile = "root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/647895/647895059/belle_2018_HFLAV_gaussShift_deltaBShift_smallShift_psi3770_199_1000_1000.root"
try:
    BDK_datafile = sys.argv[3]
except:
    BDK_datafile = "root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/647895/647895059/belle_2018_HFLAV_gaussShift_deltaBShift_smallShift_BDK_199_1000_1000.root"
try:
    fitfile = sys.argv[4]
except:
    fitfile = "root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/647895/647895059/poly_14_deltaBShift_belle_2018_HFLAV_gaussShift_deltaBShift_smallShift_BDK_199_1000_1000_comb_plot.root"
try:
    outfolder = sys.argv[5]
except:
    outfolder = "."
output = outfolder + "/" + fitfile.split("/")[-1].replace("_plot.root", "_chi2.pkl")

d_chi2 = chi2(psi3770_datafile, BDK_datafile, fitfile)
with open(output, "wb") as f:
    pkl.dump(d_chi2, f)
