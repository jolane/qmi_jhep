import pickle as pkl
import glob
import os, subprocess
import time
import math
import numpy as np
import uncertainties
import uncertainties.umath
from matplotlib import pyplot as plt
from matplotlib import patches
#import pandas as pd
from pandas import DataFrame, concat
import uproot
import scipy.stats
import scipy.optimize
import subprocess
from multiprocessing import Pool

def fitNormPull(x, nbins=15):
    import scipy.stats
    import scipy.optimize


#    print(x)
    def gauss(x, mu, sigma):
        return scipy.stats.norm(mu, sigma).pdf(x)
    n, b = np.histogram(x, bins=nbins, density=True)
    c = 0.5 * (b[1:] + b[:-1])
#    print(c)
#    print(gauss(c, np.mean(x), np.std(x))) 
    popt, pcov = scipy.optimize.curve_fit(gauss, c, n, [np.mean(x), np.std(x)])
#    print(popt)
    mu = popt[0]
    sigma = popt[1]
    dmu = pcov[0][0]**0.5
    dsigma = pcov[1][1]**0.5
    return mu, dmu, sigma, dsigma

def pickleLoad(fileName):
    return pkl.load(open(fileName, "rb"))

def makePullArr2(df, key, x0):
    x = [i.n for i in list(df[key])]
    dx = [i.s for i in list(df[key])]
    return (np.array(x) - x0)/np.array(dx)



def tform(x):
    if x<10:
        return f"0{x}"
    else:
        return f"{x}"


def makeFolder():
    t = time.localtime()
    print(t)
    yy = t.tm_year
    MM = tform(t.tm_mon)
    dd = tform(t.tm_mday)
    hh = tform(t.tm_hour)
    mm = tform(t.tm_min)
    ss = tform(t.tm_sec)
    t = time.localtime()    
    my_folder = f"{yy}_{MM}_{dd}/{hh}_{mm}_{ss}"
    os.system(f"mkdir -p {my_folder}")
    return my_folder


def getCovMatrix(logFile):
    f = open(logFile)
    l = f.readlines()
    start = 0
    end = 0
    for i in range(len(l)):
        if "CovarianceMatrix" in l[i]:
            start = i + 1
        if "ParamsOrder" in l[i]:
            end = i - 1
    d = {}
    params = l[end+1].replace("Fix", "").replace("Free", "").split()[1:]
    #print(params)
    for i in range(len(params)):
        row = l[start + i]
        a = row.split()
        #print(a)
        for j in range(len(a)):
            d[f"{params[i]},{params[j]}"] = float(a[j])
    
    return d


def getBinnedInfo(logFile):
    d = {}
    status = -1
    seed = -1


    
    with open(logFile) as f:
        for line in f:
            if "Parameter" in line:
                a = line.split()
                name = a[1]
                flag = a[2]
                value = float(a[3])
                error =  float(a[4])
                d[name] = [uncertainties.ufloat(value, error)]
            if "FitQuality" in line:
                a = line.split()
                status = int(a[5])
                d["Status"] = [status]

    
    C = getCovMatrix(logFile)
    #print(C)
    CKM = "CKM::x+" in d.keys()
    if CKM:
        xp = d["CKM::x+"][0]
        yp = d["CKM::y+"][0]
        xm = d["CKM::x-"][0]
        ym = d["CKM::y-"][0]
        if ym<0:
            ym = - ym
            yp = -yp
            d["CKM::y+"][0] = -d["CKM::y+"][0]
            d["CKM::y-"][0] = -d["CKM::y-"][0]

        rB = 0.5 * (xp**2 + yp**2)**0.5 + 0.5 * (xm**2 + ym**2)**0.5
        deltaB = uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (yp * xm + ym * xp)/(xp * xm - yp * ym)) + math.pi))
        gamma = uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (yp * xm - ym * xp)/(xp * xm + yp * ym)) + math.pi))
        plist = ["CKM::x+", "CKM::y+", "CKM::x-", "CKM::y-"]
        drB = 0
        ddeltaB = 0
        dgamma = 0
        deriv_rB = list(rB.derivatives.values())
        deriv_deltaB = list(deltaB.derivatives.values())
        deriv_gamma = list(gamma.derivatives.values())
        for i in range(len(deriv_rB)):
            for j in range(len(deriv_rB)):
                drB += deriv_rB[i] * C[f"{plist[i]},{plist[j]}"] * deriv_rB[j]
                ddeltaB += deriv_deltaB[i] * C[f"{plist[i]},{plist[j]}"] * deriv_deltaB[j]
                dgamma += deriv_gamma[i] * C[f"{plist[i]},{plist[j]}"] * deriv_gamma[j]

        d["CKM::rB"] = [uncertainties.ufloat(rB.n, drB**0.5)]
        d["CKM::deltaB"] = [uncertainties.ufloat(deltaB.n, ddeltaB**0.5)]
        d["CKM::gamma"] = [uncertainties.ufloat(gamma.n, dgamma**0.5)]
        

    binning_scheme = ""
    fit_method = ""
    if "equal" in logFile:
        binning_scheme = "equal"
    if "optimal" in logFile:
        if "mod_optimal" in logFile:
            binning_scheme = "mod_optimal"
        else:
            binning_scheme = "optimal"
    for i in ["psi3770","constant","constrained","combined","constant_besiiiRef", "constrained_besiiiRef"]:
        if f"{i}.log" in logFile:
            fit_method =  i

    d["binning_scheme"] = [binning_scheme]
    d["fit_method"] = [fit_method]
    return DataFrame(d)



def makeBinnedDF(job):
    logs = []
    job.backend.getOutputSandbox()
#    psi3770_logs = job.outputfiles.get("*psi3770.log")
    psi3770_logs = list(glob.glob(f"{job.outputdir}/*psi3770.log"))
#    constant_logs = job.outputfiles.get("*constant.log")
    constant_logs = list(glob.glob(f"{job.outputdir}/*constant.log"))
    constant_besiii_ref_logs = list(glob.glob(f"{job.outputdir}/*constant_besiiiRef.log"))
#    constrained_logs = job.outputfiles.get("*constrained.log")
    constrained_logs = list(glob.glob(f"{job.outputdir}/*constrained.log"))
    constrained_besiii_ref_logs = list(glob.glob(f"{job.outputdir}/*constrained_besiiiRef.log"))
#    combined_logs = job.outputfiles.get("*combined.log")
    combined_logs = list(glob.glob(f"{job.outputdir}/*combined.log"))
#    logs += [i.localDir + "/" + i.namePattern for i in psi3770_logs]
#    logs += [i.localDir + "/" + i.namePattern for i in constant_logs]
#    logs += [i.localDir + "/" + i.namePattern for i in constrained_logs]
#    logs += [i.localDir + "/" + i.namePattern for i in combined_logs]
    logs = psi3770_logs + constant_logs + constrained_logs + combined_logs + constant_besiii_ref_logs + constrained_besiii_ref_logs
    dfs = []
    print(logs)
    for log in logs:
    #    print(f"Binned log = {log}")
        dfs += [getBinnedInfo(log)]
#        print(dfs)

    return concat(dfs)

def makeBinnedDFFromSubjobs(job):
    dfs = []
    for j in job.subjobs:
        try:
            dfs += [makeBinnedDF(j)]
        except:
            print(f"No DF from {j.id} status = {j.status}")
    return concat(dfs)

def saveBinnedDFFromSubjobs(job, folder):
    df = makeBinnedDFFromSubjobs(job)
    with open(f"{folder}/{job.name}_binned.pkl", "wb") as f:
        pkl.dump(df, f)
    print(f"{folder}/{job.name}_binned.pkl")


def mergePsiAndBDKDF(df_psipp, df_BDK):
    dict_psipp = dict(df_psipp.query("Order>0"))
    dict_BDK = dict(df_BDK.query("Order>0"))
    dict_full = {}
    for key in dict_psipp:
        print(len(list(dict_psipp[key])))
        dict_full[f"psipp_{key}"] = list(dict_psipp[key])
    for key in dict_BDK:
        print(len(list(dict_BDK[key])))
        dict_full[f"BDK_{key}"] = list(dict_BDK[key])
    return DataFrame(dict_full)



def getFitInfo(j, my_folder, doPlot = True):
    
    MD_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("MD*BDK*_plot.root")])
    BDK_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("poly*BDK*_bound_plot.root")])
    psi3770_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("poly*psi3770*_plot.root")])
    BDK_list = " ".join(BDK_URLs)
    MD_list = " ".join(MD_URLs)
    psi3770_list = " ".join(psi3770_URLs)
    output_psi3770 = j.name + "_results_psi3770.pkl"
    output_BDK = j.name + "_results_BDK.pkl"
    output_MD = j.name + "_results_MD.pkl"
    plotFlag = "False"
#    my_folder = makeFolder()
    if doPlot:
        plotFlag = "True"
    os.system(f"./logFromTuple.sh {plotFlag} {my_folder} {output_psi3770} {psi3770_list}")
    os.system(f"./logFromTuple.sh {plotFlag} {my_folder} {output_BDK} {BDK_list}")
    os.system(f"./logFromTuple.sh {plotFlag} {my_folder} {output_MD} {MD_list}")

def multiJobInfo(js, my_folder, outputName):
    MD_list = ""
    BDK_list = ""
    comb_list = ""
    psi3770_list  = ""
    for j in js:
        try:
            MD_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("MD*BDK*_plot.root")])
        except:
            MD_URLs = []
        BDK_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("poly*BDK_plot.root")])
        comb_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("poly*comb_plot.root")])
        psi3770_URLs = sorted([o.accessURL()[0] for o in j.outputfiles.get("poly*psi3770*_plot.root")])
        BDK_list += " ".join(BDK_URLs)
        BDK_list += " "
        comb_list += " ".join(comb_URLs)
        comb_list += " "

        psi3770_list += " ".join(psi3770_URLs)
        psi3770_list += " "
        MD_list += " ".join(MD_URLs)
        MD_list += " "
    output_psi3770 = outputName + "_psi3770.pkl"
    output_BDK = outputName + "_BDK.pkl"
    output_comb = outputName + "_comb.pkl"
    output_MD = outputName + "_MD.pkl"

    os.system(f"./logFromTuple.sh False {my_folder} {output_psi3770} {psi3770_list}")
    os.system(f"./logFromTuple.sh False {my_folder} {output_BDK} {BDK_list}")
    os.system(f"./logFromTuple.sh False {my_folder} {output_comb} {comb_list}")
    os.system(f"./logFromTuple.sh False {my_folder} {output_MD} {MD_list}")



def mergeSubDFs(job, my_folder, delete=False):

    psi3770_pkl = sorted(glob.glob(f"{my_folder}/{job.name}_*_psi3770.pkl"))
    BDK_pkl = sorted(glob.glob(f"{my_folder}/{job.name}_*_BDK.pkl"))
    comb_pkl = sorted(glob.glob(f"{my_folder}/{job.name}_*_comb.pkl"))
    MD_pkl = sorted(glob.glob(f"{my_folder}/{job.name}_*_MD.pkl"))

    psi3770_dfs = [pkl.load(open(i, "rb")) for i in psi3770_pkl]
    if len(psi3770_dfs)!=0:
        psi3770_df = concat(psi3770_dfs)
        with open(f"{my_folder}/{job.name}_psi3770.pkl", "wb") as f:
            pkl.dump(psi3770_df, f)
        print(f"{my_folder}/{job.name}_psi3770.pkl")


    BDK_dfs = [pkl.load(open(i, "rb")) for i in BDK_pkl]
    if len(BDK_dfs)!=0:
        BDK_df = concat(BDK_dfs)
    
        with open(f"{my_folder}/{job.name}_BDK.pkl", "wb") as f:
            pkl.dump(BDK_df, f)
        print(f"{my_folder}/{job.name}_BDK.pkl")

    MD_dfs = [pkl.load(open(i, "rb")) for i in MD_pkl]
    if len(MD_dfs)!=0:
        MD_df = concat(MD_dfs)
        with open(f"{my_folder}/{job.name}_MD.pkl", "wb") as f:
            pkl.dump(MD_df, f)
        print(f"{my_folder}/{job.name}_MD.pkl")

    comb_dfs = [pkl.load(open(i, "rb")) for i in comb_pkl]
    if len(comb_dfs)!=0:
        comb_df = concat(comb_dfs)
    
        with open(f"{my_folder}/{job.name}_comb.pkl", "wb") as f:
            pkl.dump(comb_df, f)
        print(f"{my_folder}/{job.name}_comb.pkl")





    if delete:
        for fileName in psi3770_pkl:
            os.system(f"rm {fileName}")
        for fileName in BDK_pkl:
            os.system(f"rm {fileName}")
        for fileName in comb_pkl:
            os.system(f"rm {fileName}")

        for fileName in MD_pkl:
            os.system(f"rm {fileName}")
 

 
     

def dfsFromSubjobs(job, my_folder):
    for i in range(int(len(job.subjobs)/10)):
        js = job.subjobs.select(10*i, 10 * (i+1) - 1)
        multiJobInfo(js, my_folder, job.name + "_" + str(i))
    mergeSubDFs(job, my_folder, True)   

def mergePsiAndBDKJobFolder(job, my_folder):
    df_psipp = pkl.load(open(f"{my_folder}/{job.name}_psi3770.pkl", "rb"))
    df_BDK = pkl.load(open(f"{my_folder}/{job.name}_BDK.pkl", "rb"))
    df = mergePsiAndBDKDF(df_psipp, df_BDK)
    with open(f"{my_folder}/{job.name}.pkl","wb") as f:
        pkl.dump(df, f)
    print(f"{my_folder}/{job.name}.pkl")

def pullStat(x):
    m = np.mean(x)
    s = np.std(x)
    dm = s/(len(x)**0.5)
    ds = dm/(2**0.5)
    z_m = (m - 0)/dm
    z_s = (s - 1)/ds
    
    return f"{m:.2f}\pm{dm:.2f}",f"{s:.2f}\pm{ds:.2f}"


def confPullStat(x):
    m = np.mean(x)
    s = np.std(x)
    dm = s/(len(x)**0.5)
    ds = dm/(2**0.5)
    z_m = (m - 0)/dm
    z_s = (s - 1)/ds

    z_95 = 1.96
    
    
    return f"{m:.2f}\pm{dm:.2f}",f"{s:.2f}\pm{ds:.2f}",f"{z_m:.2f}",f"{z_s:.2f}",f"{abs(z_m) > z_95}", f"{abs(z_s) > z_95}"



def makePullTableHeader(paramType = "cartesian"):
#    return "Order & $x_+$ & $y_+$ & $x_-$ & $y_-$ & $r_B$ & $\delta_B$ & $\gamma$ \\\\\\hline"
    header =  "Fit Type & "
    if paramType=="polar":
        header += "$\langle \mathrm{pull}(r_B) \\rangle \pm \\frac{s(\mathrm{pull}(r_B))}{\sqrt{N}}, s(\mathrm{pull}(r_B)) \pm \\frac{s(\mathrm{pull}(r_B))}{\sqrt{2N}}$ &"
        header += "$\langle \mathrm{pull}(\delta_B) \\rangle \pm \\frac{s(\mathrm{pull}(\delta_B))}{\sqrt{N}}, s(\mathrm{pull}(\delta_B)) \pm \\frac{s(\mathrm{pull}(\delta_B))}{\sqrt{2N}}$ &"
        header += "$\langle \mathrm{pull}(\gamma) \\rangle \pm \\frac{s(\mathrm{pull}(\gamma))}{\sqrt{N}}, s(\mathrm{pull}(\gamma)) \pm \\frac{s(\mathrm{pull}(\gamma))}{\sqrt{2N}}$"
    else:
        header += "$\langle {\mathrm{pull}(x_+)} \\rangle \pm \\frac{s(\mathrm{pull}(x_+))}{\sqrt{N}}, s(\mathrm{pull}(x_+)) \pm \\frac{s(\mathrm{pull}(x_+))}{\sqrt{2N}}$ &"
        header += "$\langle {\mathrm{pull}(y_+)} \\rangle \pm \\frac{s(\mathrm{pull}(y_+))}{\sqrt{N}}, s(\mathrm{pull}(y_+)) \pm \\frac{s(\mathrm{pull}(y_+))}{\sqrt{2N}}$ &"
        header += "$\langle {\mathrm{pull}(x_-)} \\rangle \pm \\frac{s(\mathrm{pull}(x_-))}{\sqrt{N}}, s(\mathrm{pull}(x_-)) \pm \\frac{s(\mathrm{pull}(x_-))}{\sqrt{2N}}$ &"
        header += "$\langle \mathrm{pull}(y_-) \\rangle \pm \\frac{s(\mathrm{pull}(y_-))}{\sqrt{N}}, s(\mathrm{pull}(y_-)) \pm \\frac{s(\mathrm{pull}(y_-))}{\sqrt{2N}}$ "
#    & $y_+$ & $x_-$ & $y_-$ & $r_B$ & $\delta_B$ & $\gamma$ 
    header += "\\\\\\hline"
    return header

def makeBinnedTableHeader(paramType="cartesian"):
    if paramType=="polar":
        return "Order & $r_B$ & $\delta_B$ & $\gamma$ \\\\\\hline"
    else:
        return "Order & $x_+$ & $y_+$ & $x_-$ & $y_-$ \\\\\\hline"



def makePullTableRow(df, paramType="cartesian"):

    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi))
    gamma0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi))

    xp = df["CKM::x+"]
    yp = df["CKM::y+"]
    xm = df["CKM::x-"]
    ym = df["CKM::y-"]
    rB = df["CKM::rB"]
    deltaB = df["CKM::deltaB"]
    gamma = df["CKM::gamma"]

    pull_xp = [(i.n - xp0)/i.s for i in xp]
    pull_yp = [(i.n - yp0)/i.s for i in yp]
    pull_xm = [(i.n - xm0)/i.s for i in xm]
    pull_ym = [(i.n - ym0)/i.s for i in ym]
    if "CKM::x+_minos_lower" in df.keys():
        def pullMinos(x, x0, lower, upper):
            if x > x0:
                return (x - x0)/lower
            elif x < x0:
                return (x - x0)/upper
            else:
                return 0
        xp_minos_lower = df["CKM::x+_minos_lower"]
        xp_minos_upper = df["CKM::x+_minos_upper"]
        yp_minos_lower = df["CKM::y+_minos_lower"]
        yp_minos_upper = df["CKM::y+_minos_upper"]
        xm_minos_lower = df["CKM::x-_minos_lower"]
        xm_minos_upper = df["CKM::x-_minos_upper"]
        ym_minos_lower = df["CKM::y-_minos_lower"]
        ym_minos_upper = df["CKM::y-_minos_upper"]
        pull_xp = []
        pull_yp = []
        pull_xm = []
        pull_ym = []
        for i in range(len(xp)):
            pull_xp += [pullMinos(xp[i].n, xp0, xp_minos_lower[i], xp_minos_upper[i])]
            pull_yp += [pullMinos(yp[i].n, yp0, yp_minos_lower[i], yp_minos_upper[i])]
            pull_xm += [pullMinos(xm[i].n, xm0, xm_minos_lower[i], xm_minos_upper[i])]
            pull_ym += [pullMinos(ym[i].n, ym0, ym_minos_lower[i], ym_minos_upper[i])]



    pull_rB = [(i.n - rB0)/i.s for i in rB]
    pull_deltaB = [(i.n - deltaB0)/i.s for i in deltaB]
    pull_gamma = [(i.n - gamma0)/i.s for i in gamma]

    mean_xp, std_xp = pullStat(pull_xp)
    mean_yp, std_yp = pullStat(pull_yp)
    mean_xm, std_xm = pullStat(pull_xm)
    mean_ym, std_ym = pullStat(pull_ym)
    mean_rB, std_rB = pullStat(pull_rB)
    mean_deltaB, std_deltaB = pullStat(pull_deltaB)
    mean_gamma, std_gamma = pullStat(pull_gamma)

    row = ""
    if paramType=="polar":
        row = f"${mean_rB}$,${std_rB}$&"
        row += f"${mean_deltaB}$,${std_deltaB}$&"
        row += f"${mean_gamma}$,${std_gamma}$"

    else:

        row = f"${mean_xp}$,${std_xp}$&"
        row += f"${mean_yp}$,${std_yp}$&"
        row += f"${mean_xm}$,${std_xm}$&"
        row += f"${mean_ym}$,${std_ym}$"
    return row


def pullConfColor(x, x0):
    if abs(x)<x0:
        return "green"
    else:
        return "red"

def pullConfEntry(x, x0):
    color = pullConfColor(x, x0)
    return f"\pullEntry{{{x:.2f}}}{{{color}}}"


def makePullTableRowConf(df, z0=1.96 ,paramType="cartesian"):
    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi))
    gamma0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi))

    xp = df["CKM::x+"]
    yp = df["CKM::y+"]
    xm = df["CKM::x-"]
    ym = df["CKM::y-"]
    rB = df["CKM::rB"]
    deltaB = df["CKM::deltaB"]
    gamma = df["CKM::gamma"]

    pull_xp = [(i.n - xp0)/i.s for i in xp]
    pull_yp = [(i.n - yp0)/i.s for i in yp]
    pull_xm = [(i.n - xm0)/i.s for i in xm]
    pull_ym = [(i.n - ym0)/i.s for i in ym]
    

    
    if "CKM::x+_minos_lower" in df.keys():
        def pullMinos(x, x0, lower, upper):
            if x > x0:
                return (x - x0)/lower
            elif x < x0:
                return (x - x0)/upper
            else:
                return 0
        xp_minos_lower = df["CKM::x+_minos_lower"]
        xp_minos_upper = df["CKM::x+_minos_upper"]
        yp_minos_lower = df["CKM::y+_minos_lower"]
        yp_minos_upper = df["CKM::y+_minos_upper"]
        xm_minos_lower = df["CKM::x-_minos_lower"]
        xm_minos_upper = df["CKM::x-_minos_upper"]
        ym_minos_lower = df["CKM::y-_minos_lower"]
        ym_minos_upper = df["CKM::y-_minos_upper"]
        pull_xp = []
        pull_yp = []
        pull_xm = []
        pull_ym = []
        for i in range(len(xp)):
            pull_xp += [pullMinos(xp[i].n, xp0, xp_minos_lower[i], xp_minos_upper[i])]
            pull_yp += [pullMinos(yp[i].n, yp0, yp_minos_lower[i], yp_minos_upper[i])]
            pull_xm += [pullMinos(xm[i].n, xm0, xm_minos_lower[i], xm_minos_upper[i])]
            pull_ym += [pullMinos(ym[i].n, ym0, ym_minos_lower[i], ym_minos_upper[i])]










    pull_rB = [(i.n - rB0)/i.s for i in rB]
    pull_deltaB = [(i.n - deltaB0)/i.s for i in deltaB]
    pull_gamma = [(i.n - gamma0)/i.s for i in gamma]

    mean_xp = np.mean(pull_xp) 
    mean_yp = np.mean(pull_yp) 
    mean_xm = np.mean(pull_xm) 
    mean_ym = np.mean(pull_ym) 
    mean_rB = np.mean(pull_rB) 
    mean_deltaB = np.mean(pull_deltaB) 
    mean_gamma = np.mean(pull_gamma) 
    std_xp = np.std(pull_xp) 
    std_yp = np.std(pull_yp) 
    std_xm = np.std(pull_xm) 
    std_ym = np.std(pull_ym) 
    std_rB = np.std(pull_rB) 
    std_deltaB = np.std(pull_deltaB) 
    std_gamma = np.std(pull_gamma) 
    dmean_xp = std_xp/np.sqrt(len(pull_xp))
    dmean_yp = std_yp/np.sqrt(len(pull_yp))
    dmean_xm = std_xm/np.sqrt(len(pull_xm))
    dmean_ym = std_ym/np.sqrt(len(pull_ym))
    
 
    dmean_rB = std_rB/np.sqrt(len(pull_rB))
    dmean_deltaB = std_deltaB/np.sqrt(len(pull_deltaB))
    dmean_gamma = std_gamma/np.sqrt(len(pull_gamma))
    
    dstd_xp = dmean_xp/np.sqrt(2)
    dstd_yp = dmean_yp/np.sqrt(2)
    dstd_xm = dmean_xm/np.sqrt(2)
    dstd_ym = dmean_ym/np.sqrt(2)
    dstd_rB = dmean_rB/np.sqrt(2)
    dstd_deltaB = dmean_deltaB/np.sqrt(2)
    dstd_gamma = dmean_gamma/np.sqrt(2)
   
#    mean_xp, dmean_xp, std_xp, dstd_xp = fitNormPull(pull_xp)
#    mean_yp, dmean_yp, std_yp, dstd_yp = fitNormPull(pull_yp)
#    mean_xm, dmean_xm, std_xm, dstd_xm = fitNormPull(pull_xm)
#    mean_ym, dmean_ym, std_ym, dstd_ym = fitNormPull(pull_ym)
#    mean_rB, dmean_rB, std_rB, dstd_rB = fitNormPull(pull_rB)
#    mean_deltaB, dmean_deltaB, std_deltaB, dstd_deltaB = fitNormPull(pull_deltaB)
#    mean_gamma, dmean_gamma, std_gamma, dstd_gamma = fitNormPull(pull_gamma)
   
    #print(min(pull_xm))
    #print(max(pull_xm))
    #print(mean_xm)
    #print(dmean_xm)
    #print(np.median(pull_xm))
    print(f"x+ : {min(pull_xp):.3f} {max(pull_xp):.3f} {mean_xp:.3f} +- {dmean_xp:.3f} {np.median(pull_xp):.3f}")
    print(f"y+ : {min(pull_yp):.3f} {max(pull_yp):.3f} {mean_yp:.3f} +- {dmean_yp:.3f} {np.median(pull_yp):.3f}")
    print(f"x- : {min(pull_xm):.3f} {max(pull_xm):.3f} {mean_xm:.3f} +- {dmean_xm:.3f} {np.median(pull_xm):.3f}")
    print(f"y- : {min(pull_ym):.3f} {max(pull_ym):.3f} {mean_ym:.3f} +- {dmean_ym:.3f} {np.median(pull_ym):.3f}")
#    print(std_xm)


    z_0 = 1.96
    z_mean_xp = mean_xp/dmean_xp 
    z_mean_yp = mean_yp/dmean_yp 
    z_mean_xm = mean_xm/dmean_xm 
    z_mean_ym = mean_ym/dmean_ym 
    z_mean_rB = mean_rB/dmean_rB 
    z_mean_deltaB = mean_deltaB/dmean_deltaB 
    z_mean_gamma = mean_gamma/dmean_gamma 

    z_std_xp = (std_xp - 1)/dstd_xp
    z_std_yp = (std_yp - 1)/dstd_yp
    z_std_xm = (std_xm - 1)/dstd_xm
    z_std_ym = (std_ym - 1)/dstd_ym
    z_std_rB = (std_rB - 1)/dstd_rB
    z_std_deltaB = (std_deltaB - 1)/dstd_deltaB
    z_std_gamma = (std_gamma - 1)/dstd_gamma
    
    cl0 = scipy.stats.norm(0, 1).cdf(z_0) *100
    cl_mean_xp = scipy.stats.norm(0, 1).cdf(abs(z_mean_xp)) * 100
    cl_mean_yp = scipy.stats.norm(0, 1).cdf(abs(z_mean_yp)) * 100
    cl_mean_xm = scipy.stats.norm(0, 1).cdf(abs(z_mean_xm)) * 100
    cl_mean_ym = scipy.stats.norm(0, 1).cdf(abs(z_mean_ym)) * 100

    cl_std_xp = scipy.stats.norm(0, 1).cdf(abs(z_std_xp)) * 100
    cl_std_yp = scipy.stats.norm(0, 1).cdf(abs(z_std_yp)) * 100
    cl_std_xm = scipy.stats.norm(0, 1).cdf(abs(z_std_xm)) * 100
    cl_std_ym = scipy.stats.norm(0, 1).cdf(abs(z_std_ym)) * 100

    cl_mean_rB = scipy.stats.norm(0, 1).cdf(abs(z_mean_rB)) * 100
    cl_mean_deltaB = scipy.stats.norm(0, 1).cdf(abs(z_mean_deltaB)) * 100
    cl_mean_gamma = scipy.stats.norm(0, 1).cdf(abs(z_mean_gamma)) * 100

    cl_std_rB = scipy.stats.norm(0, 1).cdf(abs(z_std_rB)) * 100
    cl_std_deltaB = scipy.stats.norm(0, 1).cdf(abs(z_std_deltaB)) * 100
    cl_std_gamma = scipy.stats.norm(0, 1).cdf(abs(z_std_gamma)) * 100


    if paramType=="cartesian":
        return f"{pullConfEntry(cl_mean_xp, cl0)}&{pullConfEntry(cl_std_xp, cl0)}&{pullConfEntry(cl_mean_yp, cl0)}&{pullConfEntry(cl_std_yp, cl0)}&{pullConfEntry(cl_mean_xm, cl0)}&{pullConfEntry(cl_std_xm, cl0)}&{pullConfEntry(cl_mean_ym, cl0)}&{pullConfEntry(cl_std_ym, cl0)}"

    else:

        return f"{pullConfEntry(cl_mean_rB, cl0)}&{pullConfEntry(cl_std_rB, cl0)}&{pullConfEntry(cl_mean_deltaB, cl0)}&{pullConfEntry(cl_std_deltaB, cl0)}&{pullConfEntry(cl_mean_gamma, cl0)}&{pullConfEntry(cl_std_gamma, cl0)}"
           

def makePullTableRowConfFromJob(job, folder, z0=1.96, paramType="cartesian", fitType="BDK", seed_exc = ""):
    for x in [fitType]:
        try:
            pickleFile = f"{folder}/{job.name}_{x}.pkl"
            if os.path.isfile(pickleFile)==False:
                dfsFromSubjobs(job, folder)
            df = pickleLoad(pickleFile)
            #print(df)
            Order = max(df["Order"])
            print(Order)
            df = df.query(f"Order=={Order}")
            print(df)
#            if len(df.query("Status==0")) == 0:
#            try:
            df_0 = df.query("Status==0")
            print(f"Have {len(df_0)} of Status = 0")



            if len(df_0) == 0:
                edm_3 = 0.02
            else:
                edm_3 = 100 * max(df.query("Status==0")["edm"])
#            edm_0 =  0.00002
#            except:
                #fcn_0 = 1e3
#                fcn_0 = min(df.query("Status==1")["FCN"])

            

            df_3 = df.query("Status==3")
            print(f"Have {len(df_3)} of Status = 3")
            df_3 = df_3[df_3["edm"] <= edm_3]
            print(f"Choose {len(df_3)} of Status = 3 with edm < {edm_3 * 1e3:.3f}e-3")

#            else:
#                df = df.query("Status==0")
#            df = df.query("Status!=3")
            

            df_1 = df.query("Status==1")
            print(f"Have {len(df_1)} of Status = 1")
            edm_1 = edm_3/15.
            df_1 = df_1[df_1["edm"] < edm_1]
            print(f"Have {len(df_1)} of Status = 1 with edm < {edm_1*1e3:.3f}e-3")
            #df = df.query("Status!=3")
            df = concat([df_0, df_1, df_3])
            print(f"Have {len(df)} entries")
#            print(df[df["edm"]<edm_0])
            
            #0.00002

            
            #df = pd.concat([df_0, df_1])
            #df = df_1
#            if len(df.query("Status==0"))!=0:
#                df_0 = df.query("Status==0")
#            if len(df.query("Status==1"))!=0:
#                df_1 = df.query("Status==1")
#            if(len(df_1) > len(df_0)):
#                df = df_1
##            
#            if (len(df_0) > len(df_1)):
#                df = df_0
#            print(df_0)
#            print(df_1)
#            print(df)

            
            
            #print(df["Seed"])
#            df = df.query(f"Status==0")
#            if seed_exc != "":
#                df = df.query(seed_exc)
            #print(min(df["CKM::x-"]))
            #print(max(df["CKM::x-"]))
            for p in ["cartesian", "polar"]:
                print(f"Writing row for {job.name}")
                row = makePullTableRowConf(df, z0, p)
                print(row) 
                with open(f"{folder}/{job.name}_{x}_{p}_{z0}.tex", "w") as f:
                    f.write(row)
                print(f"{folder}/{job.name}_{x}_{p}_{z0}.tex")
               
                print(len(df))
        except:
            null_ = ""

#def makePullTableRowConf(df, z0=1.96 ,paramType="cartesian"):



def plotPullHist(x, bins, output, xlabel):
    fig, ax = plt.subplots(1,1)
    X = np.linspace(min(x), max(x), 1000)
    n, b = np.histogram(x, bins=bins)
    c = 0.5 * (b[1:] + b[:-1])
    w = 0.7 * (c[1] - c[0])
    bw = c[1] - c[0]
    
    mean = np.mean(x)
    std = np.std(x)
    #print(x)
    #print(std)
    #print(mean)
    #print(len(x))
    dmean = std/np.sqrt(len(x))
    dstd = std/np.sqrt(2 * len(x))
    
   
    Y = 1/(np.sqrt(2 * np.pi) * std * (max(x) - min(x))) * np.exp(-(X - mean)**2/(2*std**2))
    Y = Y * sum(n)/sum(Y) * len(X)/len(c)
    Ymu = Y * (X - mean)/std**2
    Ysigma = (Y/std) * ( (X - mean)**2 - std**2)/std**2
    dY = (dmean**2 * Ymu**2 + dstd**2 * Ysigma**2)**0.5

    bar = ax.bar(c, n, width=w, label="Pulls", color="black")
    line = ax.plot(X, Y, label=rf"$N({mean:.2f}\pm{dmean:.2f}, {std:.2f}\pm{dstd:.2f})$", color="red")
    dlineL = ax.plot(X, Y - dY, color="red", ls="dashed")
    dlineU = ax.plot(X, Y + dY, color="red", ls="dashed")
    ax.set_xlabel(xlabel)
    ax.set_ylabel(f"Frequency/{bw:.2f}")
    ax.legend(bbox_to_anchor=(0,-0.33), loc="lower left")
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()


#plotPull2D(pull_xp, pull_yp, f"{output}_xp_yp_pull.png",r"$\frac{x_+ - x_+^0}{\sigma x_+}$", r"$\frac{y_+ - y_+^0}{\sigma y_+}$")

def plotPull2D(x, y, output, xlabel = "", ylabel = ""):
    fig, ax = plt.subplots(1,1)
    n, b1, b2 = np.histogram2d(x, y, bins=5)
    c1 = 0.5 * (b1[1:] + b1[:-1])
    c2 = 0.5 * (b2[1:] + b2[:-1])
    w1 = c1[1] - c1[0]
    w2 = c2[1] - c2[0]
    sc = ax.imshow(n, origin="lower", extent=(min(c1), max(c1), min(c2), max(c2)))
    cb = plt.colorbar(sc)
    mu_1 = np.mean(c1 * n)
    mu_2 = np.mean(c2 * n)
    s_12 = np.mean(c1**2 * n) - np.mean(c1 * n)**2
    s_22 = np.mean(c2**2 * n) - np.mean(c2 * n)**2
    s_1 = abs(s_12)**0.5
    s_2 = abs(s_22)**0.5
    dm_1 = s_1/np.sqrt(len(c1))
    dm_2 = s_2/np.sqrt(len(c2))
    ds_1 = dm_1/np.sqrt(2)
    ds_2 = dm_2/np.sqrt(2)
    
    ax.set_xlabel(xlabel + rf"""
$\mu = {mu_1:.3f}\pm{dm_1:.3f}$
$\sigma = {s_1:.3f}\pm{ds_1:.3f}$""")
    ax.set_ylabel(ylabel + rf"""
$\mu = {mu_2:.3f}\pm{dm_2:.3f}$
$\sigma = {s_2:.3f}\pm{ds_2:.3f}$""")
    #ax.set_ylabel(ylabel)
    fig.savefig(output, dpi=300)
    plt.close()

def makePullArr(x, x0):
    p = []
    X = list(x)
    for i in range(len(X)):
        try:
            p+= [(X[i].n - x0)/X[i].s]
        except:
            a = 0
    #        print(f"No pull for {x[i]}")
    return p

def getCentralVal(x):
    v = []
    X = list(x)
    for i in range(len(X)):
        try:
            v += [X[i].n]
        except:
            a = 0
    return v

def makePullPlots(df, output):
    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi))
    gamma0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi))
    xp = list(df["CKM::x+"])
    yp = list(df["CKM::y+"])
    xm = list(df["CKM::x-"])
    ym = list(df["CKM::y-"])
    rB = list(df["CKM::rB"])
    deltaB = list(df["CKM::deltaB"])
    gamma = list(df["CKM::gamma"])
    #print(df)
    #print(xp)
    pull_xp = makePullArr(xp, xp0)#[(i.n - xp0)/i.s for i in xp]
    pull_yp = makePullArr(yp,yp0)# [(i.n - yp0)/i.s for i in yp]
    pull_xm = makePullArr(xm,xm0)# [(i.n - xm0)/i.s for i in xm]
    pull_ym = makePullArr(ym,ym0)# [(i.n - ym0)/i.s for i in ym]


    if "CKM::x+_minos_lower" in df.keys():
        def pullMinos(x, x0, lower, upper):
            if x > x0:
                return (x - x0)/abs(lower)
            elif x < x0:
                return (x - x0)/upper
            else:
                return 0
        xp_minos_lower = df["CKM::x+_minos_lower"]
        xp_minos_upper = df["CKM::x+_minos_upper"]
        yp_minos_lower = df["CKM::y+_minos_lower"]
        yp_minos_upper = df["CKM::y+_minos_upper"]
        xm_minos_lower = df["CKM::x-_minos_lower"]
        xm_minos_upper = df["CKM::x-_minos_upper"]
        ym_minos_lower = df["CKM::y-_minos_lower"]
        ym_minos_upper = df["CKM::y-_minos_upper"]
        pull_xp = []
        pull_yp = []
        pull_xm = []
        pull_ym = []
        xp_minos_lower = list(df["CKM::x+_minos_lower"])
        xp_minos_upper = list(df["CKM::x+_minos_upper"])
        yp_minos_lower = list(df["CKM::y+_minos_lower"])
        yp_minos_upper = list(df["CKM::y+_minos_upper"])
        xm_minos_lower = list(df["CKM::x-_minos_lower"])
        xm_minos_upper = list(df["CKM::x-_minos_upper"])
        ym_minos_lower = list(df["CKM::y-_minos_lower"])
        ym_minos_upper = list(df["CKM::y-_minos_upper"])
        pull_xp = []
        pull_yp = []
        pull_xm = []
        pull_ym = []
        xp = list(df["CKM::x+"])
        yp = list(df["CKM::y+"])
        xm = list(df["CKM::x-"])
        ym = list(df["CKM::y-"])


        for i in range(len(xp)):
            pull_xp += [pullMinos(xp[i].n, xp0, xp_minos_lower[i], xp_minos_upper[i])]
            pull_yp += [pullMinos(yp[i].n, yp0, yp_minos_lower[i], yp_minos_upper[i])]
            pull_xm += [pullMinos(xm[i].n, xm0, xm_minos_lower[i], xm_minos_upper[i])]
            pull_ym += [pullMinos(ym[i].n, ym0, ym_minos_lower[i], ym_minos_upper[i])]



    pull_rB = makePullArr(rB, rB0)#[(i.n - rB0)/i.s for i in rB]
    pull_deltaB = makePullArr(deltaB, deltaB0)#[(i.n - deltaB0)/i.s for i in deltaB]
    pull_gamma = makePullArr(gamma, gamma0)#[(i.n - gamma0)/i.s for i in gamma]

    mean_xp, std_xp = pullStat(pull_xp)
    mean_yp, std_yp = pullStat(pull_yp)
    mean_xm, std_xm = pullStat(pull_xm)
    mean_ym, std_ym = pullStat(pull_ym)
    mean_rB, std_rB = pullStat(pull_rB)
    mean_deltaB, std_deltaB = pullStat(pull_deltaB)
    mean_gamma, std_gamma = pullStat(pull_gamma)
    print(f"N(x+) = {len(pull_xp)}")

#def plotPullHist(x, bins, output, xlabel, ylabel):
    plotPullHist(pull_xp, 10, f"{output}_xp_pull.png", r"$\frac{x_+ - x_+^0}{\sigma x_+}$")
    plotPullHist(pull_yp, 10, f"{output}_yp_pull.png", r"$\frac{y_+ - y_+^0}{\sigma y_+}$")
    plotPullHist(pull_xm, 10, f"{output}_xm_pull.png", r"$\frac{x_- - x_-^0}{\sigma x_-}$")
    plotPullHist(pull_ym, 10, f"{output}_ym_pull.png", r"$\frac{y_- - y_-^0}{\sigma y_-}$")
    plotPullHist(pull_rB, 10, f"{output}_rB_pull.png", r"$\frac{r_B - r_B^0}{\sigma r_B}$")
    plotPullHist(pull_deltaB, 10, f"{output}_deltaB_pull.png", r"$\frac{\delta_B - \delta_B^0}{\sigma \delta_B}$")
    plotPullHist(pull_gamma, 10, f"{output}_gamma_pull.png", r"$\frac{\gamma - \gamma^0}{\sigma \gamma}$")

    plotPull2D(pull_xp, pull_yp, f"{output}_xp_yp_pull.png",r"$\frac{x_+ - x_+^0}{\sigma x_+}$", r"$\frac{y_+ - y_+^0}{\sigma y_+}$")
    plotPull2D(pull_xm, pull_ym, f"{output}_xm_ym_pull.png",r"$\frac{x_- - x_-^0}{\sigma x_-}$", r"$\frac{y_- - y_-^0}{\sigma y_-}$")

 
 


def plotValStat(x, x0, xlabel, output, nbins=10, prec=""):
    fig, ax = plt.subplots(1,1)
    n, b = np.histogram(x, bins=nbins)
    c = 0.5 * (b[1:] + b[:-1])
    w = c[1] - c[0]
    bar = ax.bar(c, n, width=0.7 * w, color="black")
    mu = np.mean(x)
    sigma = np.std(x)
    dmu = sigma/np.sqrt(len(x))
    dsigma = dmu/np.sqrt(2)

    X = np.linspace(min(x), max(x), 1000)
    Y = 1/(np.sqrt(2 * np.pi)) * np.exp(-(X - mu)**2/(2*sigma**2))/sigma
    Y = Y * sum(n)/sum(Y) * len(X)/len(c)
    Ymu = Y * (X - mu)/sigma**2
    Ysigma = (Y/sigma) * ( (X - mu)**2 - sigma**2)/sigma**2
    dY = (dmu**2 * Ymu**2 + dsigma**2 * Ysigma**2)**0.5

    
    line_label=rf"""$\mu = {mu:.3f}\pm{dmu:.3f}$
$\sigma = {sigma:.3f} \pm {dsigma:.3f}$"""
    if prec=="angle":
        line_label=rf"""$\mu = {mu:.1f}\pm{dmu:.1f}$
$\sigma = {sigma:.1f} \pm {dsigma:.1f}$"""

    
    line = ax.plot(X, Y, color="red", label=line_label)
    line_p = ax.plot(X, Y + dY, color="red", linestyle="dashed")
    line_m = ax.plot(X, Y - dY, color="red", linestyle="dashed")
    line_x0_label = rf"${xlabel}={x0:.3f}$"
    if prec=="angle":
        line_x0_label = rf"${xlabel}={x0:.1f}$"

    line_x0 = ax.plot([x0,x0],[0,max(Y)], color="blue", linestyle="dashed", label=line_x0_label)
    ax.set_xlabel(rf"${xlabel}$")
    y_label = rf"$N/{w:.3f}$" 
    if prec=="angle":
        y_label = rf"$N/{w:.1f}$"
    ax.set_ylabel(y_label)
    ax.legend(bbox_to_anchor=(0,0), loc="best")
    fig.tight_layout()


    fig.savefig(output, dpi=300)
    plt.close()



def makeValStatPlots(df, output):
    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    
    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = 0.5 * math.degrees(math.atan((yp0*xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi)
    gamma0 = 0.5 * math.degrees(math.atan((yp0*xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi)
    

    xp = getCentralVal(list(df["CKM::x+"]))
    yp = getCentralVal(list(df["CKM::y+"]))
    xm = getCentralVal(list(df["CKM::x-"]))
    ym = getCentralVal(list(df["CKM::y-"]))
    rB = getCentralVal(list(df["CKM::rB"]))
    deltaB = getCentralVal(list(df["CKM::deltaB"]))
    gamma = getCentralVal(list(df["CKM::gamma"]))
    #[i.n for i in df["CKM::x+"]]
#    yp = [i.n for i in df["CKM::y+"]]
#    xm = [i.n for i in df["CKM::x-"]]
#    ym = [i.n for i in df["CKM::y-"]]
#    rB = [i.n for i in df["CKM::rB"]]
#    deltaB = [i.n for i in df["CKM::deltaB"]]
#    gamma = [i.n for i in df["CKM::gamma"]]

    for i in range(len(deltaB)):
        _xp = xp[i]
        _yp = yp[i]
        _xm = xm[i]
        _ym = ym[i]
        _deltaB = deltaB[i]
        _rB = rB[i]
#        (xp + i yp) (xm + i ym) = xp xm - yp ym + i(yp xm + 
        if _deltaB < 100:
            #print(_deltaB)
            #print(_rB)
            raw_deltaB = math.degrees(0.5 * math.atan( (_yp * _xm + _ym * _xp)/(_xm * _xp - _ym * _yp)))
            raw_gamma = math.degrees(0.5 * math.atan( (_yp * _xm - _ym * _xp)/(_xm * _xp + _ym * _yp)))
            deltaB[i] = raw_deltaB + 90
            print(raw_deltaB)
            print(raw_gamma)
            print(raw_deltaB + 180)
            print(raw_deltaB + 270)
            #print(raw_deltaB + 360)

            

    plotValStat(xp, xp0, "x_+", f"{output}_xp_val.png")
    plotValStat(yp, yp0, "y_+",f"{output}_yp_val.png")
    plotValStat(xm, xm0, "x_\_",f"{output}_xm_val.png")
    plotValStat(ym, ym0, "y_\_",f"{output}_ym_val.png")
    plotValStat(rB, rB0, "r_B",f"{output}_rB_val.png")
    plotValStat(deltaB, deltaB0, "\delta_B",f"{output}_deltaB_val.png", prec="angle")
    plotValStat(gamma, gamma0, "\gamma",f"{output}_gamma_val.png", prec="angle")

    
    
 
def makeValStatTableRow(df, paramType="cartesian", includeChi2=False):
    #print(df)
    xp = df["CKM::x+"] 
    yp = df["CKM::y+"]
    xm = df["CKM::x-"]
    ym = df["CKM::y-"]
    rB = df["CKM::rB"]
    deltaB = df["CKM::deltaB"]
    gamma = df["CKM::gamma"]

    m_xp = np.mean([i.n for i in xp])
    s_xp = np.std([i.n for i in xp])
    m_yp = np.mean([i.n for i in yp])
    s_yp = np.std([i.n for i in yp])
    m_xm = np.mean([i.n for i in xm])
    s_xm = np.std([i.n for i in xm])
    m_ym = np.mean([i.n for i in ym])
    s_ym = np.std([i.n for i in ym])
    m_rB = np.mean([i.n for i in rB])
    s_rB = np.std([i.n for i in rB])
    m_deltaB = np.mean([i.n for i in deltaB])
    s_deltaB = np.std([i.n for i in deltaB])
    m_gamma = np.mean([i.n for i in gamma])
    s_gamma = np.std([i.n for i in gamma])
    

    row = ""
    if paramType=="polar":
        row = f"${m_rB:.3f}$&${s_rB:.3f}$&"
        row += f"${m_deltaB:.1f}$&${s_deltaB:.1f}$&"
        row += f"${m_gamma:.1f}$&${s_gamma:.1f}$"



    else:
        row = f"${m_xp:.3f}$&${s_xp:.3f}$&"
        row += f"${m_yp:.3f}$&${s_yp:.3f}$&"
        row += f"${m_xm:.3f}$&${s_xm:.3f}$&"
        row += f"${m_ym:.3f}$&${s_ym:.3f}$"

    if includeChi2:

        chi2_KK = np.mean(df["KK_chi2"]/df["KK_ndf"])
        chi2_Kspi0 = np.mean(df["Kspi0_chi2"]/df["Kspi0_ndf"])
        chi2_Kspipi = np.mean(df["Kspipi_chi2"]/df["Kspipi_ndf"])
        row += f"&${chi2_KK:.3f}$&"
        row += f"${chi2_Kspi0:.3f}$&"
        row += f"${chi2_Kspipi:.3f}$"


        
    return row



def writeValStatTableRow(df, output, includeChi2 = False):
    row_cartesian = makeValStatTableRow(df, paramType="cartesian", includeChi2=includeChi2)
    row_polar = makeValStatTableRow(df, paramType="polar", includeChi2=includeChi2)
    with open(f"{output}_cart.tex", "w") as f:
        f.write(row_cartesian)

    with open(f"{output}_polar.tex", "w") as f:
        f.write(row_polar)


def makeValTableRow(df, paramType="cartesian"):
    #print(df)
    xp = list(df["CKM::x+"])[0]
    yp = list(df["CKM::y+"])[0]
    xm = list(df["CKM::x-"])[0]
    ym = list(df["CKM::y-"])[0]
    rB = list(df["CKM::rB"])[0]
    deltaB = list(df["CKM::deltaB"])[0]
    gamma = list(df["CKM::gamma"])[0]
    if paramType=="polar":
        row = f"${rB.n:.3f}\pm{rB.s:.3f}$&"
        row += f"${deltaB.n:.1f}\pm{deltaB.s:.1f}$&"
        row += f"${gamma.n:.1f}\pm{gamma.s:.1f}$"
        return row

    else:
        row = f"${xp.n:.3f}\pm{xp.s:.3f}$&"
        row += f"${yp.n:.3f}\pm{yp.s:.3f}$&"
        row += f"${xm.n:.3f}\pm{xm.s:.3f}$&"
        row += f"${ym.n:.3f}\pm{ym.s:.3f}$"
        return row

def makeBinnedCKMTable(df, output, paramType="cartesian"):
    binning_schemes = {"equal" : "Equal", "optimal":"Optimal", "mod_optimal":"Modified Optimal"}
    table = makeBinnedTableHeader(paramType) + "\n"
    for b in binning_schemes:
        df_b = df.query(f"(binning_scheme=='{b}')&(fit_method=='constrained')")
        table += f"{binning_schemes[b]} & {makeValTableRow(df_b, paramType)} \\\\\n"
    with open(output, "w") as f:
        f.write(table)
    print(output)
    print(table)


def makePullTable(df, output, paramType="cartesian"):
    table = makePullTableHeader(paramType) + "\n"
    Omin = min(df["Order"])
    Omax = max(df["Order"])
    for i in range(Omin, Omax + 1):
        df_i = df.query(f"Order=={i}")
        row = makePullTableRow(df_i, paramType)
        if i==0:
            table += f"Model Dependent&{row}\\\\\n"
        else:
            table += f"QMI Order {i}&{row}\\\\\n"
    print(table)
    with open(output, "w") as f:
        f.write(table)
    return table

def makeUnbinnedPullTable(df_unbinned, output, paramType="cartesian"):
    table = makePullTableHeader(paramType) + "\n"

    df_i = df_unbinned.query(f"Order==0")
    row = makePullTableRow(df_i, paramType)
    table += f"MD&{row}\\\\\\hline\n"
    Omin = max([1, min(df_unbinned["Order"])])
    Omax = max(df_unbinned["Order"])
    for i in range(Omin, Omax + 1):
        df_i = df_unbinned.query(f"Order=={i}")
        row = makePullTableRow(df_i, paramType)
        table += f"QMI Order {i}&{row}\\\\\n"

    table = table[:len(table) - 3]
    print(table)
    with open(output, "w") as f:
        f.write(table)
    return table

def makeChi2TableHeader(fitType):
    table = ""
    if fitType=="psi3770" or fitType=="combined":
        table += "$\chi^2^{K^+ K^-}$/n_\mathrm{dof}^{K^+ K^-} &"
        table += "$\chi^2^{K_S^0 \pi^0}$/n_\mathrm{dof}^{K_S^0 \pi^0} &"
        table += "$\chi^2^{K_S^0 \pi^+ \pi^-}$/n_\mathrm{dof}^{K_S^0 \pi^+ \pi^-}"
        if fitType == "combined":
            table += "&"
        else:
            table += "\\\hline\n"
    if fitType == "BDK" or fitType=="combined": 
        table += "$\chi^2^{B^+}$/n_\mathrm{dof}^{B^+} &"
        table += "$\chi^2^{B^-}$/n_\mathrm{dof}^{B^-} \\\hline\n"
    return table


def makeChi2TableRow(df, fitType):
    row = ""
    if fitType == "psi3770" or fitType == "combined":
        KK_chi2 = np.mean(df["KK_chi2"])
        KK_ndf = np.mean(df["KK_ndf"])
        Kspi0_chi2 = np.mean(df["Kspi0_chi2"])
        Kspi0_ndf = np.mean(df["Kspi0_ndf"])
        Kspipi_chi2 = np.mean(df["Kspipi_chi2"])
        Kspipi_ndf = np.mean(df["Kspipi_ndf"])
        row += f"${KK_chi2:.2f}/{KK_ndf}$ & "
        row += f"${Kspi0_chi2:.2f}/{Kspi0_ndf}$ & "
        row += f"${Kspipi_chi2:.2f}/{Kspipi_ndf}$ & "
        if fitType=="combined":
            row += "&"
    if fitType=="BDK" or fitType=="combined":
        Bp2DKp_chi2 = np.mean(df["Bp2DKp_chi2"])
        Bp2DKp_ndf = np.mean(df["Bp2DKp_ndf"])
        Bm2DKm_chi2 = np.mean(df["Bm2DKm_chi2"])
        Bm2DKm_ndf = np.mean(df["Bm2DKm_ndf"])
        row += f"${Bp2DKp_chi2:.2f}/{Bp2DKp_ndf}$ & "
        row += f"${Bm2DKm_chi2:.2f}/{Bm2DKm_ndf}$ "
    return row



def makeUnbinnedChi2Table(df_unbinned, output, fitType="psi3770"):
    table = makeChi2TableHeader(fitType) + "\n"

    df_i = df_unbinned.query(f"Order==0")
    #row = makeChi2TableRow(df_i, fitType)
    #table += f"MD&{row}\\\\\\hline\n"
    Omin = max([1, min(df_unbinned["Order"])])
    Omax = max(df_unbinned["Order"])
    for i in range(Omin, Omax + 1):
        df_i = df_unbinned.query(f"Order=={i}")
        row = makeChi2TableRow(df_i, fitType)
        table += f"QMI Order {i}&{row}\\\\\n"

    table = table[:len(table) - 3]
    print(table)
    with open(output, "w") as f:
        f.write(table)
    return table


def makeUnbinnedChi2TableFromJob(job, folder, fitType="psi3770"):
    output = f"{folder}/{job.name}_chi2_{fitType}.tex"
    if fitType == "BDK" or fitType=="combined":
        if os.path.isfile(f"{folder}/{job.name}_BDK.pkl")==False:
            dfsFromSubjobs(job, folder)
        df_BDK = pickleLoad(f"{folder}/{job.name}_BDK.pkl")   
        makeUnbinnedChi2Table(df_BDK, output, fitType)
    if fitType=="psi3770":
        if os.path.isfile(f"{folder}/{job.name}_psi3770.pkl")==False:
            dfsFromSubjobs(job, folder)
        df_psi3770 = pickleLoad(f"{folder}/{job.name}_psi3770.pkl") 
        makeUnbinnedChi2Table(df_psi3770, output, fitType)








def makeBinnedAndUnbinnedPullTable(df_binned, df_unbinned, output, paramType="cartesian"):
    table = makePullTableHeader(paramType) + "\n"
    binning_schemes = {"equal" : "Equal", "optimal":"Optimal", "mod_optimal":"Modified Optimal"}

    df_i = df_unbinned.query(f"Order==0")
    row = makePullTableRow(df_i, paramType)
    table += f"MD&{row}\\\\\\hline\n"

    for b in binning_schemes:
        df_b = df_binned.query(f"(binning_scheme=='{b}')&(fit_method=='constrained')")
        table += f"Binned MI {binning_schemes[b]} & {makePullTableRow(df_b, paramType)} \\\\\n"

    table = table[:len(table) - 1]
    table += "\\hline \n"
    Omin = max([1, min(df_unbinned["Order"])])
    Omax = max(df_unbinned["Order"])
    for i in range(Omin, Omax + 1):
        df_i = df_unbinned.query(f"Order=={i}")
        row = makePullTableRow(df_i, paramType)
        table += f"QMI Order {i}&{row}\\\\\n"

    table = table[:len(table) - 3]
    print(table)
    with open(output, "w") as f:
        f.write(table)
    return table

def makeUnbinnedPullTableFromJob(job, folder, paramType="cartesian"):


    if os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
        dfsFromSubjobs(job, folder)
    df_unbinned = pkl.load(open(f"{folder}/{job.name}_BDK.pkl", "rb"))

    makeUnbinnedPullTable( df_unbinned, f"{folder}/{job.name}_{paramType}_pulls.tex", paramType)


 
def makeBinnedAndUnbinnedPullTableFromJob(job, folder, paramType="cartesian"):
    if os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
        dfsFromSubjobs(job, folder)
    df_binned = pkl.load(open(f"{folder}/{job.name}_binned.pkl", "rb"))
    if os.path.isfile(f"{folder}/{job.name}_binned.pkl") == False:
        saveBinnedDFFromSubjobs(job, folder)

    df_unbinned = pkl.load(open(f"{folder}/{job.name}_BDK.pkl", "rb"))

    makeBinnedAndUnbinnedPullTable(df_binned, df_unbinned, f"{folder}/{job.name}_{paramType}_pulls.tex", paramType)

def makeUnbinnedPullPlotsFromJob(job, folder, extraFilter = "", output_folder = ""):
    if os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
        dfsFromSubjobs(job, folder)
    df_unbinned = pkl.load(open(f"{folder}/{job.name}_BDK.pkl", "rb"))

    if output_folder == "":
        output_folder = folder
    #makeBinnedAndUnbinnedPullTable(df_binned, df_unbinned, f"{folder}/{job.name}_{paramType}_pulls.tex", paramType)

    Omin = max([1, min(df_unbinned["Order"])])
    Omax = max(df_unbinned["Order"])
    df_i = df_unbinned.query(f"Order==0")
    if len(df_i)!=0:
        makePullPlots(df_i, f"{output_folder}/{job.name}_MD")
    else:
        try:
            df_MD = pickleLoad(f"{folder}/{job.name}_MD.pkl")
            makePullPlots(df_MD, f"{output_folder}/{job.name}_MD")
        except:
            print(f"No MD pkl for {job.name}")
    for i in range(Omin, Omax + 1):
        df_i = df_unbinned.query(f"Order=={i}")
        if extraFilter != "":
            df_i = df_i.query(extraFilter)
        try:
            makePullPlots(df_i, f"{output_folder}/{job.name}_QMI_Order_{i}")
        except:
            print(f"Couldn't make plots for {df_i}")

    print(f"Done for {job.id} {job.name}")


def makeUnbinnedPullPlotsFromJobPsiFiltered(job, folder):
    if os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
        dfsFromSubjobs(job, folder)
    df_unbinned_psipp = pkl.load(open(f"{folder}/{job.name}_psi3770.pkl", "rb"))
    df_unbinned_BDK = pkl.load(open(f"{folder}/{job.name}_BDK.pkl", "rb"))
    df_unbinned_MD = pkl.load(open(f"{folder}/{job.name}_MD.pkl", "rb"))

    #makeBinnedAndUnbinnedPullTable(df_binned, df_unbinned, f"{folder}/{job.name}_{paramType}_pulls.tex", paramType)

    Omin = max([1, min(df_unbinned_BDK["Order"])])
    Omax = max(df_unbinned_BDK["Order"])
    df_i = df_unbinned_BDK.query(f"Order==0")
    df_MD = df_unbinned_MD.query(f"Order==0")
    try:
        makePullPlots(df_MD, f"{folder}/{job.name}_MD")
    except:
        print(f"No MD for {job.name}")
    for i in range(Omin, Omax + 1):
        df_psipp_i = df_unbinned_psipp.query(f"Order=={i}")
        df_BDK_i = df_unbinned_BDK.query(f"Order=={i}")

        df_filtered_i = df_BDK_i[df_psipp_i["Status"]==0]

        makePullPlots(df_filtered_i, f"{folder}/{job.name}_QMI_Order_{i}_filtered")

def makeBinnedPlotsFromJob(job, folder):
    if os.path.isfile(f"{folder}/{job.name}_binned.pkl") == False:
        saveBinnedDFFromSubjobs(job, folder)
    df_binned = pkl.load(open(f"{folder}/{job.name}_binned.pkl", "rb"))
    binning_schemes = {"equal" : "Equal", "optimal":"Optimal", "mod_optimal":"Modified Optimal"}
    fit_methods = ["constrained","combined", "constant"]
#    fm = "constrained"
    
    for b in binning_schemes:
        for fm in fit_methods:
            print(f"{b} {fm}")
            makePullPlots(df_binned.query(f"(binning_scheme=='{b}')&(fit_method=='{fm}')"), f"{folder}/{job.name}_binned_{b}_{fm}")
            makeValStatPlots(df_binned.query(f"(binning_scheme=='{b}')&(fit_method=='{fm}')"), f"{folder}/{job.name}_binned_{b}_{fm}")


def makeBinnedAndUnbinnedPullPlotsFromJob(job, folder):
    if os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
        dfsFromSubjobs(job, folder)
    if os.path.isfile(f"{folder}/{job.name}_binned.pkl") == False:
        saveBinnedDFFromSubjobs(job, folder)

    df_unbinned = pkl.load(open(f"{folder}/{job.name}_BDK.pkl", "rb"))
    df_binned = pkl.load(open(f"{folder}/{job.name}_binned.pkl", "rb"))
    #makeBinnedAndUnbinnedPullTable(df_binned, df_unbinned, f"{folder}/{job.name}_{paramType}_pulls.tex", paramType)

    binning_schemes = {"equal" : "Equal", "optimal":"Optimal", "mod_optimal":"Modified Optimal"}
    fm = "constrained"
    for b in binning_schemes:
        makePullPlots(df_binned.query(f"(binning_scheme=='{b}')&(fit_method=='{fm}')"), f"{folder}/{job.name}_binned_{b}_{fm}")
    Omin = max([1, min(df_unbinned["Order"])])
    Omax = max(df_unbinned["Order"])
    df_i = df_unbinned.query(f"Order==0")
    makePullPlots(df_i, f"{folder}/{job.name}_MD")
    for i in range(Omin, Omax + 1):
        df_i = df_unbinned.query(f"Order=={i}")
        makePullPlots(df_i, f"{folder}/{job.name}_QMI_Order_{i}")







def makeXYFromDF(df):
    x = []
    y = []

    for i in range(1, 9):
        x += [list(df[f"c{i}"])[0]]
        y += [list(df[f"s{i}"])[0]]
    return x, y
def makeXYFromDict(d):
    x = []
    y = []

    for i in range(1, 9):
        x += [d[f"c{i}"]]
        y += [d[f"s{i}"]]
    return x, y



def belle_babar_2018(binning_scheme):

    c_equal = {
            "1":uncertainties.ufloat(0.662, 0.0),
            "2":uncertainties.ufloat(0.622, 0.0),
            "3":uncertainties.ufloat(0.094, 0.0),
            "4":uncertainties.ufloat(-0.505, 0.0),
            "5":uncertainties.ufloat(-0.948,0.0),
            "6":uncertainties.ufloat(-0.574,0.0),
            "7":uncertainties.ufloat(0.027, 0.0),
            "8":uncertainties.ufloat(0.442,0.0)
            }
    s_equal = {
            "1":uncertainties.ufloat(0.003, 0.0),
            "2":uncertainties.ufloat(0.423, 0.0),
            "3":uncertainties.ufloat(0.828,0.0),
            "4":uncertainties.ufloat(0.751,0.0),
            "5":uncertainties.ufloat(-0.035,0.0),
            "6":uncertainties.ufloat(-0.562,0.0),
            "7":uncertainties.ufloat(-0.793,0.0),
            "8":uncertainties.ufloat(-0.403,0.0)
            }
    c_optimal = {
            "1":uncertainties.ufloat(-0.018,0.0),
            "2":uncertainties.ufloat(0.844,0.0),
            "3":uncertainties.ufloat(0.187,0.0),
            "4":uncertainties.ufloat(-0.913,0.0),
            "5":uncertainties.ufloat(-0.155,0.0),
            "6":uncertainties.ufloat(0.362,0.0),
            "7":uncertainties.ufloat(0.864,0.0),
            "8":uncertainties.ufloat(0.857,0.0)
            }
    s_optimal = {
            "1":uncertainties.ufloat(-0.811,0.0),
            "2":uncertainties.ufloat(-0.133,0.0),
            "3":uncertainties.ufloat(-0.865,0.0),
            "4":uncertainties.ufloat(-0.080,0.0),
            "5":uncertainties.ufloat(0.857,0.0),
            "6":uncertainties.ufloat(0.794,0.0),
            "7":uncertainties.ufloat(0.206,0.0),
            "8":uncertainties.ufloat(-0.333,0.0)
            }
    c_mod_optimal = {
            "1":uncertainties.ufloat(-0.356,0.0),
            "2":uncertainties.ufloat(0.805,0.0),
            "3":uncertainties.ufloat(0.068,0.0),
            "4":uncertainties.ufloat(-0.943,0.0),
            "5":uncertainties.ufloat(-0.354,0.0),
            "6":uncertainties.ufloat(0.257,0.0),
            "7":uncertainties.ufloat(0.713,0.0),
            "8":uncertainties.ufloat(0.784,0.0)
            }
    s_mod_optimal = {
            "1":uncertainties.ufloat(-0.282,0.0),
            "2":uncertainties.ufloat(-0.005,0.0),
            "3":uncertainties.ufloat(-0.727,0.0),
            "4":uncertainties.ufloat(-0.112,0.0),
            "5":uncertainties.ufloat(0.807,0.0),
            "6":uncertainties.ufloat(0.782,0.0),
            "7":uncertainties.ufloat(0.231,0.0),
            "8":uncertainties.ufloat(-0.378,0.0)
            }


    z_equal = {}
    z_optimal = {}
    z_mod_optimal = {}
    for i in range(1, 9):
        z_equal[f"c{i}"] = c_equal[f"{i}"]
        z_equal[f"s{i}"] = s_equal[f"{i}"]
        z_optimal[f"c{i}"] = c_optimal[f"{i}"]
        z_optimal[f"s{i}"] = s_optimal[f"{i}"]
        z_mod_optimal[f"c{i}"] = c_mod_optimal[f"{i}"]
        z_mod_optimal[f"s{i}"] = s_mod_optimal[f"{i}"]


    if binning_scheme=="equal":
        return z_equal
    if binning_scheme=="optimal":
        return z_optimal
    if binning_scheme=="mod_optimal":
        return z_mod_optimal
        



def BESIII_2020(binning_scheme):
    c_equal = {
            "1":uncertainties.ufloat(0.708, 0.02),
            "2":uncertainties.ufloat(0.671, 0.035),
            "3":uncertainties.ufloat(0.001, 0.047),
            "4":uncertainties.ufloat(-0.602, 0.053),
            "5":uncertainties.ufloat(-0.965,0.019),
            "6":uncertainties.ufloat(-0.554,0.062),
            "7":uncertainties.ufloat(-0.046, 0.057),
            "8":uncertainties.ufloat(0.403,0.036)
            }
    s_equal = {
            "1":uncertainties.ufloat(0.123, 0.076),
            "2":uncertainties.ufloat(0.341, 0.134),
            "3":uncertainties.ufloat(0.893,0.112),
            "4":uncertainties.ufloat(0.723,0.143),
            "5":uncertainties.ufloat(0.020,0.081),
            "6":uncertainties.ufloat(-0.589,0.147),
            "7":uncertainties.ufloat(-0.686,0.143),
            "8":uncertainties.ufloat(-0.474,0.091)
            }
    c_optimal = {
            "1":uncertainties.ufloat(-0.034,0.052),
            "2":uncertainties.ufloat(0.839,0.062),
            "3":uncertainties.ufloat(0.140,0.064),
            "4":uncertainties.ufloat(-0.904,0.021),
            "5":uncertainties.ufloat(-0.300,0.042),
            "6":uncertainties.ufloat(0.303,0.088),
            "7":uncertainties.ufloat(0.927,0.016),
            "8":uncertainties.ufloat(0.771,0.032)
            }
    s_optimal = {
            "1":uncertainties.ufloat(-0.899,0.094),
            "2":uncertainties.ufloat(-0.272,0.166),
            "3":uncertainties.ufloat(-0.674,0.172),
            "4":uncertainties.ufloat(-0.065,0.062),
            "5":uncertainties.ufloat(1.047,0.055),
            "6":uncertainties.ufloat(0.884,0.191),
            "7":uncertainties.ufloat(0.228,0.066),
            "8":uncertainties.ufloat(-0.316,0.123)
            }
    c_mod_optimal = {
            "1":uncertainties.ufloat(-0.270,0.061),
            "2":uncertainties.ufloat(0.829,0.027),
            "3":uncertainties.ufloat(0.038,0.044),
            "4":uncertainties.ufloat(-0.963,0.020),
            "5":uncertainties.ufloat(-0.460,0.044),
            "6":uncertainties.ufloat(0.130,0.055),
            "7":uncertainties.ufloat(0.762,0.025),
            "8":uncertainties.ufloat(0.699,0.035)
            }
    s_mod_optimal = {
            "1":uncertainties.ufloat(-0.140,0.168),
            "2":uncertainties.ufloat(-0.014,0.100),
            "3":uncertainties.ufloat(-0.796,0.095),
            "4":uncertainties.ufloat(-0.202,0.080),
            "5":uncertainties.ufloat(0.899,0.078),
            "6":uncertainties.ufloat(0.832,0.131),
            "7":uncertainties.ufloat(0.178,0.094),
            "8":uncertainties.ufloat(-0.085,0.141)
            }



    z_equal = {}
    z_optimal = {}
    z_mod_optimal = {}
    for i in range(1, 9):
        z_equal[f"c{i}"] = c_equal[f"{i}"]
        z_equal[f"s{i}"] = s_equal[f"{i}"]
        z_optimal[f"c{i}"] = c_optimal[f"{i}"]
        z_optimal[f"s{i}"] = s_optimal[f"{i}"]
        z_mod_optimal[f"c{i}"] = c_mod_optimal[f"{i}"]
        z_mod_optimal[f"s{i}"] = s_mod_optimal[f"{i}"]


    if binning_scheme=="equal":
        return z_equal
    if binning_scheme=="optimal":
        return z_optimal
    if binning_scheme=="mod_optimal":
        return z_mod_optimal
        



       



def plot_cisi(df, label, output, BESIII = {}, Model = {}, model_label="", negative=True, conjugate=False):
    fig, ax = plt.subplots(1,1)
    x, y = makeXYFromDF(df)    
    modifier = 1
    if negative:
        modifier=-1
    y_modifier = 1
    if conjugate:
        y_modifier=-1
    sc = ax.errorbar([modifier * i.n for i in x], [modifier * y_modifier * i.n for i in y],xerr=[i.s for i in x], yerr=[i.s for i in y] , ls="none", marker="x", color="red", label=label)
    ax.set_xlabel(r"$c_i$")
    ax.set_ylabel(r"$s_i$")

    x_label = []
    y_label = []

    for i in range(8):
        ax.annotate( f"{i+1}", (modifier * x[i].n, modifier * y_modifier * y[i].n), color="red")
        x_label += [[]]
        y_label += [[]]
    
    if len(BESIII)!=0:
        x, y = makeXYFromDict(BESIII)
        scBESIII = ax.errorbar([i.n for i in x], [i.n for i in y],xerr=[i.s for i in x], yerr=[i.s for i in y] , ls="none", marker="o", color="blue", label="BESIII")
        for i in range(8):
            x_label[i] += [x[i].n]
            y_label[i] += [y[i].n]
 
 
    if len(Model)!=0:
        x, y = makeXYFromDict(Model)
        scModel = ax.errorbar([i.n for i in x], [i.n for i in y],xerr=[i.s for i in x], yerr=[i.s for i in y] , ls="none", marker="o", color="green", label="Model")
        for i in range(8):
            x_label[i] += [x[i].n]
            y_label[i] += [y[i].n]
 
 
    for i in range(len(x_label)):
        ax.annotate(f"{i+1}", (np.mean(x_label[i]), np.mean(y_label[i])), color="blue")

    t = np.linspace(-np.pi, np.pi, 1000)
    l = ax.plot(np.cos(t), np.sin(t), ls="--", label=r"$c_i^2 + s_i^2=1$", color="black")

    ax.set_aspect("equal")

    ax.legend(bbox_to_anchor=(1.05, 1),loc="upper left")
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()

def plot_cisi_from_job(j, binning_scheme, fit_method, output, negative=False, conjugate=False):
    besiii_equal = BESIII_2020(binning_scheme)                                                                                                                                             
    model_equal = belle_babar_2018(binning_scheme)                                    
    if binning_scheme == "optimal":
        conjugate=False
    if binning_scheme == "mod_optimal":
        conjugate = False

    df = makeBinnedDF(j)
    z_equal = df.query(f"(fit_method=='{fit_method}')&(binning_scheme=='{binning_scheme}')")
    plot_cisi(z_equal, "Fit", f"{output}_{binning_scheme}.png", BESIII=besiii_equal, Model=model_equal, negative=negative, conjugate=conjugate)


def plotScatter(x, y, z, xlabel, ylabel, zlabel, output, aspect="auto", plotCB=True):
    fig, ax = plt.subplots(1,1)
    sc = ax.scatter(x, y, c=z, s=0.5)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if plotCB:
        cb = plt.colorbar(sc, ax=ax)
        cb.set_label(zlabel)
    ax.set_aspect(aspect)
    fig.tight_layout()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.savefig(output, dpi=300)
    plt.close()


def plotHist1D(x, bins, xlabel, output):
    fig, ax = plt.subplots(1,1)
    n, b= np.histogram(x, bins=bins)
    c = 0.5 * (b[1:] + b[:-1])
    bw = b[1] - b[0]
    w = 0.7 * bw
    bar = ax.bar(c, n, width=w, color="black")
    ax.set_xlabel(xlabel)
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()

def plotHist2D(x, y, bins, xlabel, output, aspect="auto"):
    fig, ax = plt.subplots(1,1)
    n, b1, b2 = np.histogram_2d(x, y, bins=bins)
    X = []
    Y = []
    Z = []
    c1 = 0.5 * (b1[1:] + b1[:-1])
    c2 = 0.5 * (b2[1:] + b2[:-1])
    for i in range(len(n)):
        for j in range(len(n[i])):
            X += [c1[i]]
            Y += [c2[j]]
            Z += [n[i][j]]
    sc = ax.scatter(X, Y, c=Z, s=1)
    cb = plt.colorbar(sc, ax=ax)
    ax.set_aspect(aspect)
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()



def DeltaKspi(s01, s02, s12):
    mD = 1.86484
    mKs = 0.497611
    mpi = 0.13957039
    
    E_Ks = (mD**2 + mKs**2 - s12)/(2 * mD)
    E_pip = (mD**2 + mpi**2 - s02)/(2 * mD)
    E_pim = (mD**2 + mpi**2 - s01)/(2 * mD)

    p_Ks = (E_Ks**2 - mKs**2)**0.5
    p_pip =(E_pip**2 - mpi**2)**0.5
    p_pim = (E_pim**2 - mpi**2)**0.5
    
    theta_Kspip = np.arccos((mKs**2 + mpi**2 + 2 * E_Ks * E_pip - s01)/(2*p_Ks * p_pip))
    theta_Kspim = np.arccos((mKs**2 + mpi**2 + 2 * E_Ks * E_pim - s02)/(2*p_Ks * p_pim))
    return (theta_Kspip - theta_Kspim)/np.pi


def m12_prime(s12):
    mD = 1.86484
    mKs = 0.497611
    mpi = 0.13957039
 
    s12_min = 4 * mpi**2
    s12_max = (mD - mKs)**2

    return (2 * s12 - (s12_min + s12_max))/(s12_max - s12_min)


def plotPhaseSpace(fileName, output):
    f = uproot.open(fileName)
    s01 = f["Dalitz"]["s01"].array()
    s02 = f["Dalitz"]["s02"].array()
    s12 = f["Dalitz"]["s12"].array()

    angle_Kspi = DeltaKspi(s01, s02, s12)
    m12_p = m12_prime(s12)

    O = np.zeros(len(s01))
    print(f"{min(s01)} {max(s01)}")
    print(f"{min(s02)} {max(s02)}")
#def plotScatter(x, y, z, xlabel, ylabel, zlabel, output, aspect="auto"):
    plotScatter(s01, s02, O, r"$m_+^2$", r"$m_\_^2$", "", f"{output}/Dalitz_PhaseSpace_Reg.png", "equal", plotCB=False)
    plotScatter(m12_p, angle_Kspi, O, r"$m'_{\pi\pi}^2$",  r"$\Delta_{K_S^0 \pi}$", "", f"{output}/Dalitz_PhaseSpace_Square.png", "equal", plotCB=False)


def plotStrongPhase(fileName, output):
    f = uproot.open(fileName)
    s01 = f["Dalitz"]["s01"].array()
    s02 = f["Dalitz"]["s02"].array()
    s12 = f["Dalitz"]["s12"].array()
    dd = f["dd"]["x"].array()
    sign_pi = []

    for i in range(len(s01)):
        if (s01[i]<s02[i]):
            sign_pi += [1]
        else: 
            sign_pi += [-1]
    
    dd2 = [np.angle(-np.exp(1j * i)) for i in dd]
    angle_Kspi = DeltaKspi(s01, s02, s12)
    m12_p = m12_prime(s12)

    plotScatter(s01, s02, dd2, r"$m_+^2$", r"$m_\_^2$", r"$\Delta \delta_D$", f"{output}/StrongPhase_Reg.png", "equal")
    plotScatter(s01, s02, sign_pi, r"$m_+^2$", r"$m_\_^2$", r"sign $\Delta \delta_D$", f"{output}/Sign_StrongPhase_Reg.png", "equal")
    plotScatter(m12_p, angle_Kspi, dd2, r"$m'_{\pi\pi}^2$",  r"$\Delta_{K_S^0 \pi}$", r"$\Delta \delta_D$", f"{output}/StrongPhase_Square.png", "equal")

            

def pullsCKM(df, signs = [1,1,1,1], forceUnitWidth=False):
    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    
    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = 0.5 * math.degrees(math.atan((yp0*xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi)
    gamma0 = 0.5 * math.degrees(math.atan((yp0*xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi)
    
    print(f"x+0 = {xp0:.3f}")
    print(f"y+0 = {yp0:.3f}")
    print(f"x-0 = {xm0:.3f}")
    print(f"y-0 = {ym0:.3f}")
    print(f"rB0 = {rB0:.3f}")
    print(f"deltaB0 = {deltaB0:.3f}")
    print(f"gamma0 = {gamma0:.3f}")
    
    dxp_mod = 1
    dckm = 0.01

    xp = np.mean([i.n for i in df["CKM::x+"]])
    xm = np.mean([i.n for i in df["CKM::x-"]])
    yp = np.mean([i.n for i in df["CKM::y+"]])
    ym = np.mean([i.n for i in df["CKM::y-"]])
    rB = np.mean([i.n for i in df["CKM::rB"]])
    deltaB = np.mean([i.n for i in df["CKM::deltaB"]])
    gamma = np.mean([i.n for i in df["CKM::gamma"]])


    dxp = np.std([i.n for i in df["CKM::x+"]])
    dxm = np.std([i.n for i in df["CKM::x-"]])
    dyp = np.std([i.n for i in df["CKM::y+"]])
    dym = np.std([i.n for i in df["CKM::y-"]])
    drB = np.std([i.n for i in df["CKM::rB"]])
    ddeltaB = np.std([i.n for i in df["CKM::deltaB"]])
    dgamma = np.std([i.n for i in df["CKM::gamma"]])

    print(f"x+ = {xp:.3f} +- {dxp:.3f}") 
    print(f"y+ = {yp:.3f} +- {dyp:.3f}") 
    print(f"x- = {xm:.3f} +- {dxm:.3f}") 
    print(f"y- = {ym:.3f} +- {dym:.3f}") 
   
    pull_xp = [(i.n * signs[0] - xp0)/i.s for i in df["CKM::x+"]]
    pull_yp = [(i.n  * signs[1] - yp0)/i.s for i in df["CKM::y+"]]
    pull_xm = [(i.n * signs[2] - xm0)/i.s for i in df["CKM::x-"]]
    pull_ym = [(i.n * signs[3] - ym0)/i.s for i in df["CKM::y-"]]

    if forceUnitWidth:
        pull_xp = [(i.n * signs[0] - xp0)/dxp for i in df["CKM::x+"]]
        pull_yp = [(i.n  * signs[1] - yp0)/dyp for i in df["CKM::y+"]]
        pull_xm = [(i.n * signs[2] - xm0)/dxm for i in df["CKM::x-"]]
        pull_ym = [(i.n * signs[3] - ym0)/dym for i in df["CKM::y-"]]


#    if "CKM::x+_minos_lower" in df.keys():
#        def pullMinos(x, x0, lower, upper):
#            if x > x0:
#                return (x - x0)/abs(lower)
#            elif x < x0:
#                return (x - x0)/upper
#            else:
#                return 0
#        xp_minos_lower = list(df["CKM::x+_minos_lower"])
#        xp_minos_upper = list(df["CKM::x+_minos_upper"])
#        yp_minos_lower = list(df["CKM::y+_minos_lower"])
#        yp_minos_upper = list(df["CKM::y+_minos_upper"])
#        xm_minos_lower = list(df["CKM::x-_minos_lower"])
#        xm_minos_upper = list(df["CKM::x-_minos_upper"])
#        ym_minos_lower = list(df["CKM::y-_minos_lower"])
#        ym_minos_upper = list(df["CKM::y-_minos_upper"])
#        pull_xp = []
#        pull_yp = []
#        pull_xm = []
#        pull_ym = []
#        xp = list(df["CKM::x+"])
#        yp = list(df["CKM::y+"])
#        xm = list(df["CKM::x-"])
#        ym = list(df["CKM::y-"])
#
#        for i in range(len(xp)):
#            pull_xp += [pullMinos(xp[i].n, xp0, xp_minos_lower[i], xp_minos_upper[i])]
#            pull_yp += [pullMinos(yp[i].n, yp0, yp_minos_lower[i], yp_minos_upper[i])]
#            pull_xm += [pullMinos(xm[i].n, xm0, xm_minos_lower[i], xm_minos_upper[i])]
#            pull_ym += [pullMinos(ym[i].n, ym0, ym_minos_lower[i], ym_minos_upper[i])]
#
#

    pull_rB = [(i.n - rB0)/i.s for i in df["CKM::rB"]]
    pull_deltaB = [(i.n - deltaB0)/i.s for i in df["CKM::deltaB"]]
    pull_gamma = [(i.n - gamma0)/i.s for i in df["CKM::gamma"]]
    mod_deltaB = 0
    mod_gamma = 0
    sign_deltaB = 1
    sign_gamma = 1
    if signs[0]==-1 and signs[2]==-1:
        mod_deltaB = 180
        mod_gamma = 180
        sign_deltaB = -1
        sign_gamma = -1
        
    print(f"rB = {rB:.3f} +- {drB:.3f}") 
    print(f"deltaB = {deltaB:.3f} +- {ddeltaB:.3f}") 
    print(f"gamma = {gamma:.3f} +- {dgamma:.3f}") 

    print(f"deltaB' = {sign_deltaB *deltaB + mod_deltaB:.3f} +- {ddeltaB:.3f}") 
    print(f"gamma' = {sign_gamma * gamma + mod_gamma:.3f} +- {dgamma:.3f}") 
 


    if forceUnitWidth:
        pull_rB = [(i.n - rB0)/drB for i in df["CKM::rB"]]
        pull_deltaB = [(sign_deltaB*i.n + mod_deltaB - deltaB0)/ddeltaB for i in df["CKM::deltaB"]]
        pull_gamma = [(sign_gamma * i.n + mod_gamma - gamma0)/dgamma for i in df["CKM::gamma"]]


    print(confPullStat(pull_xp))
    print(confPullStat(pull_yp))
    print(confPullStat(pull_xm))
    print(confPullStat(pull_ym))
    print(confPullStat(pull_rB))
    print(confPullStat(pull_deltaB))
    print(confPullStat(pull_gamma))
    


    
def filter_df(df_psipp, df_BDK, chi2_factor=2):
    my_filter = (df_psipp["KK_chi2"]<chi2_factor * df_psipp["KK_ndf"])&(df_psipp["Kspi0_chi2"]<chi2_factor * df_psipp["Kspi0_ndf"])&(df_psipp["Kspipi_chi2"]<chi2_factor * df_psipp["Kspipi_ndf"])&(df_psipp["Status"]!=3)
    df_filtered = df_BDK[my_filter]
    return df_filtered


def filter_df_from_job(job, folder):
#    if os.path.isfile(f"{folder}/{job.name}_psi3770.pkl") == False or os.path.isfile(f"{folder}/{job.name}_BDK.pkl") == False:
    dfsFromSubjobs(job, folder)
    df_psipp = pickleLoad(f"{folder}/{job.name}_psi3770.pkl")
    df_BDK = pickleLoad(f"{folder}/{job.name}_BDK.pkl")
    return filter_df(df_psipp, df_BDK)

def psipp_chi2(df):
    chi2 = {"KK":[], "Kspi0":[],"Kspipi":[], "Kppim" : [], "Kmpip" : []}
    ndf = {"KK":[], "Kspi0":[],"Kspipi":[], "Kppim" : [], "Kmpip" : []}
    
    for k in "KK Kspi0 Kppim Kmpip Kspipi".split():
        chi2[k] += [df[f"{k}_chi2"]]
        ndf[k] += [df[f"{k}_ndf"]]
    
    tchi2 = 0
    tndf = 0
    for k in chi2:
        tchi2 += np.mean(np.array(chi2[k])/np.array(ndf[k]))

    tchi2 = tchi2/len(chi2)
    print(f"chi2 = {tchi2}")
    return chi2, ndf


def chi2FromJob(job, folder):
    psi_file = job.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
    BDK_file = job.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
    fit_files = job.outputfiles.get("*BDK*_plot*.root")[0].accessURL()
    for fit_file in fit_files:
        print(f"Getting chi2 for {fit_file}")
        os.system(f"./calcChi2.sh {psi_file} {BDK_file} {fit_file} {folder} &")

def chi2FromSubjobs(job, folder):
    for i in range(int(len(job.subjobs)/10)):
        js = job.subjobs.select(10*i, 10 * (i+1) - 1)
        cmd = ""
        for j in js:
            try:
                psi_file = j.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
                BDK_file = j.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
                fit_file = j.outputfiles.get("*BDK*_plot*.root")[0].accessURL()[0]
                cmd += f"./calcChi2.sh {psi_file} {BDK_file} {fit_file} {folder}\n"
            except:
                print(f"no urls for Job {job.id}.{j.id}")
        with open(f"{job.name}_calcChi2_{i}.sh", "w") as f:
            f.write("#!/bin/bash\n")
            f.write(cmd)
        os.system(f"chmod +x {job.name}_calcChi2_{i}.sh")
        os.system(f"./{job.name}_calcChi2_{i}.sh > {job.name}_calcChi2_{i}.stdout&")

def redoBinnedFit(psi_url, BDK_url, folder):
#    psi_file = job.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
#    BDK_file = job.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
    os.system(f"xrdcp {psi_url} .")
    os.system(f"xrdcp {BDK_url} .")

    psi_file = psi_url.split("/")[-1]
    BDK_file = BDK_url.split("/")[-1]
    for binning_scheme in ["equal", "optimal", "mod_optimal"]:
        psi_log = folder + "/" + psi_file.split("/")[-1].replace(".root", f"{binning_scheme}_psi3770.log")
        constant_log = folder + "/" + BDK_file.split("/")[-1].replace(".root", f"{binning_scheme}_constant.log")
        constrained_log = folder + "/" + BDK_file.split("/")[-1].replace(".root", f"{binning_scheme}_constrained.log")
        combined_log = folder + "/" + BDK_file.split("/")[-1].replace(".root", f"{binning_scheme}_combined.log")
        optFile = BDK_file.replace(".root", f"{binning_scheme}.opt")

     
        with open(optFile, "w") as f:
            f.write("""EventType D0 K0S0 pi- pi+
    c1 Free 0 1
    c2 Free 0 1
    c3 Free 0 1
    c4 Free 0 1
    c5 Free 0 1
    c6 Free 0 1
    c7 Free 0 1
    c8 Free 0 1
    s1 Free 0 1
    s2 Free 0 1
    s3 Free 0 1
    s4 Free 0 1
    s5 Free 0 1
    s6 Free 0 1
    s7 Free 0 1
    s8 Free 0 1
    F1 Fix 0 1
    F2 Fix 0 1
    F3 Fix 0 1
    F4 Fix 0 1
    F5 Fix 0 1
    F6 Fix 0 1
    F7 Fix 0 1
    F8 Fix 0 1
    F-1 Fix 0 1
    F-2 Fix 0 1
    F-3 Fix 0 1
    F-4 Fix 0 1
    F-5 Fix 0 1
    F-6 Fix 0 1
    F-7 Fix 0 1
    F-8 Fix 0 1
    CKM::x+ Free 0 1
    CKM::y+ Free 0 1
    CKM::x- Free 0 1
    CKM::y- Free 0 1
    TagTypes  {
        "KK D0{K+,K-} 2.546"
        "Kspi0 D0{K0S0,pi0} 1.725"
        "Kppim D0{K+,pi-} 23.457"
        "Kmpip D0{K-,pi+} 23.457"
        "Kspipi D0{K0S0,pi-,pi+} 1.833"
    }
    BTagTypes {
        "Bm2DKm -1 12.553" 
        "Bp2DKp 1 12.553" 
    }
    """)
            f.write(f"binning KsPiPi_{binning_scheme}.txt\n")
        gaudi_options = optFile.replace(".opt", ".py")
        with open(gaudi_options, "w") as f:
            f.write("import os\n")
            f.write(f"os.system('KspipiCKMSimBinnedFit.exe --BESIIIDataSample {psi_file} --LHCbDataSample {BDK_file} --nCores 1 --Psi3770Log {psi_log} --constantLog {constant_log} --constrainedLog {constrained_log} --combinedLog {combined_log} {optFile}')")
        os.system(f"~/sw/qmiAmpGen/DaVinciDev_v45r1/run gaudirun.py {gaudi_options} 2>&1 > /dev/null")
    os.system(f"rm {psi_file}")
    os.system(f"rm {BDK_file}")
    
def redoBinnedFits(jobs, folder):
    args = []
    os.system(f"mkdir -p {folder}")
    for j in jobs:
        try:
            psi_url = j.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
            BDK_url = j.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
            args += [(psi_url, BDK_url, folder)]
        except:
            print(f"Couldn't get arguments for {j.id}")
    with Pool(5) as p:
        p.starmap(redoBinnedFit, args)



def makeBinnedDFFromFolder(folder):
    logs = []
    psi3770_logs = list(glob.glob(f"{folder}/*psi3770.log"))
    constant_logs = list(glob.glob(f"{folder}/*constant.log"))
    constrained_logs = list(glob.glob(f"{folder}/*constrained.log"))
    combined_logs = list(glob.glob(f"{folder}/*combined.log"))
    logs = psi3770_logs + constant_logs + constrained_logs + combined_logs
    dfs = []
    for log in logs:
        dfs += [getBinnedInfo(log)]
    #    print(f"Binned log = {log}")
    return concat(dfs)


def saveBinnedDFFromFolder(folder, output):
    df = makeBinnedDFFromFolder(folder)
    with open(f"{folder}/{output}_binned.pkl", "wb") as f:
        pkl.dump(df, f)
    print(f"{folder}/{output}_binned.pkl")


def redoFit(job, folder, dry=True):
    try:
        psi_file = job.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
    except:
        psi_file = ""
    try:
        BDK_file = job.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
    except:
        BDK_file = ""
    
    try:
        psi_fit = job.outputfiles.get("*psi3770*plot*.root")[0].accessURL()[0]
    except:
        psi_fit = ""
    try:
        BDK_fit = job.outputfiles.get("*BDK*_bound*plot*.root")[0].accessURL()[0]
    except:
        BDK_fit = ""
    try:
        comb_file = job.outputfiles.get("*comb*plot*.root")[0].accessURL()[0]
    except:
        comb_file = ""
    
    print(f"psi = {psi_file}, BDK =  {BDK_file}, psi_fit =  {psi_fit}, BDK_fit =  {BDK_fit}, comb =  {comb_file}")
    fit_file = ""
    os.system(f"mkdir -p {folder}/{job.name}")
    if psi_file!="" and BDK_file != "":
        if comb_file !="":
            os.system(f"./redoFit.sh {psi_file} {BDK_file} {comb_file} {folder}/{job.name}")
            plot_file = comb_file.split("/")[-1]
            run_name = plot_file.replace("_plot.root", ".py")
            opt_name = plot_file.replace("_plot.root", ".opt")

            if dry==False:
                os.system(f"~/sw/qmiAmpGen/DaVinciDev_v45r1/run gaudirun.py {folder}/{job.name}/{run_name}")
                new_comb_file = folder + "/" + comb_file.split("/")[-1]
                os.system(f"./writeChi2.sh {psi_file} {BDK_file} {comb_file} {folder}/{job.name}")
            print(f"{folder}/{job.name}")
            os.system(f"ls {folder}/{job.name}")
        else:
            if psi_fit != "":
                os.system(f"./redoFit.sh {psi_file} {BDK_file} {BDK_fit} {folder}")
                os.system(f"~/sw/qmiAmpGen/DaVinciDev_v45r1/run gaudirun.py {folder}/redo.py")


def writeChi2(job, folder):
    try:
        psi_file = job.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
    except:
        psi_file = ""
    try:
        BDK_file = job.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
    except:
        BDK_file = ""
    
    try:
        psi_fit = job.outputfiles.get("*psi3770*plot*.root")[0].accessURL()[0]
    except:
        psi_fit = ""
    try:
        BDK_fit = job.outputfiles.get("*BDK*_bound*plot*.root")[0].accessURL()[0]
    except:
        BDK_fit = ""
    try:
        comb_file = job.outputfiles.get("*comb*plot*.root")[0].accessURL()[0]
    except:
        comb_file = ""
    
    print(f"psi = {psi_file}, BDK =  {BDK_file}, psi_fit =  {psi_fit}, BDK_fit =  {BDK_fit}, comb =  {comb_file}")
    fit_file = ""

    if psi_file!="" and BDK_file != "":
        if comb_file !="":
            os.system(f"./writeChi2.sh {psi_file} {BDK_file} {comb_file} {folder}")


def makeProjections(psi_file, BDK_file, fit_file, folder):
#    fit_folder = fit_file.replace("_plot.root", "")
#    os.system(f"mkdir -p {fit_folder}")
    os.system(f"./plotTuple.sh {psi_file} {BDK_file} {fit_file} {folder}")
def makeProjectionsJob(job, folder):
    try:
        psi_file = job.outputfiles.get("belle*psi3770*.root")[0].accessURL()[0]
    except:
        psi_file = ""
    try:
        BDK_file = job.outputfiles.get("belle*BDK*.root")[0].accessURL()[0]
    except:
        BDK_file = ""
    
    try:
        psi_fit = job.outputfiles.get("*psi3770*plot*.root")[0].accessURL()[0]
    except:
        psi_fit = ""
    try:
        BDK_fit = job.outputfiles.get("*BDK*_bound*plot*.root")[0].accessURL()[0]
    except:
        BDK_fit = ""
    try:
        comb_file = job.outputfiles.get("*comb*plot*.root")[0].accessURL()[0]
#        comb_file = f"{folder}/" + comb_file.split("/")[-1]
    except:
        comb_file = ""
    
    if os.path.isfile(comb_file) == False:
        redoFit(job, folder)


    if psi_file!="" and BDK_file != "":
        if comb_file !="":
            makeProjections(psi_file, BDK_file, comb_file, folder)

        else:
            if BDK_fit != "":
                makeProjections(psi_file, BDK_file, psi_fit, folder)
                makeProjections(psi_file, BDK_file, BDK_fit, folder)

                
            
    



def resubmit(j):
    try:
        j.backend.settings["BannedSites"]+= [j.backend.actualCE]
        j.resubmit()
    except:
        print(f"Couldn't resubmit {j.id}")



def diracStatus(i):
    o = subprocess.check_output(f"lb-dirac dirac-wms-job-status {i}".split()).decode()
    s = {}
    for j in o.split(";"):
        try:
            a = j.split("=")
            name = a[0]
            val = a[1]
            s[name] = [val]
        except:
            a = ""
    try:
        s["JobID"] = [int(s["JobID"][0].split(" ")[0])]
    except:
        a = ""

    s2 = {}
    for k in s:
        s2[k.replace(" ", "")] = s[k]

    

    
    cputime = 0
    
#    o = subprocess.check_output(f"lb-dirac dirac-wms-job-peek {i}".split()).decode()
   
    try:
        o = subprocess.check_output(f"lb-dirac dirac-wms-job-peek {i}".split()).decode()

        cputime = o.split("\n")[3].split(" ")[-2].split(":")
        cputime = 3600 * float(cputime[0]) + 60 * float(cputime[1]) + float(cputime[2])
    except:
        dummy = 0



    s2["CPUTime"] = [cputime]
    
        
    return s2



def diracStatuses(js):
    try:
        d0 = diracStatus(list(js)[0].backend.id)
        d0["gangaID"] = [list(js)[0].id]
    except:
        d0 =  { "gangaID": [] ,"JobID" : [], "MinorStatus" : [], "Status" : [], "Site":[]}

    
    for j in list(js)[1:]:
        d = diracStatus(j.backend.id)
        for k in d:
            d0[k] += d[k]
        d0["gangaID"] += [j.id]

    return DataFrame(d0)
    
    

def getBiasInfo(j):
    os.system(f"mkdir -p tmp/")
    os.system(f"tar xvfz {j.inputdir}/*.tgz -C tmp/") 
    try:
        genFile = glob.glob(f"tmp/belle*.opt")[0]
    except:
        genFile = ""

    d = {}

    if genFile != "":
        with open(genFile) as f:
            for line in f:
                if "::Gauss::" in line:
                    a = line.split()
                    name = a[0]
                    flag = a[1]
                    value = float(a[2])
                    error = float(a[3])
                    d[name] = value
    return d


def compPullPlots(m, s, dm, ds, output, xlabel, lims = [-3, 3], plotIdeal=True):
    fig, ax = plt.subplots(1,1)
    ax.set_yticks([0,1,2, 2.5, 3.0])
    ax.set_yticklabels(["Unbinned MD", "QMI", "Binned (Mod Optimal)", "Binned (Optimal)", "Binned (Equal)"])

    if plotIdeal:
        ax.plot([-1,-1],[-1,4],linestyle="dashed", color="red")
        ax.plot([1,1],[-1,4],linestyle="dashed", color="red")
        ax.plot([0,0],[-1,4],linestyle="dashed", color="red")
    h = 0.25
    
    x_max_R = max(np.array(m) + np.array(dm) + np.array(s) + np.array(ds)) 
    x_max_L = abs(min(np.array(m) - np.array(dm) - np.array(s) - np.array(ds)))
    x_max = max([x_max_L, x_max_R])
    lims = [-x_max, x_max]
    

    ax.set_xlim(lims)
    ax.set_ylim([-1,4])
    print(m[0] - s[0] - ds[0]) 
    r1_l = patches.Rectangle((m[0] - s[0] - ds[0], 0 - h/2), 2 * ds[0], h, color="black")
    r1_m = patches.Rectangle((m[0] - dm[0], 0 - h/2), 2 * dm[0], h, color="black")
    r1_r = patches.Rectangle((m[0] + s[0] - ds[0], 0 - h/2), 2 * ds[0], h, color="black")

    ax.add_patch(r1_l)
    ax.add_patch(r1_m)
    ax.add_patch(r1_r)

    r2_l = patches.Rectangle((m[1] - s[1] - ds[1], 1 - h/2), 2 * ds[1], h, color="black")
    r2_m = patches.Rectangle((m[1] - dm[1], 1 - h/2), 2 * dm[1], h, color="black")
    r2_r = patches.Rectangle((m[1] + s[1] - ds[1], 1 - h/2), 2 * ds[1], h, color="black")


    ax.add_patch(r2_l)

    ax.add_patch(r2_m)
    ax.add_patch(r2_r)



    r3_l = patches.Rectangle((m[2] - s[2] - ds[2], 2 - h/2), 2 * ds[2], h, color="black")
    r3_m = patches.Rectangle((m[2] - dm[2], 2 - h/2), 2 * dm[2], h, color="black")
    r3_r = patches.Rectangle((m[2] + s[2] - ds[2], 2 - h/2), 2 * ds[2], h, color="black")

    r4_l = patches.Rectangle((m[3] - s[3] - ds[3], 2.5 - h/2), 2 * ds[3], h, color="black")
    r4_m = patches.Rectangle((m[3] - dm[3], 2.5 - h/2), 2 * dm[2], h, color="black")
    r4_r = patches.Rectangle((m[3] + s[3] - ds[3], 2.5 - h/2), 2 * ds[3], h, color="black")

    r5_l = patches.Rectangle((m[4] - s[4] - ds[4], 3.0 - h/2), 2 * ds[4], h, color="black")
    r5_m = patches.Rectangle((m[4] - dm[4], 3.0 - h/2), 2 * dm[2], h, color="black")
    r5_r = patches.Rectangle((m[4] + s[4] - ds[4], 3.0 - h/2), 2 * ds[4], h, color="black")





    ax.plot([m[0] + dm[0], m[0] + s[0] - ds[0]], [0,0], color="black")
    ax.plot([m[0] - dm[0], m[0] - s[0] + ds[0]], [0,0], color="black")

    ax.plot([m[1] + dm[1], m[1] + s[1] - ds[1]], [1,1], color="black")
    ax.plot([m[1] - dm[1], m[1] - s[1] + ds[1]], [1,1], color="black")



    ax.plot([m[2] + dm[2], m[2] + s[2] - ds[2]], [2,2], color="black")
    ax.plot([m[2] - dm[2], m[2] - s[2] + ds[2]], [2,2], color="black")

    ax.plot([m[3] + dm[3], m[3] + s[3] - ds[3]], [2.5,2.5], color="black")
    ax.plot([m[3] - dm[3], m[3] - s[3] + ds[3]], [2.5,2.5], color="black")

    ax.plot([m[4] + dm[4], m[4] + s[4] - ds[4]], [3.0,3.0], color="black")
    ax.plot([m[4] - dm[4], m[4] - s[4] + ds[4]], [3.0,3.0], color="black")





    ax.add_patch(r3_l)
    ax.add_patch(r3_m)
    ax.add_patch(r3_r)
    ax.add_patch(r4_l)
    ax.add_patch(r4_m)
    ax.add_patch(r4_r)
    ax.add_patch(r5_l)
    ax.add_patch(r5_m)
    ax.add_patch(r5_r)


    ax.set_xlabel(xlabel)



    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()

def compValPlots(m, s, output, xlabel, lims = [-3, 3], plotIdeal=True, x0=0):
    fig, ax = plt.subplots(1,1)
    ax.set_yticks([0,1,2, 2.5, 3.0])
    #ax.set_yticklabels(["MD", "QMI", "Mod Optimal", "Optimal", "Equal"])
    ax.set_yticklabels(["Unbinned MD", "QMI", "Binned (Mod Optimal)", "Binned (Optimal)", "Binned (Equal)"])
    if plotIdeal:
        ax.plot([x0,x0],[-1,4],linestyle="dashed", color="red")
 
#    x_max_R = max(np.array(m) +  np.array(s) )
#    x_max_L = min(np.array(m) - np.array(s) )
#    sign_L = x_max_L/abs(x_max_L)
#    sign_R = x_max_R/abs(x_max_R)


#    lims = [sign_L * abs(x_max_L), sign_R * abs(x_max_R)]
    
    ax.set_xlim(lims)
    ax.set_ylim([-1,4])

    ax.errorbar([m[0]], [0], xerr=[s[0]], color="black", fmt=".", capsize=5)
    ax.errorbar([m[1]], [1], xerr=[s[1]], color="black", fmt=".", capsize=5)
    ax.errorbar([m[2]], [2], xerr=[s[2]], color="black", fmt=".", capsize=5)
    ax.errorbar([m[3]], [2.5], xerr=[s[3]], color="black", fmt=".", capsize=5)
    ax.errorbar([m[4]], [3.0], xerr=[s[4]], color="black", fmt=".", capsize=5)
    ax.set_xlabel(xlabel)
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()



def makeCompParams(job, folder):
    p = lambda f : pkl.load(open(f, "rb"))
    #dfsFromSubjobs(job, folder)
    #saveBinnedDFFromSubjobs(job, folder)
    df_MD = p(f"{folder}/{job.name}_MD.pkl")
    df_QMI = p(f"{folder}/{job.name}_BDK.pkl")
    O = max(df_QMI["Order"])
    df_QMI = df_QMI.query(f"Order=={O}")
    df_QMI = df_QMI.query("Status!=3")
    df_binned = p(f"{folder}/{job.name}_binned.pkl")
    df_binned2 = df_binned.query("fit_method=='combined'")
    df_MI_Equal = df_binned2.query("binning_scheme=='equal'")
    df_MI_Optimal = df_binned2.query("binning_scheme=='optimal'")
    df_MI_Mod_Optimal = df_binned2.query("binning_scheme=='mod_optimal'")
    
    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069

    rB0 = 0.5 * (xp0**2 + yp0**2)**0.5 + 0.5 * (xm0**2 + ym0**2)**0.5
    deltaB0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 + ym0 * xp0)/(xp0 * xm0 - yp0 * ym0)) + math.pi))
    gamma0 = math.degrees(0.5 * (math.atan( (yp0 * xm0 - ym0 * xp0)/(xp0 * xm0 + yp0 * ym0)) + math.pi))

    x0 = {"CKM::x+":-0.094, "CKM::y+":-0.014, "CKM::x-":0.059, "CKM::y-":0.069, "CKM::rB":rB0, "CKM::deltaB":deltaB0, "CKM::gamma":gamma0}
    pull_MD = {}
    pull_QMI = {}
    pull_MI_Equal = {}
    pull_MI_Optimal = {}
    pull_MI_Mod_Optimal = {}
    val_MD = {}
    val_QMI = {}
    val_MI_Equal = {}
    val_MI_Optimal = {}
    val_MI_Mod_Optimal = {}


    for k in x0:
        pull_MD[k] = makePullArr(df_MD[k], x0[k])# [(i.n - x0[k])/i.s for i in df_MD[k]]
        print(pull_MD[k])
        val_MD[k] = getCentralVal(df_MD[k])#[i.n for i in df_MD[k]] 
        pull_QMI[k] = makePullArr(df_QMI[k], x0[k])# [(i.n - x0[k])/i.s for i in df_QMI[k]]
        val_QMI[k] = getCentralVal(df_QMI[k])#[i.n for i in df_QMI[k]] 
        pull_MI_Equal[k] = makePullArr(df_MI_Equal[k], x0[k])# [(i.n - x0[k])/i.s for i in df_MI_Equal[k]]
        val_MI_Equal[k] = getCentralVal(df_MI_Equal[k])#[i.n for i in df_MI_Equal[k]] 
        pull_MI_Optimal[k] = makePullArr(df_MI_Optimal[k], x0[k])# [(i.n - x0[k])/i.s for i in df_MI_Optimal[k]]
        val_MI_Optimal[k] = getCentralVal(df_MI_Optimal[k])#[i.n for i in df_MI_Optimal[k]] 
        pull_MI_Mod_Optimal[k] = makePullArr(df_MI_Mod_Optimal[k], x0[k])# [(i.n - x0[k])/i.s for i in df_MI_Mod_Optimal[k]]
        val_MI_Mod_Optimal[k] = getCentralVal(df_MI_Mod_Optimal[k])#[i.n for i in df_MI_Mod_Optimal[k]] 







#        pull_QMI[k] = [(i.n - x0[k])/i.s for i in df_QMI[k]] 
#        val_QMI[k] = [i.n for i in df_QMI[k]] 
#        pull_MI_Equal[k] = [(i.n - x0[k])/i.s for i in df_MI_Equal[k]] 
#        val_MI_Equal[k] = [i.n for i in df_MI_Equal[k]] 
#        pull_MI_Optimal[k] = [(i.n - x0[k])/i.s for i in df_MI_Optimal[k]] 
#        val_MI_Optimal[k] = [i for i in df_MI_Optimal[k]] 
#        pull_MI_Mod_Optimal[k] = [(i.n - x0[k])/i.s for i in df_MI_Mod_Optimal[k]] 
#        val_MI_Mod_Optimal[k] = [i.n for i in df_MI_Mod_Optimal[k]] 

    mean_MD = {}
    mean_QMI = {}
    mean_MI_equal = {}
    mean_MI_optimal = {}
    mean_MI_mod_optimal = {}
    std_MD = {}
    std_QMI = {}
    std_MI_equal = {}
    std_MI_optimal = {}
    std_MI_mod_optimal = {}


    mu_MD = {}
    mu_QMI = {}
    mu_MI_equal = {}
    mu_MI_optimal = {}
    mu_MI_mod_optimal = {}
    sigma_MD = {}
    sigma_QMI = {}
    sigma_MI_equal = {}
    sigma_MI_optimal = {}
    sigma_MI_mod_optimal = {}
    dmu_MD = {}
    dmu_QMI = {}
    dmu_MI_equal = {}
    dmu_MI_optimal = {}
    dmu_MI_mod_optimal = {}
    dsigma_MD = {}
    dsigma_QMI = {}
    dsigma_MI_equal = {}
    dsigma_MI_optimal = {}
    dsigma_MI_mod_optimal = {}
    for k in pull_MD: 
        mu_MD[k] = np.mean(pull_MD[k]) 
        mu_QMI[k] = np.mean(pull_QMI[k]) 
        mu_MI_equal[k] = np.mean(pull_MI_Equal[k]) 
        mu_MI_optimal[k] = np.mean(pull_MI_Optimal[k]) 
        mu_MI_mod_optimal[k] = np.mean(pull_MI_Mod_Optimal[k]) 
        mean_MD[k] = np.mean(val_MD[k]) 
        mean_QMI[k] = np.mean(val_QMI[k]) 
        mean_MI_equal[k] = np.mean(val_MI_Equal[k]) 
        mean_MI_optimal[k] = np.mean(val_MI_Optimal[k]) 
        mean_MI_mod_optimal[k] = np.mean(val_MI_Mod_Optimal[k]) 



        sigma_MD[k] = np.std(pull_MD[k]) 
        sigma_QMI[k] = np.std(pull_QMI[k]) 
        sigma_MI_equal[k] = np.std(pull_MI_Equal[k]) 
        sigma_MI_optimal[k] = np.std(pull_MI_Optimal[k]) 
        sigma_MI_mod_optimal[k] = np.std(pull_MI_Mod_Optimal[k]) 

        std_MD[k] = np.std(val_MD[k]) 
        std_QMI[k] = np.std(val_QMI[k]) 
        std_MI_equal[k] = np.std(val_MI_Equal[k]) 
        std_MI_optimal[k] = np.std(val_MI_Optimal[k]) 
        std_MI_mod_optimal[k] = np.std(val_MI_Mod_Optimal[k]) 



        dmu_MD[k] = np.std(pull_MD[k])/np.sqrt(len(pull_MD[k])) 
        dmu_QMI[k] = np.std(pull_QMI[k])/np.sqrt(len(pull_QMI[k]))  
        dmu_MI_equal[k] = np.std(pull_MI_Equal[k])/np.sqrt(len(pull_MI_Equal[k]))  
        dmu_MI_optimal[k] = np.std(pull_MI_Optimal[k])/np.sqrt(len(pull_MI_Optimal[k]))  
        dmu_MI_mod_optimal[k] = np.std(pull_MI_Mod_Optimal[k])/np.sqrt(len(pull_MI_Mod_Optimal[k]))  


        dsigma_MD[k] = np.std(pull_MD[k])/np.sqrt(2*len(pull_MD[k])) 
        dsigma_QMI[k] = np.std(pull_QMI[k])/np.sqrt(2*len(pull_QMI[k]))  
        dsigma_MI_equal[k] = np.std(pull_MI_Equal[k])/np.sqrt(2*len(pull_MI_Equal[k]))  
        dsigma_MI_optimal[k] = np.std(pull_MI_Optimal[k])/np.sqrt(2*len(pull_MI_Optimal[k]))  
        dsigma_MI_mod_optimal[k] = np.std(pull_MI_Mod_Optimal[k])/np.sqrt(2*len(pull_MI_Mod_Optimal[k]))  
        print(f"{mu_MD[k]:.3f} +- {dmu_MD[k]:.3f}")
        print(f"{mu_QMI[k]:.3f} +- {dmu_QMI[k]:.3f}")
        print(f"{mu_MI_equal[k]:.3f} +- {dmu_MI_equal[k]:.3f}")

    names = {"CKM::x+":"xp", "CKM::y+":"yp", "CKM::x-":"xm", "CKM::y-":"ym", "CKM::rB":"rB", "CKM::deltaB":"deltaB", "CKM::gamma":"gamma"}
#    x0 = {"CKM::x+":-0.094, "CKM::y+":-0.014, "CKM::x-":0.059, "CKM::y-":0.069}
    eps = {}
    epsCart =  3 * 0.01
    eps["CKM::x+"] = epsCart
    eps["CKM::y+"] = epsCart
    eps["CKM::x-"] = epsCart
    eps["CKM::y-"] = epsCart
    eps["CKM::rB"] = epsCart
    eps["CKM::deltaB"] = 3 * 5
    eps["CKM::gamma"] = 3  * 5
    xlabel = {"CKM::x+":"x_+", "CKM::y+":"y_+", "CKM::x-":"x_-", "CKM::y-":"y_-", "CKM::rB":"r_B", "CKM::deltaB":"\delta_B", "CKM::gamma":"\gamma"}

    string = "import makeCompPlots\n"
    for k in names:
        string += f"makeCompPlots.pullPlots([{mu_MD[k]:.3f}, {mu_QMI[k]:.3f}, {mu_MI_optimal[k]:.3f}], [{sigma_MD[k]:.3f}, {sigma_QMI[k]:.3f}, {sigma_MI_optimal[k]:.3f}], [{dmu_MD[k]:.3f}, {dmu_QMI[k]:.3f}, {dmu_MI_optimal[k]:.3f}],[{dsigma_MD[k]:.3f}, {dsigma_QMI[k]:.3f}, {dsigma_MI_optimal[k]:.3f}], \"{folder}/{names[k]}_pull.png\", r\"$\\frac{{{xlabel[k]} - {xlabel[k]}^0}}{{\sigma {xlabel[k]}}}$\")\n"
        compPullPlots([mu_MD[k], mu_QMI[k], mu_MI_mod_optimal[k], mu_MI_optimal[k], mu_MI_equal[k]], [sigma_MD[k], sigma_QMI[k], sigma_MI_mod_optimal[k], sigma_MI_optimal[k], sigma_MI_equal[k]], [dmu_MD[k], dmu_QMI[k], dmu_MI_mod_optimal[k], dmu_MI_optimal[k], dmu_MI_equal[k]],[dsigma_MD[k], dsigma_QMI[k], dsigma_MI_mod_optimal[k], dsigma_MI_optimal[k], dsigma_MI_equal[k]], f"{folder}/{names[k]}_pull.png", rf"$\frac{{{xlabel[k]} - {xlabel[k]}^0}}{{\sigma {xlabel[k]}}}$")
        compValPlots([mean_MD[k], mean_QMI[k], mean_MI_mod_optimal[k], mean_MI_optimal[k], mean_MI_equal[k]], [std_MD[k], std_QMI[k], std_MI_mod_optimal[k], std_MI_optimal[k], std_MI_equal[k]], f"{folder}/{names[k]}_val.png", rf"${xlabel[k]}$", x0=x0[k], lims = [x0[k] - eps[k], x0[k] + eps[k]] )
        gstring += f"makeCompPlots.valPlots([{mean_MD[k]:.3f}, {mean_QMI[k]:.3f}, {mean_MI_optimal[k]:.3f}], [{std_MD[k]:.3f}, {std_QMI[k]:.3f}, {std_MI_optimal[k]:.3f}], \"{folder}/{names[k]}_val.png\", r\"${xlabel[k]}$\", x0={x0[k]:.3f})\n"



    with open(f"{folder}/comp.py", "w") as f:
        f.write(string)




def CKMStats(df):
    pull_xp = [(i.n + 0.094)/i.s for i in df["CKM::x+"]]
    pull_yp = [(i.n + 0.014)/i.s for i in df["CKM::y+"]]
    pull_xm = [(i.n - 0.059)/i.s for i in df["CKM::x-"]]
    pull_ym = [(i.n - 0.069)/i.s for i in df["CKM::y-"]]
    xp = [i.n for i in df["CKM::x+"]]
    yp = [i.n for i in df["CKM::y+"]]
    xm = [i.n for i in df["CKM::x-"]]
    ym = [i.n for i in df["CKM::y-"]]
    n = len(xp)
    
    m = { 
        "xp":np.mean(xp),
        "yp":np.mean(yp),
        "xm":np.mean(xm),
        "ym":np.mean(ym)
        }
    s = { 
        "xp":np.std(xp),
        "yp":np.std(yp),
        "xm":np.std(xm),
        "ym":np.std(ym)
        }

    pull_m = { 
        "xp":np.mean(pull_xp),
        "yp":np.mean(pull_yp),
        "xm":np.mean(pull_xm),
        "ym":np.mean(pull_ym)
        }
    pull_s = { 
        "xp":np.mean(pull_xp),
        "yp":np.mean(pull_yp),
        "xm":np.mean(pull_xm),
        "ym":np.mean(pull_ym)
        }

    pull_dm = { 
        "xp":np.std(pull_xp)/np.sqrt(n),
        "yp":np.std(pull_yp)/np.sqrt(n),
        "xm":np.std(pull_xm)/np.sqrt(n),
        "ym":np.std(pull_ym)/np.sqrt(n)
        }

    pull_ds = { 
        "xp":np.std(pull_xp)/np.sqrt(2*n),
        "yp":np.std(pull_yp)/np.sqrt(2*n),
        "xm":np.std(pull_xm)/np.sqrt(2*n),
        "ym":np.std(pull_ym)/np.sqrt(2*n)
        }

    return [m,s],[pull_m,pull_s,pull_dm,pull_ds]


def printConfTest(job):
    folder = f"{job.id}"
    p = lambda f :  pkl.load(open(f, "rb"))
    df_MD = p(glob.glob(f"{folder}/*MD.pkl")[0])
    df_QMI = p(glob.glob(f"{folder}/*BDK.pkl")[0]).query("Status!=3").sample(75)
    df_binned = p(glob.glob(f"{folder}/*binned.pkl")[0])
    df_binned = df_binned.query("fit_method=='combined'")
    O = max(df_QMI["Order"])
    df_QMI = df_QMI.query(f"Order=={O}")
#    makeCompParams(job, folder)
#    makePullPlots(df_MD, f"{folder}/MD")
#    makePullPlots(df_QMI, f"{folder}/qmi_{O}")
    pullsCKM(df_binned.query("binning_scheme=='equal'"))
    pullsCKM(df_binned.query("binning_scheme=='optimal'"))
    pullsCKM(df_binned.query("binning_scheme=='mod_optimal'"))
    pullsCKM(df_QMI.query("Status!=3"))
    pullsCKM(df_MD)



def rB(xp, yp, xm, ym):
    return 0.5 * (xp**2 + yp**2)**0.5 + 0.5 * (xm**2 + ym**2)**0.5
def deltaB(xp, yp, xm, ym):
    try:
        return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (xp * ym + xm* yp)/(xp*xm - yp * ym)) + math.pi))
    except:
        return uncertainties.ufloat(0,0)
def gamma(xp, yp, xm, ym):
    try:
        return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (-xp * ym + xm* yp)/(xp*xm + yp * ym)) + math.pi))
    except:
        return uncertainties.ufloat(0,0)



def makePTable(job, includeBinned=True, order=-1, usePolar=False, fit_method = "combined", MDJob = "", includeHFLAV=False):
    folder = f"{job.id}"
    print(f"{job.name}")
    p = lambda f :  pkl.load(open(f, "rb"))
    if MDJob:
        df_MD = p(glob.glob(f"{MDJob.id}/*MD.pkl")[0])
        df_binned = p(glob.glob(f"{MDJob.id}/*binned.pkl")[0])

    df_MD = p(glob.glob(f"{folder}/*MD.pkl")[0])
    df_QMI = p(glob.glob(f"{folder}/*BDK.pkl")[0]).query("Status!=3")
    df_binned = p(glob.glob(f"{folder}/*binned.pkl")[0])
    df_binned = df_binned.query("fit_method=='constant'")
    if order==-1:
        O = max(df_QMI["Order"])
    else:
        O = order
    print(f"N(QMI) = {len(df_QMI)}")
    df_QMI = df_QMI.query(f"Order=={O}").query("Status!=3")

    d0 = {"CKM::x+":-0.094, "CKM::y+":-0.014, "CKM::x-":0.059, "CKM::y-":0.069}
    dd0 = {"CKM::x+":0.010, "CKM::y+":0.012, "CKM::x-":0.010, "CKM::y-":0.011}
    #dd0 = {"CKM::x+":0.0, "CKM::y+":0.0, "CKM::x-":0., "CKM::y-":0.0}
    

    if usePolar:
        rB0 = rB(uncertainties.ufloat(-0.094, 0.010), uncertainties.ufloat(-0.014, 0.011), uncertainties.ufloat(0.059, 0.010), uncertainties.ufloat(0.069, 0.011))
        deltaB0 = deltaB(uncertainties.ufloat(-0.094, 0.010), uncertainties.ufloat(-0.014, 0.011), uncertainties.ufloat(0.059, 0.010), uncertainties.ufloat(0.069, 0.011))
        gamma0 = gamma(uncertainties.ufloat(-0.094, 0.010), uncertainties.ufloat(-0.014, 0.011), uncertainties.ufloat(0.059, 0.010), uncertainties.ufloat(0.069, 0.011))
        print(f"rB = {rB0.n:.3f} +- {rB0.s:.3f}")
        print(f"deltaB = {deltaB0.n:.1f} +- {deltaB0.s:.1f}")
        print(f"gamma = {gamma0.n:.1f} +- {gamma0.s:.1f}")
        d0 = {"CKM::rB" : rB0.n, "CKM::deltaB":deltaB0.n, "CKM::gamma":gamma0.n}
        dd0 = {"CKM::rB" : rB0.s, "CKM::deltaB":deltaB0.s, "CKM::gamma":gamma0.s}


    print(f"N = {len(df_QMI)}")
    pmuMD = {}
    pmuQMI={}
    pmuBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}

    psigmaMD = {}
    psigmaQMI={}
    psigmaBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}


    pull_MD = {}
    pull_QMI = {}
    pull_Binned = {"equal":{}, "optimal":{},"mod_optimal":{}}

    muMD = {}
    muQMI={}
    muBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}
    sigmaMD = {}
    sigmaQMI={}
    sigmaBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}
    dmuMD = {}
    dmuQMI={}
    dmuBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}
    dsigmaMD = {}
    dsigmaQMI={}
    dsigmaBinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}

    def makePull(df, k, x0):
        p = []
        for i in range(len(df[k])):
            if list(df[k])[i].s != 0:
                p += [(list(df[k])[i].n - x0[k])/list(df[k])[i].s]
        return p



    norm = scipy.stats.norm(0,1)

    def getP(x):
        return x
        #print(f"x = {x} {100 * (1 - norm.cdf(abs(x)))}")
        #return norm.cdf(-abs(x)) * 2
#        p0 = norm.cdf(x)
#        if x<0:
#            return p0
#        else:
#            return 1 - p0
#
    mustringMD = []
    mustringQMI = []
    mustringBinned = {"equal":[], "optimal":[], "mod_optimal":[]}
    

    sigmastringMD = []
    sigmastringQMI = []
    sigmastringBinned = {"equal":[], "optimal":[], "mod_optimal":[]}


    muValstringMD = []
    muValstringQMI = []
    muValstringBinned = {"equal":[], "optimal":[], "mod_optimal":[]}
    

   
    
    muValMD = {}
    sigmaValMD = {}
    muValQMI = {}
    sigmaValQMI = {}
    muValbinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}
    sigmaValbinned = {"equal":{}, "optimal":{}, "mod_optimal":{}}
    





    for k in d0: 
        pull_MD[k] = makePull(df_MD, k, d0) #[(df_MD[k][i].n - d0[k])/df_MD[k][i].s for i in range(len(df_MD[k]))]
        pull_QMI[k] = makePull(df_QMI, k, d0)#[(df_QMI[k][i].n - d0[k])/df_QMI[k][i].s for i in range(len(df_QMI[k]))]
        muValMD[k] = np.mean([i.n for i in df_MD[k]])
        sigmaValMD[k] = np.std([i.n for i in df_MD[k]])
        muValQMI[k] = np.mean([i.n for i in df_QMI[k]])
        sigmaValQMI[k] = np.std([i.n for i in df_QMI[k]])


        for b in ["equal", "optimal", "mod_optimal"]:
            df_binned_b = df_binned.query(f"binning_scheme=='{b}'")
            pull_Binned[b][k] = makePull(df_binned_b, k, d0)#[df_binned_b[k][i].n - d0[k])/df_binned_b[k][i].s for i in range(len(df_binned_b[k]))]
            muValbinned[b][k] = np.mean([i.n for i in df_binned_b[k]])
            sigmaValbinned[b][k] = np.std([i.n for i in df_binned_b[k]])



        muMD[k] = np.mean(pull_MD[k])
        sigmaMD[k] = np.std(pull_MD[k])
        dmuMD[k] = sigmaMD[k]/np.sqrt(len(pull_MD[k]))
        dsigmaMD[k] = dmuMD[k]/np.sqrt(2)
        muQMI[k] = np.mean(pull_QMI[k])
        sigmaQMI[k] = np.std(pull_QMI[k])
        dmuQMI[k] = sigmaQMI[k]/np.sqrt(len(pull_QMI[k]))
        dsigmaQMI[k] = dmuQMI[k]/np.sqrt(2)

        pmuMD[k] = ( getP(muMD[k]/dmuMD[k]) * 1)  
        psigmaMD[k] = (  getP( (sigmaMD[k] - 1)/dsigmaMD[k]) * 1) 

        pmuQMI[k] = (  getP(muQMI[k]/dmuQMI[k]) * 1) 
        psigmaQMI[k] = (  getP( (sigmaQMI[k] - 1)/dsigmaQMI[k]) * 1) 

        #mustringMD += [f"{1 * pmuMD[k]:.2f}"]
        mustringMD += [f"${1 * muMD[k]:.2f}\pm{dmuMD[k]:.2f}$"]
        #mustringQMI += [f"{1 * pmuQMI[k]:.2f}"]
        mustringQMI += [f"${1 * muQMI[k]:.2f}\pm{dmuQMI[k]:.2f}$"]

        #sigmastringMD += [f"{1 * psigmaMD[k]:.2f}"]
        sigmastringMD += [f"${sigmaMD[k]:.2f}\pm{dsigmaMD[k]:.2f}$"]
        #sigmastringQMI += [f"{1 * psigmaQMI[k]:.2f}"]
        sigmastringQMI += [f"${sigmaQMI[k]:.2f}\pm{dsigmaQMI[k]:.2f}$"]
 

        #mustringMD += [f"{1 * pmuMD[k]:.2f}"]
        if "deltaB" in k or "gamma" in k:
            muValstringMD += [f"${1 * muValMD[k]:.1f}\pm{sigmaValMD[k]:.1f}$"]
            muValstringQMI += [f"${1 * muValQMI[k]:.1f}\pm{sigmaValQMI[k]:.1f}$"]

        else:
            muValstringMD += [f"${1 * muValMD[k]:.3f}\pm{sigmaValMD[k]:.3f}$"]
            muValstringQMI += [f"${1 * muValQMI[k]:.3f}\pm{sigmaValQMI[k]:.3f}$"]
            
        #muValstringQMI += [f"{1 * pmuQMI[k]:.2f}"]




        for b in ["equal", "optimal", "mod_optimal"]:
            muBinned[b][k] = np.mean(pull_Binned[b][k])
            sigmaBinned[b][k] = np.std(pull_Binned[b][k])
            dmuBinned[b][k] = np.std(pull_Binned[b][k])/np.sqrt(len(pull_Binned[b][k]))
            dsigmaBinned[b][k] = dmuBinned[b][k]/np.sqrt(2) 

            pmuBinned[b][k] = (  getP(muBinned[b][k]/dmuBinned[b][k]) * 1) 
            psigmaBinned[b][k] = ( getP( (sigmaBinned[b][k] - 1)/dsigmaBinned[b][k]) * 1) 

            #mustringBinned[b] += [f"{1 * pmuBinned[b][k]:.2f}"]
            mustringBinned[b] += [f"${muBinned[b][k]:.2f}\pm{dmuBinned[b][k]:.2f}$"]
            #sigmastringBinned[b] += [f"{1 * psigmaBinned[b][k]:.2f}"]
            sigmastringBinned[b] += [f"${sigmaBinned[b][k]:.2f}\pm{dsigmaBinned[b][k]:.2f}$"]

            if "deltaB" in k or "gamma" in k:
                muValstringBinned[b] += [f"${muValbinned[b][k]:.1f}\pm{sigmaValbinned[b][k]:.1f}$"]
            else:
                muValstringBinned[b] += [f"${muValbinned[b][k]:.3f}\pm{sigmaValbinned[b][k]:.3f}$"]
            #sigmastringBinned[b] += [f"{1 * psigmaBinned[b][k]:.2f}"]

    muStringMD = "&".join(mustringMD)
    sigmaStringMD = "&".join(sigmastringMD)

    muStringQMI = "&".join(mustringQMI)
    sigmaStringQMI = "&".join(sigmastringQMI)
    muStringBinned = {}
    sigmaStringBinned = {}
    #headerLabelMu = lambda k : f"$\\frac{{\langle {k} \\rangle}}{{\sqrt{{s({k})/N}}}}$"
    headerLabelMu = lambda k : f"${k}$"
    #headerLabelSigma = lambda k : f"$\\frac{{ s({k})}}{{\sqrt{{s({k})/N}}}}$"
    headerLabelSigma = lambda k : f"${k}$"

    print(muValstringMD)
    print(muValstringQMI)

    muValstringMD = "&".join(muValstringMD)
    muValstringQMI = "&".join(muValstringQMI)

    print(muValstringMD)
    print(muValstringQMI)




    def headerLabelMu2(k):
        if k=="delta_B":
            return "$\delta_B"
        if k=="gamma":
            return "$\gamma$"
        else:
            return f"${k}$"

    if includeBinned:
        for b in ["equal", "optimal", "mod_optimal"]:
            muStringBinned[b] = "&".join(mustringBinned[b])
            muValstringBinned[b] = "&".join(muValstringBinned[b])
            sigmaStringBinned[b] = "&".join(sigmastringBinned[b])
    #    muProbTableString = f"""Method & $\frac{{\mu_{{x_+}}}}{{\sigma_{{x_+}}}} $ & $\frac{{\mu_{{y_+}}}}{{\sigma_{{y_+}}}}$ &  $\frac{{\mu_{{x_-}}}}{{\sigma_{{x_-}}}}$ &  $\frac{{\mu_{{y_-}}}}{{\sigma_{{y_-}}}}$ \\\\
#    muProbTableString = f"""Method & $\frac{{\langle q_{{x_+}} \\rangle \sqrt{N} }}{{\sqrt{{s(q_{{x_+}})}}}}$ & $\frac{{\langle q_{{y_+}} \\rangle \sqrt{N} }}{{\sqrt{{s(q_{{y_+}})}}}}$ & $\frac{{\langle q_{{x_-}} \\rangle \sqrt{N} }}{{\sqrt{{s(q_{{x_-}})}}}}$ & $\frac{{\langle q_{{y_+}} \\rangle \sqrt{N} }}{{\sqrt{{s(q_{{y_-}})}}}}$ \\\\ \hline

    muProbTableString = f"""Method & {headerLabelMu('x_+')} & {headerLabelMu('y_+')} & {headerLabelMu('x_-')} & {headerLabelMu('y_-')} \\\\\hline
"""


    sigmaProbTableString = f"""Method & {headerLabelSigma('x_+')} & {headerLabelSigma('y_+')} & {headerLabelSigma('x_-')} & {headerLabelSigma('y_-')} \\\\\hline
"""



    if usePolar:
        muProbTableString = f"""Method & {headerLabelMu2('r_B')} & {headerLabelMu2('delta_B')} & {headerLabelMu2('gamma')} \\\\\hline
"""
        sigmaProbTableString = f"""Method & {headerLabelMu2('r_B')} & {headerLabelMu2('delta_B')} & {headerLabelMu2('gamma')} \\\\\hline
"""

    muValTableString = muProbTableString


    if includeBinned:
        muProbTableString += f"""Binned Methods & \\\\
Equal Binning & {muStringBinned['equal']} \\\\
Optimal Binning & {muStringBinned['optimal']} \\\\
Modified Optimal Binning & {muStringBinned['mod_optimal']} \\\\
Unbinned Methods & \\\\
"""
        sigmaProbTableString += f"""Binned Methods & \\\\
Equal Binning & {sigmaStringBinned['equal']} \\\\
Optimal Binning & {sigmaStringBinned['optimal']} \\\\
Modified Optimal Binning & {sigmaStringBinned['mod_optimal']} \\\\
Unbinned Methods & \\\\
"""
        muValTableString += f"""Binned Methods & \\\\
Equal Binning & {muValstringBinned['equal']} \\\\
Optimal Binning & {muValstringBinned['optimal']} \\\\
Modified Optimal Binning & {muValstringBinned['mod_optimal']} \\\\
Unbinned Methods & \\\\
"""

    muProbTableString += f"""QMI Method & {muStringQMI} \\\\
MD Method & {muStringMD}"""
    sigmaProbTableString += f"""QMI Method & {sigmaStringQMI} \\\\
MD Method & {sigmaStringMD}"""

    muValTableString += f"""QMI Method & {muValstringQMI} \\\\
MD Method & {muValstringMD}"""


#    sigmaProbTableString = f"""Method & $P_0(\sigma_{{x_+}} - 1)$ & $P_0(\sigma_{{y_+}} - 1)$ & $P_0(\sigma_{{x_-}} - 1)$ & $P_0(\sigma_{{y_-}} - 1)$ \\\\hline


    
   # sigmaProbTableString = f"""Method & $\frac{{s(q_{{x_+})} \\rangle \sqrt{2N} }}{{s(q_{{x_+}})}}$ & $\frac{{\langle q_{{y_+}} \\rangle \sqrt{N} }}{{s(q_{{y_+}})}}$ & $\frac{{\langle q_{{x_-}} \\rangle \sqrt{N} }}{{s(q_{{x_-}})}}$ & $\frac{{\langle q_{{y_+}} \\rangle \sqrt{N} }}{{s(q_{{y_-}})}}$ \\\\ \hline

#    sigmaProbTableString = f"""Method & $\frac{{s(q_{{x_+}} ) \sqrt{N} }}{{\sqrt{{s(q_{{x_+}})}}}}$ & $\frac{{\langle q_{{y_+}} ) \sqrt{N} }}{{\sqrt{{s(q_{{y_+}})}}}}$ & $\frac{{\langle q_{{x_-}} ) \sqrt{N} }}{{\sqrt{{s(q_{{x_-}})}}}}$ & $\frac{{\langle q_{{y_+}} ) \sqrt{N} }}{{\sqrt{{s(q_{{y_-}})}}}}$ \\\\ \hline
    
    if includeBinned:
        muName = f"{folder}/muPullTable.tex"
        sigmaName = f"{folder}/sigmaPullTable.tex"
        muValName = f"{folder}/valTable.tex"

    else:
        muName = f"{folder}/muPullTableNoBinned.tex"
        sigmaName = f"{folder}/sigmaPullTableNoBinned.tex"
        muValName = f"{folder}/valTableNoBinned.tex"

    if usePolar:
        muName = muName.replace(".tex", "_polar.tex")
        muValName = muValName.replace(".tex", "_polar.tex")
        sigmaName = sigmaName.replace(".tex", "_polar.tex")
    else:
        muName = muName.replace(".tex", "_cart.tex")
        muValName = muValName.replace(".tex", "_cart.tex")
        sigmaName = sigmaName.replace(".tex", "_cart.tex")



    with open(muName, "w") as f:
        f.write(muProbTableString)

    with open(sigmaName, "w") as f:
        f.write(sigmaProbTableString)

    with open(muValName, "w") as f:
        f.write(muValTableString)


    print(muProbTableString)         
    print(sigmaProbTableString)         
    print(muValTableString)         

    import makeCompPlots
    xlabels = {
            "CKM::x+":"$x_+$",
            "CKM::x-":"$x_-$",
            "CKM::y+":"$y_+$",
            "CKM::y-":"$y_-$",
            "CKM::rB":"$r_B$",
            "CKM::deltaB":"$\delta_B$",
            "CKM::gamma":"$\gamma$"
            }
    xnames = {
            "CKM::x+":"xp",
            "CKM::x-":"xm",
            "CKM::y+":"yp",
            "CKM::y-":"ym",
            "CKM::rB":"rB",
            "CKM::deltaB":"deltaB",
            "CKM::gamma":"gamma"
            }
    for k in d0:
        outputPatternPull = f"{folder}/{xnames[k]}_pull"
        outputPatternVal = f"{folder}/{xnames[k]}_val"
        if not includeBinned:
            outputPatternPull += "_noBinned"
            outputPatternVal += "_noBinned"
        if usePolar:
            outputPatternPull += "_polar"
            outputPatternVal += "_polar"
        else:
            outputPatternPull += "_cart"
            outputPatternVal += "_cart"



        makeCompPlots.pullPlots(
                [muMD[k], muQMI[k], muBinned["mod_optimal"][k], muBinned["optimal"][k], muBinned["equal"][k]], 
                [sigmaMD[k], sigmaQMI[k], sigmaBinned["mod_optimal"][k], sigmaBinned["optimal"][k], sigmaBinned["equal"][k]], 
                [dmuMD[k], dmuQMI[k], dmuBinned["mod_optimal"][k], dmuBinned["optimal"][k], dmuBinned["equal"][k]], 
                [dsigmaMD[k], dsigmaQMI[k], dsigmaBinned["mod_optimal"][k], dsigmaBinned["optimal"][k], dsigmaBinned["equal"][k]], 
                f"{outputPatternPull}.png",
                xlabels[k],
                includeBinned=includeBinned)

       
        
        makeCompPlots.valPlots(
                [muValMD[k], muValQMI[k], muValbinned["mod_optimal"][k], muValbinned["optimal"][k], muValbinned["equal"][k]], 
                [sigmaValMD[k], sigmaValQMI[k], sigmaValbinned["mod_optimal"][k], sigmaValbinned["optimal"][k], sigmaValbinned["equal"][k]], 
                f"{outputPatternVal}.png", 
                xlabels[k],
                x0 = d0[k],
                dx0 = dd0[k],
                includeBinned=includeBinned,
                includeHFLAV=includeHFLAV)



        print(f"{outputPatternPull}")
        print(f"{outputPatternVal}")

#def pullPlots(m, s, dm, ds, output, xlabel, lims = [-3, 3], plotIdeal=True, includeBinned = True):
#    return [muStringMD, muStringQMI, muStringBinned], [sigmaStringMD, sigmaStringQMI, sigmaStringBinned] 
#    return [pmuMD, pmuQMI, pmuBinned],[psigmaMD, psigmaQMI,psigmaBinned]

            




    




def mainPlot(job, folder):
    p = lambda f :  pkl.load(open(f, "rb"))
    df_MD = p(glob.glob(f"{folder}/*MD.pkl")[0])
    df_QMI = p(glob.glob(f"{folder}/*BDK.pkl")[0]).query("Status!=3").sample(75)
    df_binned = p(glob.glob(f"{folder}/*binned.pkl")[0])
    df_binned = df_binned.query("fit_method=='combined'")
    O = max(df_QMI["Order"])
    df_QMI = df_QMI.query(f"Order=={O}")
    makeCompParams(job, folder)
    makePullPlots(df_MD, f"{folder}/MD")
    makePullPlots(df_QMI, f"{folder}/qmi_{O}")
    pullsCKM(df_QMI)

    makePullPlots(df_binned.query("binning_scheme=='equal'"), f"{folder}/binned_equal")
    makePullPlots(df_binned.query("binning_scheme=='optimal'"), f"{folder}/binned_optimal")
    makePullPlots(df_binned.query("binning_scheme=='mod_optimal'"), f"{folder}/binned_mod_optimal")

    makeValStatPlots(df_MD, f"{folder}/MD")
    makeValStatPlots(df_QMI, f"{folder}/qmi_{O}")
    makeValStatPlots(df_binned.query("binning_scheme=='equal'"), f"{folder}/binned_equal")
    makeValStatPlots(df_binned.query("binning_scheme=='optimal'"), f"{folder}/binned_optimal")
    makeValStatPlots(df_binned.query("binning_scheme=='mod_optimal'"), f"{folder}/binned_mod_optimal")



