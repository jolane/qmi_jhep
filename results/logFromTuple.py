import ROOT
import os
import pandas as pd
import uncertainties
import uncertainties.umath
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import math, cmath
import pickle as pkl
#fileName = "root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/640103/640103901/poly_4_belle_2018_HFLAV_gaussShift_deltaB_shift_psi3770_0_1000_1000_plot.root"
import sys
import uproot
import numpy as np
import time
import argparse


def dalitzLabel(k):
    if k == "s01":
        return r"$m_{K_S^0 \pi^+}^2$"
    if k=="s02":
        return r"$m_{K_S^0 \pi^-}^2$"
    if k=="s12":
        return r"$m_{\pi^+ \pi^-}^2$"
    if k=="s12Prime":
        return r"$m'_{\pi^+\pi^-}$"
    if k=="Delta":
        return r"$\Delta_{K_S^0 \pi}$"
    else:
        return k


def keyToTex(key):
    if "PhaseCorrection" in key:
        i = key.replace("PhaseCorrection::C", "").split("_")[0]
        j = key.replace("PhaseCorrection::C", "").split("_")[1]
        return "C_{" + i + ", " + j + "}" 
    if "CKM" in key:
        if "x+" in key:
            return "x_+"
        if "y+" in key:
            return "y_+"
        if "x-" in key:
            return "x_-"
        if "y-" in key:
            return "y_-"
        if "rB" in key:
            return "r_B"
        if "deltaB" in key:
            return "\delta_B"
        if "gamma" in key:
            return "\gamma"


    else:
        return key

def plotScatter(x, y, z, output, xlabel="", ylabel="", zlabel="", aspect="auto", normalize=True, percent_plot=20.):
    fig, ax = plt.subplots(1,1)



    if normalize:
        z = [cmath.phase(cmath.exp(1j*a)) for a in z]

        
    end_plot = int(percent_plot * len(x)/100.)
    sc = ax.scatter(x[0:end_plot], y[0:end_plot], c=z[0:end_plot], s=0.5)
    cb = plt.colorbar(sc, ax=ax, label=zlabel)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_aspect(aspect)
    fig.tight_layout()
    t = time.time()
    fig.savefig(output, dpi=300)
    print(f"Took {(time.time() - t)*1e3:.3f} to save {output}")
    plt.close()


def getDalitz(fileName):
    f = uproot.open(fileName)
    tree = f["Dalitz"]
    s01 = tree["s01"].array()
    s02 = tree["s02"].array()
    s12 = tree["s12"].array()
    return s01, s02, s12


def getX(fileName, name):
    f = uproot.open(fileName)
    tree = f[name]
    return tree["x"].array()

def DeltaKsPi(m12, m13, m23):
    mpi = 0.13957039
    mKs = 0.497611
    mD = 1.86484
    EKs = (mD**2 + mKs**2 - m23)/(2 * mD)
    EPiP = (mD**2 + mpi**2 - m13)/(2 * mD)
    EPiM = (mD**2 + mpi**2 - m12)/(2 *  mD)

    theta_KsPiP = np.arccos((mKs**2 + mpi**2 + 2 * EKs * EPiP - m12)/(2 * np.sqrt((EKs**2 - mKs**2) * (EPiP**2 - mpi**2))))
    theta_KsPiM = np.arccos((mKs**2 + mpi**2 + 2 * EKs * EPiM - m13)/(2 * np.sqrt((EKs**2 - mKs**2) * (EPiM**2 - mpi**2))))
    theta = theta_KsPiP - theta_KsPiM
    return theta/np.pi

def norm_mpipi(z):
    mpi = 0.13957039
    mKs = 0.497611
    mD = 1.86484
    x_max = (mD - mKs)**2
    x_min = 4 * mpi**2
    return 2 * (z - x_min)/(x_max - x_min) - 1


def tform(x):
    if x<10:
        return f"0{x}"
    else:
        return f"{x}"


def makeFolder():
    t = time.localtime()
    print(t)
    yy = t.tm_year
    MM = tform(t.tm_mon)
    dd = tform(t.tm_mday)
    hh = tform(t.tm_hour)
    mm = tform(t.tm_min)
    ss = tform(t.tm_sec)
    t = time.localtime()    
    my_folder = f"{yy}_{MM}_{dd}/{hh}_{mm}_{ss}"
    os.system(f"mkdir -p {my_folder}")
    return my_folder



def plotCorrection(fileName, my_folder):
    s01, s02, s12 = getDalitz(fileName)
    corr = getX(fileName, "corr")
    coscorr = np.cos(corr)
    sincorr = np.sin(corr)
    m12_prime = norm_mpipi(s12)
    Delta = DeltaKsPi(s01, s02, s12)
    m12_prime_label = dalitzLabel("s12Prime")
    Delta_label = dalitzLabel("Delta")
    f_label = r"$f(m_+^2, m_-^2)$"
    f_mod_pi_label = r"$f(m_+^2, m_-^2) \mathrm{mod} \pi$"
    cos_f_label = r"$\cos f(m_+^2, m_-^2)$"
    sin_f_label = r"$\sin f(m_+^2, m_-^2)$"
    plotScatter(m12_prime, Delta, corr, xlabel=m12_prime_label, ylabel=Delta_label, zlabel=f_label, output=my_folder  + "/" + fileName.split("/")[-1].replace(".root", "_corr.png"), normalize=False)
    plotScatter(m12_prime, Delta, corr, xlabel=m12_prime_label, ylabel=Delta_label, zlabel=f_mod_pi_label, output=my_folder +"/"+ fileName.split("/")[-1].replace(".root", "_corr_mod_pi.png"), normalize=True)
    plotScatter(m12_prime, Delta, coscorr, xlabel=m12_prime_label, ylabel=Delta_label, zlabel=cos_f_label, output=my_folder +"/"+ fileName.split("/")[-1].replace(".root", "_cos_corr.png"), normalize=False)
    plotScatter(m12_prime, Delta, sincorr, xlabel=m12_prime_label, ylabel=Delta_label, zlabel=sin_f_label, output=my_folder + "/" + fileName.split("/")[-1].replace(".root", "_sin_corr.png"), normalize=False)


 
   
    





def rB(xp, yp, xm, ym):
    return 0.5 * (xp**2 + yp**2)**0.5 + 0.5 * (xm**2 + ym**2)**0.5
def deltaB(xp, yp, xm, ym):
    try:
        return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (xp * ym + xm* yp)/(xp*xm - yp * ym)) + math.pi))
    except:
        return uncertainties.ufloat(0,0)
def gamma(xp, yp, xm, ym):
    try:
        return uncertainties.umath.degrees(0.5 * (uncertainties.umath.atan( (-xp * ym + xm* yp)/(xp*xm + yp * ym)) + math.pi))
    except:
        return uncertainties.ufloat(0,0)



def getChi2(fileName):
    f = ROOT.TFile.Open(fileName)
    keys = [k.GetName() for k in f.GetListOfKeys()]
    
    p = {}
    for k in keys:
        if "chi2" in k:
            chi2 = f.Get(k)[0]
            ndf = f.Get(k.replace("chi2", "ndf"))[0]
            p[k] = chi2
            p[k.replace("chi2", "ndf")] = ndf
    return p



    



def getResultsDict(fileName):
    f = ROOT.TFile.Open(fileName)
    fr = f.Get("FitResult")
    output = fileName.split("/")[-1].replace("plot.root", "results.pkl")
    order = 0
    seed=0
    if "poly_" in fileName:
        try:
            order = int(fileName.split("poly_")[-1].split("_")[0])
        except:
            order = 0
        try:
            if "BDK" in fileName:
                seed = int(fileName.split("_")[-5])
            if "psi3770" in fileName:
                seed = int(fileName.split("_")[-4])
        except:
            seed = -1



    xp0 = -0.094
    yp0 = -0.014
    xm0 = 0.059
    ym0 = 0.069
    status = fr.Status()
    nParams = fr.NPar()
    ndf = fr.Ndf()
    chi2 = fr.Chi2()
    edm = fr.Edm()
    p = {}
    fcn = fr.MinFcnValue()
    p["Seed"] = [seed]
    p["Order"] = [order]
    p["Status"] = [status]
    p["FCN"] = [fcn]
    p["edm"] = [edm]

    
    for i in range(nParams):
        p[fr.ParName(i)] = [uncertainties.ufloat(fr.Parameter(i), fr.Error(i))]
        if fr.HasMinosError(i):
            p[fr.ParName(i) + "_minos_lower"] = [fr.LowerError(i)]
            p[fr.ParName(i) + "_minos_upper"] = [fr.UpperError(i)]


    keys = [k.GetName() for k in f.GetListOfKeys()]
    for k in keys:
        if "chi2" in k:
            chi2 = f.Get(k)[0]
            ndf = f.Get(k.replace("chi2", "ndf"))[0]
            p[k] = chi2
            p[k.replace("chi2", "ndf")] = ndf
    doCKM = False
    for k in p:
        if "CKM" in k:
            doCKM = True

    if doCKM:

        def findCKM(fr, p):
            for i in range(fr.NPar()):
                if p in fr.ParName(i):
                    return i

        def CKMIndex(fr, i):
            if i==0:
                return findCKM(fr, "x+")
            if i==1:
                return findCKM(fr,"y+")
            if i==2:
                return findCKM(fr, "x-")
            if i==3:
                return findCKM(fr, "y-")
            
           
        xp = p["CKM::x+"][0]
        yp = p["CKM::y+"][0]
        xm = p["CKM::x-"][0]
        ym = p["CKM::y-"][0]
        rBFit = rB(xp, yp, xm ,ym)
        deltaBFit = deltaB(xp, yp, xm ,ym)
        gammaFit = gamma(xp, yp, xm ,ym)

        drBFit = 0
        ddeltaBFit = 0
        dgammaFit = 0
        derivrB = list(rBFit.derivatives.values())
        derivdeltaB = list(deltaBFit.derivatives.values())
        derivgamma = list(gammaFit.derivatives.values())
        for i in range(len(derivrB)):
            for j in range(len(derivrB)):
                CKMi = CKMIndex(fr, i)
                CKMj = CKMIndex(fr, j)
                try:
                    drBFit += derivrB[i] * fr.CovMatrix(CKMi, CKMj) * derivrB[j]
                except:
                    drBFit += 0
                try:
                    ddeltaBFit += derivdeltaB[i] * fr.CovMatrix(CKMi, CKMj) * derivdeltaB[j]
                except:
                    ddeltaBFit += 0
                try:
                    dgammaFit += derivgamma[i] * fr.CovMatrix(CKMi, CKMj) * derivgamma[j]
                except:
                    dgammaFit += 0

        rBFit2 = uncertainties.ufloat(rBFit.n, drBFit**0.5)
        deltaBFit2 = uncertainties.ufloat(deltaBFit.n, ddeltaBFit**0.5)
        gammaFit2 = uncertainties.ufloat(gammaFit.n, dgammaFit**0.5)
        
       

        p["CKM::rB"] = rBFit2
        p["CKM::deltaB"] = deltaBFit2
        p["CKM::gamma"] = gammaFit2
    return p
def getResults(fileName):
    p = getResultsDict(fileName)
    df = pd.DataFrame(p)
    return df

def mergePsiAndBDK(psiFile, BDKFile):
    dict_psi = getResultsDict(psiFile)
    dict_BDK = getResultsDict(BDKFile)
    dict_full = {}
    for key in dict_psi:
        dict_full[f"psi3770_{key}"] = dict_psi[key]
    for key in dict_BDK:
        dict_full[f"BDK_{key}"] = dict_BDK[key]
    return pd.DataFrame(dict_full)


def concatResults(fileNames):
    dfs = []
    for i,fileName in enumerate(fileNames):
        r = getResults(fileName)
        if r["Seed"][0] == -1:
           # print(r["Seed"])
            r["Seed"] = i+1
        dfs += [r]
    return pd.concat(dfs, sort=True)


def makeTableHeader(CKM=False):
    if CKM:
        row = "Order &"
        row += "$x_+$&"
        row += "$y_+$&"
        row += "$x_-$&"
        row += "$y_-$&"
        row += "$r_B$&"
        row += "$\delta_B$&"
        row += "$\gamma$&"
        row += "$\chi^2_{B^+}/n^\mathrm{dof}_{B^+}$ &" 
        row += "$\chi^2_{B^-}/n^\mathrm{dof}_{B^-}$" 

        return row
    else:
        row = "Order &"
        row += "$\chi^2_{K^+ K^-}/n^\mathrm{dof}_{K^+ K^-}$ &" 
        row += "$\chi^2_{K_S^0 \pi^0}/n^\mathrm{dof}_{K_S^0 \pi^0}$ &" 
        row += "$\chi^2_{K_S^0 \pi^+ \pi^-}/n^\mathrm{dof}_{K_S^0 \pi^+ \pi^-}$" 
        return row

def makeTableRow(df, string, CKM=False):
    df_row = df.query(string)
    if CKM:
        print(df_row) 
        o = int(list(df_row["Order"])[0])
        chi2_Bp2DKp = df_row["Bp2DKp_chi2"][0]
        
        ndf_Bp2DKp = int(df_row["Bp2DKp_ndf"][0])
        chi2_Bm2DKm = float(df_row["Bm2DKm_chi2"][0])
        ndf_Bm2DKm = int(df_row["Bm2DKm_ndf"][0])
        xp = df_row["CKM::x+"][0].n
        dxp = df_row["CKM::x+"][0].s
        yp = df_row["CKM::y+"][0].n
        dyp = df_row["CKM::y+"][0].s
        xm = df_row["CKM::x-"][0].n
        dxm = df_row["CKM::x-"][0].s
        ym = df_row["CKM::y-"][0].n
        dym = df_row["CKM::y-"][0].s
        rb = df_row["CKM::rB"][0].n
        drb = df_row["CKM::rB"][0].s
        db = df_row["CKM::deltaB"][0].n
        ddb = df_row["CKM::deltaB"][0].s
        g = df_row["CKM::gamma"][0].n
        dg = df_row["CKM::gamma"][0].s

        row = f"${o}$&"
        row += f"${xp:.3f}\pm{dxp:.3f}$&"
        row += f"${yp:.3f}\pm{dyp:.3f}$&"
        row += f"${xm:.3f}\pm{dxm:.3f}$&"
        row += f"${ym:.3f}\pm{dym:.3f}$&"
        row += f"${rb:.3f}\pm{drb:.3f}$&"
        row += f"${db:.1f}\pm{ddb:.1f}$&"
        row += f"${g:.1f}\pm{dg:.1f}$&"
        row += f"${chi2_Bp2DKp:.3f}/{ndf_Bp2DKp}$&"
        row += f"${chi2_Bm2DKm:.3f}/{ndf_Bm2DKm}$"

        return row

    else:

        o = int(df_row["Order"][0])
        chi2_KK = float(df_row["KK_chi2"][0])

        ndf_KK = int(df_row["KK_ndf"][0])
        chi2_Kspi0 = float(df_row["Kspi0_chi2"][0])
        ndf_Kspi0 = int(df_row["Kspi0_ndf"][0])
        chi2_Kspipi = float(df_row["Kspipi_chi2"][0])
        ndf_Kspipi = int(df_row["Kspipi_ndf"][0])
        row = f"${o}$&"
        row += f"${chi2_KK:.3f}/{ndf_KK}$&"
        row += f"${chi2_Kspi0:.3f}/{ndf_Kspi0}$&"
        row += f"${chi2_Kspipi:.3f}/{ndf_Kspipi}$"
        return row

def makeTable(df, output, CKM=False):
    table = makeTableHeader(CKM) + "\\\\\n"
    Omin = min(list(df["Order"]))
    Omax = max(list(df["Order"]))
    for i in range(Omin, Omax + 1):
        row = makeTableRow(df, f"Order=={i}", CKM)
        table += f"{row}\\\\\n"
    print(table)
    with open(output, "w") as f:
        f.write(table)
    return table



fileNames_psi3770 = []
fileNames_BDK = []
for i in range(1, 7):
    fileNames_psi3770 += [f"root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/640103/640103901/poly_{i}_belle_2018_HFLAV_gaussShift_deltaB_shift_psi3770_0_1000_1000_plot.root"]
    fileNames_BDK += [f"root://x509up_u118465@eoslhcb.cern.ch//eos/lhcb/grid/user/lhcb/user/j/jolane/2022_07/640103/640103901/poly_{i}_belle_2018_HFLAV_gaussShift_deltaB_shift_BDK_0_1000_1000_bound_plot.root"]

#df_psi3770 = concatResults(fileNames_psi3770)
#df_BDK = concatResults(fileNames_BDK)
#my_folder = makeFolder()
#makeTable(df_psi3770, f"{my_folder}/psi3770.tex", False)
#makeTable(df_BDK, f"{my_folder}/BDK.tex", True)
#
#for k in df_psi3770:
#    print(k)
#for k in df_BDK:
#    print(f"{k}")
#



if __name__ == "__main__":
    print(time.localtime())

    
    
    try:
        doPlot = sys.argv[1]
    except:
        doPlot = "False"

    try:
        my_folder = sys.argv[2]
    except:
        my_folder = makeFolder()


    try:
        output = sys.argv[3]
    except:
        output = ""

    try:
        fileNames = sys.argv[4:]
    except:
        fileNames = []
    if len(fileNames) != 0:
        
        

        df = concatResults(fileNames)
        #my_folder = makeFolder()
        
        os.system(f"mkdir -p {my_folder}")
        with open(my_folder + "/" + output, "wb") as f:
            pkl.dump(df, f)
        print(my_folder + "/" + output)
        CKM = "CKM::x+" in df.keys()
        print(df.keys())
       

        if doPlot != "False":
            for fileName in fileNames:
                #plotCorrection(fileName, my_folder)
                if CKM: 
                    makeTable(df, my_folder + "/" + fileName.split("/")[-1].replace(".root", "_BDK.tex"), CKM)
                else:
                    makeTable(df, my_folder + "/" + fileName.split("/")[-1].replace(".root", "psi3770.tex"), CKM)
     




#
