from matplotlib import pyplot as plt
from matplotlib import patches

import numpy as np

def pullPlots(m, s, dm, ds, output, xlabel, lims = [-3, 3], plotIdeal=True, includeBinned = True):
    fig, ax = plt.subplots(1,1)

    if includeBinned:
        n = 5
        ax.set_yticks([0,1,2, 3, 4])
        ax.set_yticklabels(["MD", "QMI", """Binned
Modified Optimal""","""Binned
Optimal""","""Binned
Equal"""])
    else:
        n = 2
        ax.set_yticks([0,1])
        ax.set_yticklabels(["MD", "QMI"])



    if plotIdeal:
        ax.plot([-1,-1],[-1,n],linestyle="dashed", color="red")
        ax.plot([1,1],[-1,n],linestyle="dashed", color="red")
        ax.plot([0,0],[-1,n],linestyle="dashed", color="red")
    h = 0.25
    
    x_max_R = max(np.array(m) + np.array(dm) + np.array(s) + np.array(ds)) 
    x_max_L = abs(min(np.array(m) - np.array(dm) - np.array(s) - np.array(ds)))
    x_max = max([x_max_L, x_max_R])
    lims = [-x_max, x_max]
    

    ax.set_xlim(lims)
    ax.set_ylim([-1,n])
    print(m[0] - s[0] - ds[0]) 
    r1_l = patches.Rectangle((m[0] - s[0] - ds[0], 0 - h/2), 2 * ds[0], h, color="black")
    r1_m = patches.Rectangle((m[0] - dm[0], 0 - h/2), 2 * dm[0], h, color="black")
    r1_r = patches.Rectangle((m[0] + s[0] - ds[0], 0 - h/2), 2 * ds[0], h, color="black")

    ax.add_patch(r1_l)
    ax.add_patch(r1_m)
    ax.add_patch(r1_r)

    r2_l = patches.Rectangle((m[1] - s[1] - ds[1], 1 - h/2), 2 * ds[1], h, color="black")
    r2_m = patches.Rectangle((m[1] - dm[1], 1 - h/2), 2 * dm[1], h, color="black")
    r2_r = patches.Rectangle((m[1] + s[1] - ds[1], 1 - h/2), 2 * ds[1], h, color="black")


    ax.add_patch(r2_l)

    ax.add_patch(r2_m)
    ax.add_patch(r2_r)
    
    if includeBinned:
        for i in range(2, 5):


            r3_l = patches.Rectangle((m[i] - s[i] - ds[i], i - h/2), 2 * ds[i], h, color="black")
            r3_m = patches.Rectangle((m[i] - dm[i], i - h/2), 2 * dm[i], h, color="black")
            r3_r = patches.Rectangle((m[i] + s[i] - ds[i], i - h/2), 2 * ds[i], h, color="black")

            ax.plot([m[i] + dm[i], m[i] + s[i] - ds[i]], [i,i], color="black")
            ax.plot([m[i] - dm[i], m[i] - s[i] + ds[i]], [i,i], color="black")
            ax.add_patch(r3_l)
            ax.add_patch(r3_m)
            ax.add_patch(r3_r)



    ax.plot([m[0] + dm[0], m[0] + s[0] - ds[0]], [0,0], color="black")
    ax.plot([m[0] - dm[0], m[0] - s[0] + ds[0]], [0,0], color="black")

    ax.plot([m[1] + dm[1], m[1] + s[1] - ds[1]], [1,1], color="black")
    ax.plot([m[1] - dm[1], m[1] - s[1] + ds[1]], [1,1], color="black")



    ax.set_xlabel(xlabel)




    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()

def valPlots(m, s, output, xlabel, lims = [-3, 3], plotIdeal=True, x0=0, dx0=0,  includeBinned = True, includeHFLAV=False ):
    fig, ax = plt.subplots(1,1)
    ax.set_yticks([0,1,2])
    ax.set_yticklabels(["MD", "QMI", "Binned"])
    if includeBinned:
        n = 5
        ax.set_yticks([0,1,2, 3, 4])
        ax.set_yticklabels(["MD", "QMI", """Binned
Modified Optimal""","""Binned
Optimal""","""Binned
Equal"""])
    else:
        n = 2
        ax.set_yticks([0,1])
        ax.set_yticklabels(["MD", "QMI"])


    if plotIdeal:
        ax.plot([x0,x0],[-1,n],linestyle="dashed", color="red")
        if dx0 and includeHFLAV: 
            dR = patches.Rectangle((x0 - dx0, -1 ), 2 * dx0, n+1, color="red", alpha=0.25)
            ax.add_patch(dR)
 
    x_max_R = max(np.array(m) +  3 * np.array(s) ) 

    x_max_L = min(np.array(m) - 3 * np.array(s) )
    print(f"xlims : {x_max_R}, {x_max_L}")
    print(f"{m}, {s}")
#    x_max = max([x_max_L, x_max_R])
    lims = sorted([x_max_L, x_max_R])
    
    
    ax.set_xlim(lims)
    ax.set_ylim([-1,n])

    ax.errorbar([m[0]], [0], xerr=[s[0]], color="black", fmt=".", capsize=5)
    ax.errorbar([m[1]], [1], xerr=[s[1]], color="black", fmt=".", capsize=5)
#    ax.errorbar([m[2]], [2], xerr=[s[2]], color="black", fmt=".", capsize=5)
    if includeBinned:
        for i in range(2, 5):
            ax.errorbar([m[i]], [i], xerr=[s[i]], color="black", fmt=".", capsize=5)
    ax.set_xlabel(xlabel)
    fig.tight_layout()
    fig.savefig(output, dpi=300)
    plt.close()


 


def plotMain(binned, qmi, md, output, xlabel, lims=[-3,3], plotIdeal=True):
    main([binned[0], qmi[0], md[0]], [binned[1], qmi[1], md[1]], [binned[2],qmi[2],md[2]],[binned[3],qmi[3],md[3]], output, xlabel, lims, plotIdeal)

#if __name__=="__main__":
#    main([0,0,0], [0.1,0.1,0.1], [1,1,1], [0.1, 0.1,0.1], "test.png", r"$x$")
#
#    optimal_pull_xm = [-0.07, 0.12, 1.23, 0.09]
#    optimal_pull_xp = [0.32, 0.09, 0.93, 0.07]
#    optimal_pull_ym = [-0.02, 0.11, 1.11, 0.08]
#    optimal_pull_yp = [-0.19, 0.11, 1.11, 0.08]
#
#    qmi_pull_xm = [ 0.13, 0.11, 1.05, 0.07]
#    qmi_pull_xp = [ 0.01, 0.08, 0.85, 0.06]
#    qmi_pull_ym = [ 0.09, 0.12, 1.15, 0.08]
#    qmi_pull_yp = [0.04, 0.10, 1.00, 0.07]
#
#    md_pull_xm = [-0.03, 0.11, 1.10, 0.08]
#    md_pull_xp = [0.87, 0.09, 0.85, 0.06]
#    md_pull_ym = [-1.34, 0.11, 1.14, 0.08]
#    md_pull_yp = [1.2, 0.1, 1.02, 0.07]
#
#
#    optimal_val_xm = [0.057, 0, 0.013, 0]
#    optimal_val_xp = [-0.091, 0, 0.008, 0]
#    optimal_val_ym = [0.066, 0, 0.015, 0]
#    optimal_val_yp = [-0.016, 0, 0.014, 0]
#
#    qmi_val_xm = [0.058, 0, 0.010, 0]
#    qmi_val_xp = [-0.095, 0, 0.006, 0]
#    qmi_val_ym = [0.069, 0, 0.010, 0]
#    qmi_val_yp = [-0.013, 0, 0.011, 0]
#
#    md_val_xm = [0.058, 0, 0.009, 0]
#    md_val_xp = [-0.095, 0, 0.006, 0]
#    md_val_ym = [0.068, 0, 0.010, 0]
#    md_val_yp = [-0.013, 0, 0.011, 0]
#
#
#    plotMain(optimal_pull_xm, qmi_pull_xm, md_pull_xm, "xm_pull.png", r"$\frac{x_- - x_-^0}{\sigma x_-}$")
#    plotMain(optimal_pull_xp, qmi_pull_xp, md_pull_xp, "xp_pull.png", r"$\frac{x_+ - x_+^0}{\sigma x_+}$")
#    plotMain(optimal_pull_ym, qmi_pull_ym, md_pull_ym, "ym_pull.png", r"$\frac{y_- - y_-^0}{\sigma y_-}$")
#    plotMain(optimal_pull_yp, qmi_pull_yp, md_pull_yp, "yp_pull.png", r"$\frac{y_+ - y_+^0}{\sigma y_+}$")
#    plotMain(optimal_val_xm, qmi_val_xm, md_val_xm, "xm_val.png", r"$x_-$", lims=[0.025, 0.1], plotIdeal=False)
#    plotMain(optimal_val_xp, qmi_val_xp, md_val_xp, "xp_val.png", r"$x_+$", lims =[-0.125, -0.05], plotIdeal =False)
#    plotMain(optimal_val_ym, qmi_val_ym, md_val_ym, "ym_val.png", r"$y_-$", lims = [0.025, 0.1], plotIdeal = False)
#    plotMain(optimal_val_yp, qmi_val_yp, md_val_yp, "yp_val.png", r"$y_+$", lims = [-0.05, 0.025], plotIdeal=False)
#





