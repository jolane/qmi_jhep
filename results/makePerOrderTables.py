import subprocess
import os,sys

def makeTableRow(fitFile, tableFile, rowEntry):
    os.system(f"root -l -q -b 'getFitResults.C(\"{fitFile}\", \"{tableFile}\", \"{rowEntry}\")'")
    with open(tableFile, "a+") as f2:
        f2.write("\n")



def makeTableRowPolar(fitFile, tableFile, rowEntry):
    from logFromTuple import getResultsDict, getChi2
    p = getResultsDict(fitFile)
    xpEntry = f"${p['CKM::x+'][0].n:.3f}\\pm{p['CKM::x+'][0].s:.3f}$"
    ypEntry = f"${p['CKM::y+'][0].n:.3f}\\pm{p['CKM::y+'][0].s:.3f}$"
    xmEntry = f"${p['CKM::x-'][0].n:.3f}\\pm{p['CKM::x-'][0].s:.3f}$"
    ymEntry = f"${p['CKM::y-'][0].n:.3f}\\pm{p['CKM::y-'][0].s:.3f}$"
    rBEntry = f"${p['CKM::rB'].n:.3f}\\pm{p['CKM::rB'].s:.3f}$"
    chi2 = getChi2(fitFile)
    print(p["Status"])

    psipp_chi2 = chi2["KK_chi2"] + chi2["Kspi0_chi2"] + chi2["Kspipi_chi2"]
    psipp_ndf = chi2["KK_ndf"] + chi2["Kspi0_ndf"] + chi2["Kspipi_ndf"]

    BDK_chi2 = chi2["Bp2DKp_chi2"] + chi2["Bm2DKm_chi2"]
    BDK_ndf = chi2["Bp2DKp_ndf"] + chi2["Bm2DKm_ndf"]

    psippChi2Entry = f"${psipp_chi2:.1f}/{int(psipp_ndf)}$"
    BDKChi2Entry = f"${BDK_chi2:.1f}/{int(BDK_ndf)}$"

    deltaBEntry = f"${p['CKM::deltaB'].n:.1f}\\pm{p['CKM::deltaB'].s:.1f}$"
    gammaEntry = f"${p['CKM::gamma'].n:.1f}\\pm{p['CKM::gamma'].s:.1f}$"
    stringCart = f"{rowEntry}&{psippChi2Entry}&{BDKChi2Entry}&{xpEntry}&{ypEntry}&{xmEntry}&{ymEntry}\\\\"
    stringPolar = f"{rowEntry}&{psippChi2Entry}&{BDKChi2Entry}&{rBEntry}&{deltaBEntry}&{gammaEntry}\\\\"



    with open(tableFile.replace(".tex", "_cart.tex"), "a+") as f:
        f.write(stringCart)
        f.write("\n")
    
    with open(tableFile.replace(".tex", "_polar.tex"), "a+") as f:
        f.write(stringPolar)
        f.write("\n")
 






def main(fileName):
    tableFile = fileName.replace(".txt", ".tex")
    if os.path.isfile(tableFile):
        os.system(f"rm {tableFile}")
        os.system(f"rm {tableFile.replace('.tex','_cart.tex')}")
        os.system(f"rm {tableFile.replace('.tex','_polar.tex')}")
        os.system(f"touch {tableFile}")
        os.system(f"touch {tableFile.replace('.tex','_cart.tex')}")
        os.system(f"touch {tableFile.replace('.tex','_polar.tex')}")

    with open(fileName) as f:

        for line in f:
            
            try:
                infile = line.split()[0]
                i = line.split()[1]
            except:
                infile = ""
                i=0


            #i = infile.split("/")[-1].replace("poly_", "").split("_")[0]

            if infile:
                makeTableRowPolar(infile, tableFile, i)

            

try:
    infile = sys.argv[-1]
except:
    infile = ""
if infile:
    main(infile)
