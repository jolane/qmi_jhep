import numpy as np

def getMinEDM(df):
    return df[df.edm==df.edm.min()]

def unwrap_uc(X):
    x = []
    dx = []
    for i in X:
        x += [i.n]
        dx += [i.s]
    return np.array(x), np.array(dx)

def unwrap_slice(df_slice):
    xp, dxp = unwrap_uc(df_slice["CKM::x+"])
    yp, dyp = unwrap_uc(df_slice["CKM::y+"])
    xm, dxm = unwrap_uc(df_slice["CKM::x-"])
    ym, dym = unwrap_uc(df_slice["CKM::y-"])
    #return {"xp":[xp,dxp],"yp":[yp,dyp], "xm":[xm,dxm], "ym":[ym,dym]}
    return xp, dxp, yp, dyp, xm, dxm, ym, dym
    
     

    
def makeResidual(df_slice, X0 = [-0.094, -0.014, 0.059, 0.069]):
    
    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    delta_xp = np.mean(xp) - X0[0]
    sigma_xp = np.std(xp)

    delta_yp = np.mean(yp) - X0[1]
    sigma_yp = np.std(yp)

    delta_xm = np.mean(xm) - X0[2]
    sigma_xm = np.std(xm)

    delta_ym = np.mean(ym) - X0[3]
    sigma_ym = np.std(ym)

    entry_xp = f"${delta_xp*100:.2f} \\pm {sigma_xp*100:.2f}$"
    entry_yp = f"${delta_yp*100:.2f} \\pm {sigma_yp*100:.2f}$"
    entry_xm = f"${delta_xm*100:.2f} \\pm {sigma_xm*100:.2f}$"
    entry_ym = f"${delta_ym*100:.2f} \\pm {sigma_ym*100:.2f}$"

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table



def makePull(df_slice, X0 = [-0.094, -0.014, 0.059, 0.069]):
    
    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    delta_xp = np.mean(xp) - X0[0]
    sigma_xp = np.std(xp)

    delta_yp = np.mean(yp) - X0[1]
    sigma_yp = np.std(yp)

    delta_xm = np.mean(xm) - X0[2]
    sigma_xm = np.std(xm)

    delta_ym = np.mean(ym) - X0[3]
    sigma_ym = np.std(ym)

    pull_xp = (xp - X0[0])/dxp
    pull_yp = (yp - X0[1])/dyp
    pull_xm = (xm - X0[2])/dxm
    pull_ym = (ym - X0[3])/dym

    mean_xp = np.mean(pull_xp)
    mean_yp = np.mean(pull_yp)
    mean_xm = np.mean(pull_xm)
    mean_ym = np.mean(pull_ym)

    sigma_xp = np.std(pull_xp)
    sigma_yp = np.std(pull_yp)
    sigma_xm = np.std(pull_xm)
    sigma_ym = np.std(pull_ym)


    dmean_xp = sigma_xp/np.sqrt(len(xp))
    dmean_yp = sigma_yp/np.sqrt(len(yp))
    dmean_xm = sigma_xm/np.sqrt(len(xm))
    dmean_ym = sigma_ym/np.sqrt(len(ym))

    dsigma_xp = dmean_xp/np.sqrt(2)
    dsigma_yp = dmean_yp/np.sqrt(2)
    dsigma_xm = dmean_xm/np.sqrt(2)
    dsigma_ym = dmean_ym/np.sqrt(2)

    entry_xp = f"${mean_xp:.3f}\\pm{dmean_xp:.3f}, {sigma_xp:.3f} \\pm {dsigma_xp:.3f}$"
    entry_yp = f"${mean_yp:.3f}\\pm{dmean_yp:.3f}, {sigma_yp:.3f} \\pm {dsigma_yp:.3f}$"
    entry_xm = f"${mean_xm:.3f}\\pm{dmean_xm:.3f}, {sigma_xm:.3f} \\pm {dsigma_xm:.3f}$"
    entry_ym = f"${mean_ym:.3f}\\pm{dmean_ym:.3f}, {sigma_ym:.3f} \\pm {dsigma_ym:.3f}$"
    
   
  

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table




def makePullSmall(df_slice, X0 = [-0.094, -0.014, 0.059, 0.069]):
    
    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    delta_xp = np.mean(xp) - X0[0]
    sigma_xp = np.std(xp)

    delta_yp = np.mean(yp) - X0[1]
    sigma_yp = np.std(yp)

    delta_xm = np.mean(xm) - X0[2]
    sigma_xm = np.std(xm)

    delta_ym = np.mean(ym) - X0[3]
    sigma_ym = np.std(ym)

    pull_xp = (xp - X0[0])/dxp
    pull_yp = (yp - X0[1])/dyp
    pull_xm = (xm - X0[2])/dxm
    pull_ym = (ym - X0[3])/dym

    mean_xp = np.mean(pull_xp)
    mean_yp = np.mean(pull_yp)
    mean_xm = np.mean(pull_xm)
    mean_ym = np.mean(pull_ym)

    sigma_xp = np.std(pull_xp)
    sigma_yp = np.std(pull_yp)
    sigma_xm = np.std(pull_xm)
    sigma_ym = np.std(pull_ym)


    dmean_xp = sigma_xp/np.sqrt(len(xp))
    dmean_yp = sigma_yp/np.sqrt(len(yp))
    dmean_xm = sigma_xm/np.sqrt(len(xm))
    dmean_ym = sigma_ym/np.sqrt(len(ym))

    dsigma_xp = dmean_xp/np.sqrt(2)
    dsigma_yp = dmean_yp/np.sqrt(2)
    dsigma_xm = dmean_xm/np.sqrt(2)
    dsigma_ym = dmean_ym/np.sqrt(2)

    entry_xp = f"${mean_xp:.3f}\\pm{sigma_xp:.3f}$"
    entry_yp = f"${mean_yp:.3f}\\pm{sigma_yp:.3f}$"
    entry_xm = f"${mean_xm:.3f}\\pm{sigma_xm:.3f}$"
    entry_ym = f"${mean_ym:.3f}\\pm{sigma_ym:.3f}$"
    
   
  

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table
   
def makePrecision(df_slice):

    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    
    entry_xp = f"${np.mean(dxp)*100:.3f}$"
    entry_yp = f"${np.mean(dyp)*100:.3f}$"
    entry_xm = f"${np.mean(dxm)*100:.3f}$"
    entry_ym = f"${np.mean(dym)*100:.3f}$"
#    entry_xp = f"${np.std(xp)*100:.1f}$"
#    entry_yp = f"${np.std(yp)*100:.1f}$"
#    entry_xm = f"${np.std(xm)*100:.1f}$"
#    entry_ym = f"${np.std(ym)*100:.1f}$"
#

    #entry_xp = f"${dxp[0]*100:.2f}$"
    #entry_yp = f"${dyp[0]*100:.2f}$"
    #entry_xm = f"${dxm[0]*100:.2f}$"
    #entry_ym = f"${dym[0]*100:.2f}$"

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table


def makeGammaPrecision(df_slice):
    rB, drB = unwrap_uc(df_slice["CKM::rB"])
    deltaB, ddeltaB = unwrap_uc(df_slice["CKM::deltaB"])
    gamma, dgamma = unwrap_uc(df_slice["CKM::gamma"])
    entry_gamma = f"${np.mean(dgamma):.3f}$"
    entry_deltaB = f"${np.mean(ddeltaB):.3f}$"
    entry_rB = f"${100 * np.mean(drB):.3f}$"
    
    #entry_gamma = f"${dgamma[0]:.3f}$"
#    entry_gamma = f"${np.std(gamma):.1f}$"
    table = f"{entry_rB}&{entry_deltaB}&{entry_gamma}"
    return table



   
def makePrecisionStdev(df_slice, scale =1):

    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    
    entry_xp = f"${np.std(dxp)*100*scale:.3f}$"
    entry_yp = f"${np.std(dyp)*100*scale:.3f}$"
    entry_xm = f"${np.std(dxm)*100*scale:.3f}$"
    entry_ym = f"${np.std(dym)*100*scale:.3f}$"
#    entry_xp = f"${np.std(xp)*100:.1f}$"
#    entry_yp = f"${np.std(yp)*100:.1f}$"
#    entry_xm = f"${np.std(xm)*100:.1f}$"
#    entry_ym = f"${np.std(ym)*100:.1f}$"
#

    #entry_xp = f"${dxp[0]*100:.2f}$"
    #entry_yp = f"${dyp[0]*100:.2f}$"
    #entry_xm = f"${dxm[0]*100:.2f}$"
    #entry_ym = f"${dym[0]*100:.2f}$"

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table


def makeGammaPrecisionStdev(df_slice, scale =1):
    rB, drB = unwrap_uc(df_slice["CKM::rB"])
    deltaB, ddeltaB = unwrap_uc(df_slice["CKM::deltaB"])
    gamma, dgamma = unwrap_uc(df_slice["CKM::gamma"])
    entry_gamma = f"${scale * np.std(dgamma):.3f}$"
    entry_deltaB = f"${scale * np.std(ddeltaB):.3f}$"
    entry_rB = f"${100*scale * np.std(drB):.3f}$"
    
    #entry_gamma = f"${dgamma[0]:.3f}$"
#    entry_gamma = f"${np.std(gamma):.1f}$"
    table = f"{entry_rB}&{entry_deltaB}&{entry_gamma}"
    return table





def makeStdev(df_slice, scale =1):

    xp, dxp, yp, dyp, xm, dxm, ym, dym = unwrap_slice(df_slice)
    
    entry_xp = f"${np.std(xp)*100*scale:.3f}$"
    entry_yp = f"${np.std(yp)*100*scale:.3f}$"
    entry_xm = f"${np.std(xm)*100*scale:.3f}$"
    entry_ym = f"${np.std(ym)*100*scale:.3f}$"
#    entry_xp = f"${np.std(xp)*100:.1f}$"
#    entry_yp = f"${np.std(yp)*100:.1f}$"
#    entry_xm = f"${np.std(xm)*100:.1f}$"
#    entry_ym = f"${np.std(ym)*100:.1f}$"
#

    #entry_xp = f"${dxp[0]*100:.2f}$"
    #entry_yp = f"${dyp[0]*100:.2f}$"
    #entry_xm = f"${dxm[0]*100:.2f}$"
    #entry_ym = f"${dym[0]*100:.2f}$"

    table = f"{entry_xp}&{entry_yp}&{entry_xm}&{entry_ym}"
    return table


def makeGammaStdev(df_slice, scale =1):
    rB, drB = unwrap_uc(df_slice["CKM::rB"])
    deltaB, ddeltaB = unwrap_uc(df_slice["CKM::deltaB"])
    gamma, dgamma = unwrap_uc(df_slice["CKM::gamma"])
    entry_gamma = f"${scale * np.std(gamma):.3f}$"
    entry_deltaB = f"${scale * np.std(deltaB):.3f}$"
    entry_rB = f"${100*scale * np.std(rB):.3f}$"
    
    #entry_gamma = f"${dgamma[0]:.3f}$"
#    entry_gamma = f"${np.std(gamma):.1f}$"
    table = f"{entry_rB}&{entry_deltaB}&{entry_gamma}"
    return table




def makeStdevMulti(dfs, scale = 1):


    xp0, dxp0, yp0, dyp0, xm0, dxm0, ym0, dym0 = unwrap_slice(dfs[0])
    xp1, dxp1, yp1, dyp1, xm1, dxm1, ym1, dym1 = unwrap_slice(dfs[1])
    xp2, dxp2, yp2, dyp2, xm2, dxm2, ym2, dym2 = unwrap_slice(dfs[2])
    rB0, drB0 = unwrap_uc(dfs[0]["CKM::rB"])
    deltaB0, ddeltaB0 = unwrap_uc(dfs[0]["CKM::deltaB"])
    gamma0, dgamma0 = unwrap_uc(dfs[0]["CKM::gamma"])

    rB1, drB1 = unwrap_uc(dfs[1]["CKM::rB"])
    deltaB1, ddeltaB1 = unwrap_uc(dfs[1]["CKM::deltaB"])
    gamma1, dgamma1 = unwrap_uc(dfs[1]["CKM::gamma"])

    rB2, drB2 = unwrap_uc(dfs[2]["CKM::rB"])
    deltaB2, ddeltaB2 = unwrap_uc(dfs[2]["CKM::deltaB"])
    gamma2, dgamma2 = unwrap_uc(dfs[2]["CKM::gamma"])




    entry_xp0 = f"${np.std(xp0)*100*scale:.3f}$"
    entry_yp0 = f"${np.std(yp0)*100*scale:.3f}$"
    entry_xm0 = f"${np.std(xm0)*100*scale:.3f}$"
    entry_ym0 = f"${np.std(ym0)*100*scale:.3f}$"
    entry_xp1 = f"${np.std(xp1)*100*scale:.3f}$"
    entry_yp1 = f"${np.std(yp1)*100*scale:.3f}$"
    entry_xm1 = f"${np.std(xm1)*100*scale:.3f}$"
    entry_ym1 = f"${np.std(ym1)*100*scale:.3f}$"

    entry_xp2 = f"${np.std(xp2)*100*scale:.3f}$"
    entry_yp2 = f"${np.std(yp2)*100*scale:.3f}$"
    entry_xm2 = f"${np.std(xm2)*100*scale:.3f}$"
    entry_ym2 = f"${np.std(ym2)*100*scale:.3f}$"



    entry_gamma0 = f"${scale * np.std(gamma0):.3f}$"
    entry_deltaB0 = f"${scale * np.std(deltaB0):.3f}$"
    entry_rB0 = f"${100*scale * np.std(rB0):.3f}$"
    
    entry_gamma1 = f"${scale * np.std(gamma1):.3f}$"
    entry_deltaB1 = f"${scale * np.std(deltaB1):.3f}$"
    entry_rB1 = f"${100*scale * np.std(rB1):.3f}$"
    
    entry_gamma2 = f"${scale * np.std(gamma2):.3f}$"
    entry_deltaB2 = f"${scale * np.std(deltaB2):.3f}$"
    entry_rB2 = f"${100*scale * np.std(rB2):.3f}$"
    

    table_xp = f"{entry_xp0}&{entry_xp1}&{entry_xp2}"
    table_yp = f"{entry_yp0}&{entry_yp1}&{entry_yp2}"
    table_xm = f"{entry_xm0}&{entry_xm1}&{entry_xm2}"
    table_ym = f"{entry_ym0}&{entry_ym1}&{entry_ym2}"
    table_rB = f"{entry_rB0}&{entry_rB1}&{entry_rB2}"
    table_deltaB = f"{entry_deltaB0}&{entry_deltaB1}&{entry_deltaB2}"
    table_gamma = f"{entry_gamma0}&{entry_gamma1}&{entry_gamma2}"
    return f"{table_xp}&{table_yp}&{table_xm}&{table_ym}&{table_gamma}"
       
