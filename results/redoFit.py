import ROOT
import uncertainties
import uncertainties.umath
import os
import numpy as np
import sys
import pickle as pkl
import uproot
def getParamsDict(fileName):
    f = ROOT.TFile.Open(fileName)
    fr = f.Get("FitResult")

    order = 0

    if "poly_" in fileName:
        order = int(fileName.split("poly_")[-1].split("_")[0])


    #print(fr.all_members)
    #status = fr.Status()
    nParams = fr.NPar()
    #ndf = fr.Ndf()
    #chi2 = fr.Chi2()
    p = {}
    fcn = fr.MinFcnValue()

    for i in range(nParams):
        p[fr.ParName(i)] = uncertainties.ufloat(fr.Parameter(i), fr.Error(i))

    return p, order

def makeOptFromFit(fileName, output):
    p, order = getParamsDict(fileName)
    paramPart = f"PhaseCorrection::Order {order}\n"
    for k in p:
        v = p[k].n
        dv =p[k].s
        paramPart += f"{k} Free {v} {dv}\n"
   
    preamble  = """
EventType D0 K0S0 pi- pi+
#TagTypes  {
#    "KK D0{K+,K-} .443"
#    "Kspi0 D0{K0S0,pi0} .690"
#    "Kppim D0{K+,pi-} 4.740"
#    "Kmpip D0{K-,pi+} 4.740"
#    "Kspipi D0{K0S0,pi-,pi+} .899"
#}
#
#nEVents = 1000 is the 'normal' version - change this? 
#CouplingConstant::Coordinates  polar
#CouplingConstant::AngularUnits deg 
Particle::DefaultModifier BL
#Particle::SpinFormalism   Canonical
#Particle::DefaultModifier BELLE2018
Particle::SpinFormalism   Covariant


#Use 1 decay with the yields of all types -> KK = CPEven (pretend all the CP even are just KK decays since we are w
TagTypes  {
    "KK D0{K+,K-} 2.546"
    "Kspi0 D0{K0S0,pi0} 1.725"
    "Kppim D0{K+,pi-} 23.457"
    "Kmpip D0{K-,pi+} 23.457"
    "Kspipi D0{K0S0,pi-,pi+} 1.833"
}



BTagTypes {
    "Bm2DKm -1 6.2765" 
    "Bp2DKp 1 6.2765" 
}

PhaseCorrection::debug false
makeCPConj true
PhaseCorrection::PolyType antiSym_legendre
PhaseCorrection::useSquareDalitz true
doFit false
Minimiser::Tolerance 0.01
NInt 250000
MinEvents 5
"""
    with open(output, "w") as f:
        f.write(preamble)
        f.write(paramPart)

    

        
            


def redoFit(psi_url, BDK_url, fit_file, folder):
    print(f"Fit File = {fit_file}")
    plot_file = fit_file.split("/")[-1]
    run_name = plot_file.replace("_plot.root", ".py")
    opt_name = plot_file.replace("_plot.root", ".opt")
    makeOptFromFit(fit_file, f"{folder}/{opt_name}")
    psi_file = psi_url.split("/")[-1]
    BDK_file = BDK_url.split("/")[-1]

    log_file = plot_file.replace("_plot.root", ".log")



    #os.system(f"xrdcp {psi_url} {folder}/")
    #os.system(f"xrdcp {BDK_url} {folder}/")


    args = f"--BESIIIDataSample {folder}/{psi_file} --LHCbDataSample {folder}/{BDK_file} --nBins 100 --LogFile {folder}/{log_file} --Plots {folder}/{plot_file} {folder}/{opt_name}"
    exe = "KspipiCKMSimFit.exe"
    with open(f"{folder}/{run_name}", "w") as f:
        f.write(f"""import os\n
os.system("xrdcp {psi_url} {folder}")
os.system("xrdcp {BDK_url} {folder}")
os.system("{exe} {args}")
os.system("rm {folder}/{psi_file}")
os.system("rm {folder}/{BDK_file}")
""")
    


    #os.system(f"~/sw/qmiAmpGen/DaVinciDev_v45r1/run gaudirun.py redo.py")







 
if __name__=="__main__":
    try:
        psi_file = sys.argv[1]
    except:
        psi_file = ""
    try:
        BDK_file = sys.argv[2]
    except:
        BDK_file = ""
    try:
        fit_file = sys.argv[3]
    except:
        fit_file = ""
    try:
        output_folder = sys.argv[4]
    except:
        output_folder = ""

    if output_folder != "" and psi_file != "" and BDK_file != "" and fit_file!= "":
#        if "psi3770" in data_file:
#            makePullPsi3770Hists(data_file, fit_file, output)
#        if "BDK" in data_file:
#            makePullBDKHists(data_file, fit_file, output)
        redoFit(psi_file, BDK_file, fit_file, f"{output_folder}/")
    #print(f"{data_file} {fit_file} {output}")



