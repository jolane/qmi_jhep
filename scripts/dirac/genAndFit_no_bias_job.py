import sys, os
QMI_JHEP = os.environ["QMI_JHEP"]
def myLocalFiles(files):
    return [LocalFile(f) for f in files]
Omax = 6



genModel = "no_bias"

N = 100
minSeed = 0
j = Job()

myApp = GaudiExec()
myApp.directory =QMI_JHEP +  "/AmpGenDev/"
myApp.options = ["genAndFit_" + genModel  + "_options.py"]

useDirac = True
if useDirac:
    nCores = 4
    j.backend = Dirac()
    j.backend.settings["CPUTime"] = int(float(1.3e6))
    j.backend.minProcessors = nCores
    j.backend.maxProcessors = nCores
    #j.backend.settings['BannedSites']  = ['LCG.UKI-LT2-QMUL.uk', 'LCG.RRCKI.ru', 'LCG.CSCS.ch', 'LCG.RAL-HEP.uk', 'LCG.Sheffield.uk']
#    j.backend.settings['BannedSites']  = ['LCG.RRCKI.ru', 'LCG.CSCS.ch', 'LCG.Sheffield.uk']
    j.backend.downloadSandbox = False
else:
    j.backend = Interactive()



#poly_files_deltaB = ["opts/poly_{}_deltaBShift.opt".format(i) for i in range(1, 22)]
#poly_files_gamma = ["opts/poly_{}_gammaShift.opt".format(i) for i in range(1, 22)]
#poly_files = ["opts/poly_{}.opt".format(i) for i in range(1, 22)] + poly_files_gamma + poly_files_deltaB + ["opts/poly_1_MD.opt"]
fit_files = myLocalFiles([QMI_JHEP + "/scripts/opts/poly_{}.opt".format(i) for i in range(1, 10)] + [QMI_JHEP + "/scripts/opts/MD.opt"])
#binning_schemes = [DiracFile("KsPiPi_equal.txt", lfn="/lhcb/user/j/jolane/binning_schemes/KsPiPi_equal.txt"), DiracFile("KsPiPi_optimal.txt", lfn="/lhcb/user/j/jolane/binning_schemes/KsPiPi_optimal.txt"), DiracFile("KsPiPi_mod_optimal.txt", lfn="/lhcb/user/j/jolane/binning_schemes/KsPiPi_mod_optimal.txt")]
binning_schemes = [LocalFile(QMI_JHEP + "/scripts/binning_schemes/" + i) for i in ["KsPiPi_equal.txt", "KsPiPi_optimal.txt", "KsPiPi_mod_optimal.txt"]]
model_files = [LocalFile(QMI_JHEP + "/scripts/opts/" + i + ".opt") for i in "KK Kspi0 Kmpip Kppim Kspipi Kspipi_bkg no_bias single_peak_bias double_peak_bias".split()]
print(fit_files)
print(model_files)
#j.inputfiles = [f for f in myLocalFiles(["KK.opt","Kspi0.opt","Kppim.opt","Kmpip.opt","Kspipi.opt", "Kspipi_bkg.opt",  genModel + ".opt", "MD.opt", "besiii_equal.opt", "besiii_optimal.opt", "besiii_mod_optimal.opt", "belle_2018_equal.opt", "belle_2018_optimal.opt", "belle_2018_mod_optimal.opt", "besiii_cov_matrix_equal.txt", "besiii_cov_matrix_optimal.txt", "besiii_cov_matrix_mod_optimal.txt"] + poly_files)]  + binning_schemes
j.inputfiles = fit_files + binning_schemes + model_files
j.outputfiles = [LocalFile("*.log"), DiracFile("*plot.root")]
#j.splitter = GaussSplitter(numberOfJobs=2)

j.splitter = GenericSplitter()
j.splitter.attribute = "application.extraArgs"
#j.splitter.values = [{"AMPGEN_SEED":"1"}, {"AMPGEN_SEED":"2"}]

d = []
for i in range(1, N + 1):
    #d += [{"AMPGEN_SEED":str(i)}]
    #d += [[str(i)]]
    d += [[]]
j.splitter.values = d
j.application = myApp
N_psi3770 = 1
N_BDK = 1
#j.name = "qmi_{}_{}_x{}_{}_stretchAntiSym".format(Omax, genModel, N, minSeed)
j.name = "genAndFit_{}_qmi_{}_x{}_{}_{}".format(genModel, Omax, N, N_psi3770, N_BDK) + "_NInt_1e6"
#j.name = "genAndFit_{}_binned_x{}_{}_{}".format(genModel, N, N_psi3770, N_BDK)
#j.name = "MD_{}_x{}_{}".format(genModel, N, minSeed)
#j.name = "genAndFit_maxOrder_{}_gen_{}_NJobs_{}_{}_minos".format(Omax, genModel, N, minSeed)
#j.splitter.attribute = "application"
#j.splitter.values = [myApp, myApp]


j.submit()



