
import os, subprocess, argparse
import numpy as np
import pandas as pd
import sys
#from matplotlib import pyplot as plt

#import uproot
regen = False
import math

#def plotHist1D(fileName, key, output, xlabel=""):
#    f = uproot.open(fileName)
#    n, b = f[key].to_numpy()
#    c = (b[:-1] + b[1:])/2.
#    fig, ax = plt.subplots(1,1)
#    ax.set_xlabel(xlabel)
#    ax.set_ylabel("Counts")
#    bar = ax.bar(c, n, width = 0.7 * (c[1] - c[0]))
#    fig.tight_layout()
#    fig.savefig(output, dpi=300)
#    plt.close()
#
#def plotHist2D(fileName, key, output, xlabel="", ylabel=""):
#    f = uproot.open(fileName)
#    n, b1, b2 = f[key].to_numpy()
#    c1 = (b1[:-1] + b1[1:])/2
#    c2 = (b2[:-1] + b2[1:])/2
#    
#
#
#
#
#def plotScatter(x, y, z, output, xlabel="", ylabel="", zlabel="", aspect="auto", normalize=True):
#    fig, ax = plt.subplots(1,1)
#
#
#    if normalize:
#        norm = max(z)
#        if norm != 0:
#            z = [i/norm for i in z]
#        try:
#            zlabel += f"/{norm:.3f}"
#        except:
#            str_norm = str(norm)[0:4+1]
#            zlabel += "/{}".format(str_norm)
#    sc = ax.scatter(x, y, c=z, s=1)
#    cb = plt.colorbar(sc, ax=ax, label=zlabel)
#    ax.set_xlabel(xlabel)
#    ax.set_ylabel(ylabel)
#    ax.set_aspect(aspect)
#    fig.tight_layout()
#    fig.savefig(output, dpi=300)
#    plt.close()
#
#
#def getDalitz(fileName):
#    f = uproot.open(fileName)
#    tree = f["Dalitz"]
#    s01 = tree["s01"].array()
#    s02 = tree["s02"].array()
#    s12 = tree["s12"].array()
#    return s01, s02, s12
#
#
#def getX(fileName, name):
#    f = uproot.open(fileName)
#    tree = f[name]
#    return tree["x"].array()
#


def runSP(cmd):
    output = subprocess.check_output(cmd.split())
    return output

def run(cmd):
    os.system(cmd)
    return "finished {}".format(cmd)


def genPsi3770(model, output, seed, nEvents, NInt = int(2e5), nCores = 1):


    if os.path.isfile(output)==False or regen==True:
        genOutput = run("QcGen2.exe --Output {} --Seed {} --NInt {} --nEvents {} --nCores {} {}".format(output, seed, NInt, nEvents, nCores, model))
#        x,y,z=getDalitz(output)
#        dd = getX(output, "dd")
#        corr = getX(output, "corr")
#        plotScatter(x, y, dd, output="{}".format(output.replace(".root", "_dd.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$\Delta \delta_D$")
#        plotScatter(x, y, corr, output="{}".format(output.replace(".root", "_corr.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$f(\Phi)$")
        return output, genOutput
    else:
        return output, ""

def genB2DK(model, output, seed, nEvents, NInt = int(2e5), nCores = 1):
    if os.path.isfile(output)==False or regen==True:
        genOutput = run("gammaGen.exe --Output {} --Seed {} --NInt {} --nEvents {} --nCores {} {}".format(output, seed, NInt, nEvents, nCores, model))

#        x,y,z=getDalitz(output)
#        dd = getX(output, "dd")
#        corr = getX(output, "corr")
#        plotScatter(x, y, dd, output="{}".format(output.replace(".root", "_dd.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$\Delta \delta_D$")
#        plotScatter(x, y, corr, output="{}".format(output.replace(".root", "_corr.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$f(\Phi)$")
#
        return output, genOutput
    else:
        return output, ""



def fitPsi3770(model, data, log, NInt = int(2e5), nCores = 1):
    plot = log.replace(".log", "_plot.root")
    fitOutput = run("KspipiPsi3770Fit.exe --Seed 1 --BESIIIDataSample {} --NInt {} --LogFile {} --nCores {} --Plots {} {}".format(data, NInt, log, nCores, plot, model))
#    x,y,z=getDalitz(plot)
#    dd = getX(plot, "dd")
#    corr = getX(plot, "corr")
#    plotScatter(x, y, dd, output="{}".format(plot.replace(".root", "_dd.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$\Delta \delta_D$")
#    plotScatter(x, y, corr, output="{}".format(plot.replace(".root", "_corr.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$f(\Phi)$")
#
    return log, fitOutput

def fitB2DK(model, data, log, NInt = int(2e5), nCores = 1, psi3770Log =""):
    

    plot = log.replace(".log", "_plot.root")
    if psi3770Log != "":
        fitOutput = run("KspipiBDKFit.exe --Seed 1 --LHCbDataSample {} --NInt {} --LogFile {} --nCores {} --Plots {} --psi3770Log {} {}".format(data, NInt, log, nCores, plot, psi3770Log, model))
    else:
        fitOutput = run("KspipiBDKFit.exe --Seed 1 --LHCbDataSample {} --NInt {} --LogFile {} --nCores {} --Plots {} --doChi2 false --doProjPlots false {}".format(data, NInt, log, nCores, plot, model))

#    x,y,z=getDalitz(plot)
#    dd = getX(plot, "dd")
#    corr = getX(plot, "corr")
#    plotScatter(x, y, dd, output="{}".format(plot.replace(".root", "_dd.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$\Delta \delta_D$")
#    plotScatter(x, y, corr, output="{}".format(plot.replace(".root", "_corr.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$f(\Phi)$")
#


    return log, fitOutput

def fitBoth(model, psiData, BDKData, log, NInt=int(2e5), nCores = 1, Strategy=3):

    plot = log.replace(".log", "_plot.root")
    fitOutput = run("KspipiCKMSimFit.exe --Seed 1 --BESIIIDataSample {} --LHCbDataSample {} --NInt {} --LogFile {} --nCores {} --Plots {} --Minimiser::Strategy {} --doChi2 false --doProjPlots false {}".format(psiData, BDKData, NInt, log, nCores, plot, Strategy ,model))
#    x,y,z=getDalitz(plot)
#    dd = getX(plot, "dd")
#    corr = getX(plot, "corr")
#    plotScatter(x, y, dd, output="{}".format(plot.replace(".root", "_dd.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$\Delta \delta_D$")
#    plotScatter(x, y, corr, output="{}".format(plot.replace(".root", "_corr.png")), aspect="equal", xlabel=r"$m_+^2$", ylabel=r"$m_\_^2$", zlabel=r"$f(\Phi)$")
#

    return log, fitOutput


def fitBinned(psiData, BDKData, log, binning_scheme = "equal"):
    with open("binned.opt", "w") as f:
        f.write("""EventType D0 K0S0 pi- pi+
c1 Free 0 1 -1.5 1.5
c2 Free 0 1 -1.5 1.5
c3 Free 0 1 -1.5 1.5
c4 Free 0 1 -1.5 1.5
c5 Free 0 1 -1.5 1.5
c6 Free 0 1 -1.5 1.5
c7 Free 0 1 -1.5 1.5
c8 Free 0 1 -1.5 1.5
s1 Free 0 1 -1.5 1.5
s2 Free 0 1 -1.5 1.5
s3 Free 0 1 -1.5 1.5
s4 Free 0 1 -1.5 1.5
s5 Free 0 1 -1.5 1.5
s6 Free 0 1 -1.5 1.5
s7 Free 0 1 -1.5 1.5
s8 Free 0 1 -1.5 1.5
F1 Fix 0 1
F2 Fix 0 1
F3 Fix 0 1
F4 Fix 0 1
F5 Fix 0 1
F6 Fix 0 1
F7 Fix 0 1
F8 Fix 0 1
F-1 Fix 0 1
F-2 Fix 0 1
F-3 Fix 0 1
F-4 Fix 0 1
F-5 Fix 0 1
F-6 Fix 0 1
F-7 Fix 0 1
F-8 Fix 0 1
CKM::x+ Free 0 1
CKM::y+ Free 0 1
CKM::x- Free 0 1
CKM::y- Free 0 1
TagTypes  {
    "KK D0{K+,K-} 2.546"
    "Kspi0 D0{K0S0,pi0} 1.725"
    "Kppim D0{K+,pi-} 23.457"
    "Kmpip D0{K-,pi+} 23.457"
    "Kspipi D0{K0S0,pi-,pi+} 1.833"
}
BTagTypes {
    "Bm2DKm -1 12.553" 
    "Bp2DKp 1 12.553" 
}

contraint_mod_besiiiRef 0.5
""")

        f.write("binning KsPiPi_{}.txt\n".format(binning_scheme))
        f.write("Import besiii_{}.opt\n".format(binning_scheme))
        f.write("BESIIIRefErrs besiii_{}.opt\n".format(binning_scheme))
        f.write("BESIIIRefCorr besiii_cov_matrix_{}.txt\n".format(binning_scheme))
  #      with open("belle_2018_{}.opt".format(binning_scheme)) as f2:
  #          for l in f2:
  #              f.write(l)

    psi_log = log.replace(".log", "psi3770.log")
    constant_log = log.replace(".log", "constant.log")
    constrained_log = log.replace(".log", "constrained.log")
    combined_log = log.replace(".log", "combined.log")
    fitOutput = run("KspipiCKMSimBinnedFit.exe --BESIIIDataSample {} --LHCbDataSample {} --nCores 1 --Psi3770Log {} --constantLog {} --constrainedLog {} --combinedLog {} binned.opt".format(psiData, BDKData, psi_log, constant_log, constrained_log, combined_log))
    return fitOutput, log
    

def OfromN(N):
#    O2 + 3O + 2(1-N) = 0
#    det = 9 - 4 * 2 * (1 - N)
#    O = -3/2 +- det**0.5/2
#    O>0: O = 
    return -0.5 + (8*N - 7)**0.5/2
    

def makeOptFromLog(log, bound=0):
    string = ""
    nCij = 0
    Order = -1
    with open(log) as g:
        for line in g:
            if "Parameter" in line and "Free" in line:
                a = line.split()
                if "PhaseCorrection::C" in a[1]:
                    b = a[1].replace("PhaseCorrection::C", "").split("_")
                    i = int(b[0])
                    j = int(b[1])
                    myOrder = i + j
                    if myOrder > Order:
                        Order = myOrder
                    nCij += 1
                mu = float(a[3])
                sigma = float(a[4])
                string += "{} Free {} {}".format(a[1],mu,sigma)
                if bound !=0:
                    string += " {} {}".format(mu - bound * sigma, mu + bound *sigma)
                string += "\n"
    if nCij > 0:
        O = int(OfromN(nCij)) 
        O = Order
        string += "PhaseCorrection::Order {}\n".format(O)
    return string

def makeCKMOpt(CKMPolar=False):
    string = """
KK_beta Fix 1 0
Kspi0_beta Fix 1 0
Kppim_beta Fix 1 0
Kmpip_beta Fix 1 0
Kspipi_beta Fix 1 0

KK_a0 Fix 0 0
Kspi0_a0 Fix 0 0
Kppim_a0 Fix 0 0
Kmpip_a0 Fix 0 0
Kspipi_a0 Fix 0 0


Bp2DKp_beta Fix 1 0
Bm2DKm_beta Fix 1 0
Bp2DKp_a0 Fix 0 0
Bm2DKm_a0 Fix 0 0
"""
    if CKMPolar:
        string += """CKM::rB Free 0 1
CKM::deltaB Free 0 1
CKM::gamma Free 0 1
"""
    else:
        string += """CKM::x+ Free 0 1
CKM::y+ Free 0 1
CKM::x- Free 0 1
CKM::y- Free 0 1
"""

    return string

def makePreamble(makeCKM=False, CKMPolar=False):
    string = """EventType D0 K0S0 pi- pi+
#TagTypes  {
#    "KK D0{K+,K-} .443"
#    "Kspi0 D0{K0S0,pi0} .690"
#    "Kppim D0{K+,pi-} 4.740"
#    "Kmpip D0{K-,pi+} 4.740"
#    "Kspipi D0{K0S0,pi-,pi+} .899"
#}
#
#nEVents = 1000 is the 'normal' version - change this? 
#CouplingConstant::Coordinates  polar
#CouplingConstant::AngularUnits deg 

Particle::DefaultModifier BL
Particle::SpinFormalism   Covariant



#Use 1 decay with the yields of all types -> KK = CPEven (pretend all the CP even are just KK decays since we are w
TagTypes  {
    "KK D0{K+,K-} 2.546"
    "Kspi0 D0{K0S0,pi0} 1.725"
    "Kppim D0{K+,pi-} 23.457"
    "Kmpip D0{K-,pi+} 23.457"
    "Kspipi D0{K0S0,pi-,pi+} 1.833"
}

Minimiser::PrintLevel 1

#Minimiser::Tolerance 1
#Minimiser::runminos 0
MinosCKM true

BTagTypes {
    "Bm2DKm -1 12.553" 
    "Bp2DKp 1 12.553" 
}
PhaseCorrection::useSquareDalitz false
PhaseCorrection::stretchAntiSym true
PhaseCorrection::stretchAntiSym_scale 2
PhaseCorrection::stretchAntiSym_offset 2

KK_beta Fix 1 0
Kspi0_beta Fix 1 0
Kppim_beta Fix 1 0
Kmpip_beta Fix 1 0
Kspipi_beta Fix 1 0

KK_a0 Fix 0 0
Kspi0_a0 Fix 0 0
Kppim_a0 Fix 0 0
Kmpip_a0 Fix 0 0
Kspipi_a0 Fix 0 0


Bp2DKp_beta Fix 1 0
Bm2DKm_beta Fix 1 0
Bp2DKp_a0 Fix 0 0
Bm2DKm_a0 Fix 0 0
"""
    if makeCKM:
        if CKMPolar:
            string += """CKM::rB Free 0 1
CKM::deltaB Free 0 1
CKM::gamma Free 0 1
"""
        else:
            string += """CKM::x+ Free 0 1
CKM::y+ Free 0 1
CKM::x- Free 0 1
CKM::y- Free 0 1
"""
    return string
    
def readArgs(i, default_answer):
    try:
        return sys.argv[i]
    except:
        return default_answer


def polyOrder(optFile):
    nParams = 0
    zero_j = []
    with open(optFile) as f:
        for line in f:
            if "Free" in line:
                if "PhaseCorrection::C" in line:
                    nParams += 1

    if nParams == 0:
        return -1
    else:
        return OfromN(nParams)

def polyOrder2(optFile):
    Order = -1
#    parameters = []
    with open(optFile) as f:
        for line in f:
            if "PhaseCorrection::Order" in line:
                a =line.split()
                Order = int(a[-1])
    return Order

def boundFit(fit_psi3770, psi3770_data, BDK_data, NInt, nCores):

        besiii_log, fitBESIIIOut = fitPsi3770(fit_psi3770, psi3770_data, fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", ".log"), NInt = NInt, nCores = nCores)
        #print(fitBESIIIOut)
        besiii_fit = makeOptFromLog(besiii_log, bound=0)
        besiii_log = fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", ".log")
        
        #unbound_fit = makeOptFromLog(besiii_log, bound=0)

        with open(besiii_log.replace(".log", "_bound.opt"), "w") as f:
            f.write(makePreamble(True) + "\n" + besiii_fit)
        #with open(besiii_log.replace(".log", "_unbound.opt"), "w") as f:
        #    f.write(makePreamble(True) + "\n" + unbound_fit)

        #lhcb_log, fitLHCbOut = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_bound.log"), NInt = NInt, nCores = nCores)
        lhcb_log, fitLHCbOut = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_bound.log"), NInt = NInt, nCores = nCores, psi3770Log = besiii_log)
     

def fit(fit_psi3770, psi3770_data, BDK_data, NInt, nCores, fit_psi3770_Order=0):
    try:
        nCores2 = int(os.environ["DIRAC_JOB_PROCESSORS"])
    except:
        nCores2 = nCores
    nCores = nCores2

    fit_psi3770_Order = polyOrder2(fit_psi3770) 
    if fit_psi3770_Order != 0:
#        boundFit(fit_psi3770, psi3770_data, BDK_data, NInt, nCores)
       #return 
        #print(fitLHCbOut)
        #lhcb_log2, fitLHCbOut2 = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_unbound.log"), NInt = NInt, nCores = nCores)
        #lhcb_log, fitLHCbOut = fitB2DK(fit_psi3770, BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_bound.log"), NInt = NInt, nCores = nCores)


        #print(fitLHCbOut2)
    #    lhcb_log, fitLHCbOut = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, BDK_data.replace(".root", "") + "_sep.log", NInt = NInt, nCores = nCores)
        #both_log, fitbothOut = fitBoth(besiii_log.replace(".log", "_unbound.opt"), psi3770_data, BDK_data, fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.log") , NInt = NInt, nCores = nCores)
        comb_opt = fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.opt")
        
        CKM_string = makeCKMOpt()
        with open(comb_opt, "w") as f:
            f.write(CKM_string)
        os.system("cat {} >> {}".format(fit_psi3770, comb_opt))
        both_log = fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.log") 
        both_log, fitbothOut = fitBoth(comb_opt, psi3770_data, BDK_data, fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.log") , NInt = NInt, nCores = nCores)
        single_log, fitsingleOut = fitB2DK(comb_opt, BDK_data, fit_psi3770.replace(".opt", "_")  + BDK_data.replace(".root", "_BDK.log") , NInt = NInt, nCores = nCores)

        with open(both_log.replace(".log", ".opt"), "w") as f:
            freeParams = ""
            preamble = makePreamble(False)
            f.write(preamble)
            f.write("\n")
            f.write("PhaseCorrection::Order {}\n".format(fit_psi3770_Order))
            with open(both_log) as f2:
                for line in f2:
                    if "Parameter" in line:
                        a = line.split()
                        name = a[1]
                        flag = a[2]
                        val = a[3]
                        error = a[4]
                        freeParams += "{} {} {} {}\n".format(name, flag, val, error)
            f.write(freeParams)

#        both_log, fitBothOut = fitBoth(both_log.replace(".log", ".opt"), psi3770_data, BDK_data, fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.log") , NInt = NInt, nCores = nCores, Strategy=3)




    else:
        lhcb_log2, fitLHCbOut2 = fitB2DK(fit_psi3770, BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", ".log"), NInt = NInt, nCores = nCores)




def genAndSepFit(gen_model, fit_psi3770, seed, N_psi3770, N_BDK, NInt, nCores):
    try:
        nCores2 = int(os.environ["DIRAC_JOB_PROCESSORS"])
    except:
        nCores2 = nCores
    nCores = nCores2
    psi3770_data = gen_model.replace(".opt", "") + "_psi3770_" + str(seed) + "_" + str(N_psi3770)  + ".root"
    BDK_data = gen_model.replace(".opt", "") + "_BDK_" + str(seed)+ "_" + str(N_BDK)  + ".root"

    besiii_base, genBESIIIOut = genPsi3770(gen_model, psi3770_data, seed, N_psi3770, NInt = NInt, nCores = nCores)
    #print(genBESIIIOut)
    lhcb_base, genLHCbOut = genB2DK(gen_model, BDK_data, seed, N_BDK, NInt = NInt, nCores = nCores)

    fit_psi3770_Order = polyOrder2(fit_psi3770) 
    comb_opt = fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", "_") + BDK_data.replace(".root", "_comb.opt")
    comb_log = comb_opt.replace(".opt", ".log") 
    CKM_string = makeCKMOpt()
    with open(comb_opt, "w") as f:
        f.write(CKM_string)
    os.system("cat {} >> {}".format(fit_psi3770, comb_opt))
    print(psi3770_data) 
    print(BDK_data) 
    if fit_psi3770_Order != 0:
        try:
            besiii_log = fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", ".log")
            besiii_log, fitBESIIIOut = fitPsi3770(fit_psi3770, psi3770_data, fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", ".log"), NInt = NInt, nCores = nCores)
            #print(fitBESIIIOut)
            besiii_fit = makeOptFromLog(besiii_log, bound=0)

            
            #unbound_fit = makeOptFromLog(besiii_log, bound=0)
            bound_opt = comb_log.replace("comb.log", "bound.opt")
            bound_log = comb_log.replace("comb.log", "bound.log")

            with open(bound_opt, "w") as f:
                f.write(makePreamble(True) + "\n" + besiii_fit)
        except:
            besiii_log = fit_psi3770.replace(".opt", "_") + psi3770_data.replace(".root", ".log")
            bound_opt = comb_log.replace("comb.log", "bound.opt")
            bound_log = comb_log.replace("comb.log", "bound.log")



        #with open(besiii_log.replace(".log", "_unbound.opt"), "w") as f:
        #    f.write(makePreamble(True) + "\n" + unbound_fit)
#
 #       lhcb_log, fitLHCbOut = fitB2DK(bound_opt, BDK_data, bound_log, NInt = NInt, nCores = nCores)
        lhcb_log, fitLHCbOut = fitB2DK(bound_opt, BDK_data, bound_log, NInt = NInt, nCores = nCores, psi3770Log = besiii_log)
        #return 
        #print(fitLHCbOut)
        #lhcb_log2, fitLHCbOut2 = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_unbound.log"), NInt = NInt, nCores = nCores)
        #lhcb_log, fitLHCbOut = fitB2DK(fit_psi3770, BDK_data, fit_psi3770.replace(".opt", "_") +  BDK_data.replace(".root", "_bound.log"), NInt = NInt, nCores = nCores)


       # print(fitLHCbOut2)
    #    lhcb_log, fitLHCbOut = fitB2DK(besiii_log.replace(".log", "_bound.opt"), BDK_data, BDK_data.replace(".root", "") + "_sep.log", NInt = NInt, nCores = nCores)
        #both_log, fitbothOut = fitBoth(besiii_log.replace(".log", "_unbound.opt"), psi3770_data, BDK_data, fit_psi3770.replace(".opt", "_") + BDK_data.replace(".root", "_comb.log") , NInt = NInt, nCores = nCores)
        both_log, fitbothOut = fitBoth(comb_opt, psi3770_data, BDK_data, comb_log, NInt = NInt, nCores = nCores)
        lhcb_log2, fitLHCbOut2 = fitB2DK("MD.opt", BDK_data, "MD_" +  BDK_data.replace(".root", ".log"), NInt = NInt, nCores = nCores)
    else:

#        lhcb_log2, fitLHCbOut2 = fitB2DK("MD.opt", BDK_data, "MD_" +  BDK_data.replace(".root", ".log"), NInt = NInt, nCores = nCores)
        binned_log, r_binned = fitBinned(psi3770_data, BDK_data, BDK_data.replace(".root", "equal.log"), "equal")
        binned_log, r_binned = fitBinned(psi3770_data, BDK_data, BDK_data.replace(".root", "optimal.log"), "optimal")
        binned_log, r_binned = fitBinned(psi3770_data, BDK_data, BDK_data.replace(".root", "mod_optimal.log"), "mod_optimal")
##        print("MD done")
        
    return {"psi3770_data":psi3770_data, "BDK_data":BDK_data}




def mainArgv():
    offset = 0
    print("argv = {}".format(sys.argv ))
    
    for arg in sys.argv:
        if "gaudirun.py" in arg:
            offset = 1

    gen_model = readArgs(1 + offset, "base_HFLAV.opt")
    seed = int(readArgs(2 + offset, "0"))
    N_psi3770 = int(1000 * float(readArgs(3 + offset, "1")))
    N_BDK = int(1000 * float(readArgs(4 + offset, "1")))
    fit_psi3770 = readArgs(5 + offset, "poly_0.opt")
    #fit_BDK = readArgs(6, "poly_0.opt")
    NInt = int(float(readArgs(6 + offset, "1e4")))
    nCores = int(readArgs(7 + offset, "1"))
    print("offset = {}, genmodel = {}, N_psi3770 = {} seed = {}".format(offset, gen_model, N_psi3770, seed))
    #genAndFit(gen_model, seed, N_psi3770, N_BDK, NInt, nCores)
    #genAndSepFit(gen_model, fit_psi3770, seed, N_psi3770, N_BDK, NInt, nCores)
#output =  run("echo test")
#print(output.decode())

def getEnv(string, default):
    value = os.environ[string]
    if value == "":
        value = default
    return value

def getDict(string, default, d):
    try:
        value = d[string]
    except:
        value = default
    return value
    if value == "":
        value = default
    return value


def fitDict(d):

#    genmodel = getDict("genModel", "base_HFLAV.opt", d)
    fit_psi3770= getDict("fit_psi3770", "poly_0.opt", d)
#    N_psi3770 = int(float(getDict("N_psi3770", "1", d)) * 1000)
#    N_BDK = int(float(getDict("N_BDK", "1", d)) * 1000)
    NInt = int(float(getDict("NInt", "2e5", d)))
    nCores = int(getDict("nCores", "4", d))
    seed = int(getDict("Seed", "0", d))
    print(d)
#    print("NInt = {}, nCores = {}".format(genmodel, N_psi3770, N_BDK, NInt, nCores))

    #genAndSepFit(genmodel, fit_psi3770, seed, N_psi3770, N_BDK, NInt, nCores)
#
    psi3770_data = getDict("psi3770_data", "psi3770.root", d)
    BDK_data = getDict("BDK_data", "BDK.root", d)
    fit(fit_psi3770, psi3770_data, BDK_data, NInt, nCores)


def mainEnvs():
    genmodel = getEnv("myJob::genModel", "base_HFLAV.opt")
    fit_psi3770= getEnv("myJob::fit_psi3770", "poly_0.opt")
    N_psi3770 = int(getEnv("myJob::N_psi3770", "1") * 1000)
    N_BDK = int(getEnv("myJob::N_BDK", "1") * 1000)
    NInt = int(float(getEnv("myJob::NInt", "2e5")))
    nCores = int(getEnv("myJob::nCores", "4"))
    seed = int(getEnv("myJob::Seed", "0"))
    print("genmodel = {}, N_psi3770 = {}, NBDK = {}, NInt = {}, nCores = {}".format(genmodel, N_psi3770, N_BDK, NInt, nCores))
#    genAndSetFit(gen_model, fit_psi3770, seed, N_psi3770, N_BDK, NInt, nCores)
    

def mainDict(d):

    genmodel = getDict("genModel", "base_HFLAV.opt", d)
    fit_psi3770= getDict("fit_psi3770", "poly_0.opt", d)
    N_psi3770 = int(float(getDict("N_psi3770", "1", d)) * 1000)
    N_BDK = int(float(getDict("N_BDK", "1", d)) * 1000)
    NInt = int(float(getDict("NInt", "2e5", d)))
    nCores = int(getDict("nCores", "4", d))
    seed = int(getDict("Seed", "0", d))
    print(d)
    print("genmodel = {}, N_psi3770 = {}, NBDK = {}, NInt = {}, nCores = {}".format(genmodel, N_psi3770, N_BDK, NInt, nCores))

    r = genAndSepFit(genmodel, fit_psi3770, seed, N_psi3770, N_BDK, NInt, nCores)
    return r
#




d = {}
#d["genModel"]="belle_2018_HFLAV_gaussShift_gammaShift_smallShift.opt"
d["genModel"]="no_bias.opt"
d["fit_psi3770"]="MD.opt"
d["NInt"]=str(int(1e6))
d["nCores"]="4"
N_psi3770 = 1
N_BDK = 1
d["N_psi3770"]=str(N_psi3770)
d["N_BDK"]=str(N_BDK)

try: 
    d["Seed"]=int(os.getenv('ganga_jobid').split(".")[-1])  + 1
except:
    d["Seed"] = "2"

#r = mainDict(d)
#fit('poly_1.opt', psi3770_data, BDK_data, int(float(d['NInt'])), int(d['nCores']))
#fit('poly_2.opt', psi3770_data, BDK_data, int(float(d['NInt'])), int(d['nCores']))
#fit('poly_3.opt', psi3770_data, BDK_data, int(float(d['NInt'])), int(d['nCores']))


print("Generate {}:{} then fit {}".format(d["genModel"],d["Seed"] ,  d["fit_psi3770"]))
r = mainDict(d)
psi3770_data = r['psi3770_data']
BDK_data = r['BDK_data']

#     psi3770_data = r["psi3770_data"]
   #     BDK_data = r["BDK_data"]
Omax=6
for i in range(Omax, Omax + 1):
    fitFile = "poly_{}.opt".format(i)
#    fitFile = "poly_{}_MD.opt".format(i)
#       print("Fitting {} and {} with {}".format(psi3770_data, BDK_data, fitFile))
    fit(fitFile, psi3770_data, BDK_data, int(float(d["NInt"])), int(d["nCores"]))

#def fit(fit_psi3770, psi3770_data, BDK_data, NInt, nCores):

#N = 10
#for i in range(N-1):
#    d["Seed"] = i
#    r = mainDict(d)
